<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table="pr_category";
    public $primaryKey="cate_id";

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
    protected $hidden = [
        'cate_id','created_at','updated_at'
    ];

    public function sub_category()
    {
        return $this->hasMany('App\SubCategory', 'cate_id', 'cate_id');
    }

    public function item()
    {
        return $this->belongsTo('App\Category', 'cate_id','cate_id');
    }
}

<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Sheet;

class UserExport implements FromView,ShouldAutoSize,WithEvents
{

    private $frdt;
    private $todt;
    private $data;
    public function __construct($frdt, $todt,$data) {
        $this->frdt = $frdt;
        $this->todt = $todt;
        $this->data=$data;
    }

    public function view(): View
    {
        return view('excel.pswm_daily', [
            'order_data' => $this->data,
            'frdt'=>$this->frdt,
            'todt'=>$this->todt
        ]);
    }

    public function registerEvents(): array
    {
        Sheet::macro('styleCells', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getStyle($cellRange)->applyFromArray($style);
        });

        Sheet::macro('setOrientation', function (Sheet $sheet, $orientation) {
            $sheet->getDelegate()->getPageSetup()->setOrientation($orientation);
        });

        Sheet::macro('Worksheet', function (Sheet $sheet, string $cellRange, array $style) {
            $sheet->getDelegate()->getWorksheet($cellRange)->applyFromArray($style);
        });

        return [
            
            AfterSheet::class    => function(AfterSheet $event) {
                
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
                $event->sheet->styleCells(
                    'A3:J3',
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['rgb' => '000000'],
                            ],
                        ],
                        'fill' => [
                             'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                              'rotation' => 0,
                              'startColor' => [ 'rgb' => '3366FF' ],
                              'endColor' => [ 'rgb' => '3366FF' ] 
                        ],
                        'font' => [
                            'bold' => true,
                            'color' => [ 'rgb' => 'FFFFFF' ]
                        ],
                        'alignment' => [
                            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                        ],
                    ]
                );
                $event->sheet->getStyle('A3:J3')->getAlignment()->setWrapText(true);
                $event->sheet->mergeCells('A2:J2');
                $event->sheet->mergeCells('A1:J1');
                $last_row=$event->sheet->getHighestRow();
                $ind=$last_row+2;
                //$event->sheet->setCellValue('A'.$ind,"Prepared By:…………………………………………");
                //$event->sheet->setCellValue('R'.$ind,'Reviewed By:………………………………………………');
                //$event->sheet->mergeCells('A'.$ind.':D'.$ind);
                $event->sheet->styleCells(
                    'A3:J'.$last_row,
                    [
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['rgb' => '000000'],
                            ],
                        ],
                    ]
                    
                    
                );
                /*$event->sheet->styleCells(
                    'A3:A'.$last_row,
                    [
                        'font' => [
                            'color' => [ 'rgb' => 'ff4d4d' ]
                        ],
                    ]
                );*/
                /*$event->sheet->styleCells(
                    'E5:E'.$last_row,
                    [
                        'font' => [
                            'bold'=>true,
                            'color' => [ 'rgb' => '000000' ]
                        ],
                        'fill'=>[
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                            'rotation' => 0,
                            'startColor' => [ 'rgb' => 'ff9966' ],
                            'endColor' => [ 'rgb' => 'ff9966' ] 
                        ]
                    ]
            );*/
            /*$event->sheet->styleCells(
                'B5:B'.$last_row,
                [
                    'font' => [
                        'bold'=>true,
                        'color' => [ 'rgb' => '000000' ]
                    ],
                ]
            );*/
            /*$event->sheet->styleCells(
                'J5:J'.$last_row,
                    [
                        'font' => [
                            'bold'=>true,
                            'color' => [ 'rgb' => '000000' ]
                        ],
                        'fill'=>[
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                            'rotation' => 0,
                            'startColor' => [ 'rgb' => 'ff9966' ],
                            'endColor' => [ 'rgb' => 'ff9966' ] 
                        ]
                    ]
            );*/
            /*$event->sheet->styleCells(
                'K5:K'.$last_row,
                    [
                        'font' => [
                            'bold'=>true,
                            'color' => [ 'rgb' => '000000' ]
                        ],
                        'fill'=>[
                            'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                            'rotation' => 0,
                            'startColor' => [ 'rgb' => 'ff884d' ],
                            'endColor' => [ 'rgb' => 'ff884d' ] 
                        ]
                    ]
            );*/
            /*$event->sheet->styleCells(
                'N5:N'.$last_row,
                [
                    'font' => [
                        'bold'=>true,
                        'color' => [ 'rgb' => '000000' ]
                    ],
                    'fill'=>[
                        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                        'rotation' => 0,
                        'startColor' => [ 'rgb' => '00cc00' ],
                        'endColor' => [ 'rgb' => '00cc00' ] 
                    ]
                ]
            );*/
        /*$event->sheet->styleCells(
            'O5:O'.$last_row,
            [
                'font' => [
                    'bold'=>true,
                    'color' => [ 'rgb' => '000000' ]
                ],
                'fill'=>[
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                    'rotation' => 0,
                    'startColor' => [ 'rgb' => '00cc00' ],
                    'endColor' => [ 'rgb' => '00cc00' ] 
                ]
            ]
        );*/
        /*$event->sheet->styleCells(
            'P5:P'.$last_row,
            [
                'font' => [
                    'bold'=>true,
                    'color' => [ 'rgb' => '000000' ]
                ],
                'fill'=>[
                    'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                    'rotation' => 0,
                    'startColor' => [ 'rgb' => 'ffff80' ],
                    'endColor' => [ 'rgb' => 'ffff80' ] 
                ]
            ] 
        );*/

        /*$event->sheet->styleCells(
            'Q5:Q'.$last_row,
            [
                'font' => [
                    'color' => [ 'rgb' => '00cc00' ]
                ],
                'alignment' => [
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                ]
            ] 
        );*/
      },
           
    ];
    } 

    /**
    * @return \Illuminate\Support\Collection
    */
    /*public function collection()
    {
        return Category::all();
    }*/
}

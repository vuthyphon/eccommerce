<?php

namespace App\FrontEndModel;

use Illuminate\Database\Eloquent\Model;

class Frontuser extends Model
{
    protected $table="front_user";
    public $primaryKey="account_id";

    /*protected $hidden = [
        'created_at', 'updated_at'
    ];*/

    protected $fillable = [
        'account_id','first_name','last_name','uspassword','e_mail','tel','address','acc_no'
    ];

}

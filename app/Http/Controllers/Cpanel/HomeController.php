<?php

namespace App\Http\Controllers\Cpanel;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $setting=\App\Setting::first();
        return view('backend.home.index',['setting'=>$setting]);
    }

    public function profile($id)
    {
        $users=User::find($id);
        return view('backend.home.profile',['users'=>$users]);
        //return response()->json($users);
    }

    public function update_profile(Request $request)
    {
        $id = $request->get('us_id');
        Validator::make($request->all(), [
            'username' => 'required|min:3',
            'email' => 'required|email',
        ])->validate();

        $data=array(
            'username'=>$request->input('username'),
            'email'=>$request->input('email')
        );
       if($request->get('password')!="")
        {
            $data['password']=Hash::make($request->get('password'));
            Validator::make($request->all(), [
                'password' => 'required|min:6',
                ])->validate();
        }
        
        User::where('id', $id)->update($data);

        $request->session()->flash('message','<div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Users Profile Upated!</strong>.
      </div>');
        return redirect()->back();
    }

    function update(Request $request)
    {
        $setting=\App\Setting::find(1);
        $setting->fb_link=$request->facebook;
        $setting->twitter_link=$request->twitter;
        $setting->instagram_link=$request->instagram;
        $setting->youtube_link=$request->youtube;
        $setting->email_link=$request->email;
        $setting->telephone=$request->telephone;
        $setting->mobile=$request->phone;
        $setting->address=$request->address;
        $setting->save();
        $request->session()->flash('message','<div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Setting Upated!</strong>.
      </div>');
        return redirect()->back();
    }


}

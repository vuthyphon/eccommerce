<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ItemDetail;
use App\Item;
use Auth;
use Illuminate\Support\Facades\DB;
use Image;
use Storage;
use App\ItemPrice;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $category=Category::with('sub_category:cate_id,sub_cate_name')->get();
       $item=Item::with(['sub_category:sub_cateid,sub_cate_name','category:cate_id,cate_name','user:id,username',
        'item_price'=> function($query)
        {
            $query->orderBy('price_id', 'desc');
        
        }])->where('d_status',1)->orderBy('created_at','DESC')->paginate(10);
        
        return view('backend.products.index',['products'=>$item]);
      
    }

    function get_all_item()
    {
        $item=Item::with(['sub_category:sub_cateid,sub_cate_name','category:cate_id,cate_name','user:id,username',
        'item_price'=> function($query)
        {
            $query->orderBy('price_id', 'desc');
        
        }])->where('d_status',1)->orderBy('created_at','DESC')->paginate(10);
        $response = [
            'pagination' => [
                'total' => $item->total(),
                'per_page' => $item->perPage(),
                'current_page' => $item->currentPage(),
                'last_page' => $item->lastPage(),
                'from' => $item->firstItem(),
                'to' => $item->lastItem()
            ],
            'data' => $item
        ];
        return response()->json($response);
    }

    public function search(Request $request)
    {
        $name=$request->filter;
        $item=Item::with(['sub_category:sub_cateid,sub_cate_name','category:cate_id,cate_name','user:id,username',
        'item_price'=> function($query)
        {
            $query->orderBy('price_id', 'desc');
        
        }])->select('item_name_en','cate_id','sub_cate_id','creator_id','item_id','created_at')->where('item_name_en','like','%'.$name.'%')
        ->paginate(50);
        //return response()->json($item);

        return view('backend.products.index',['products'=>$item]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category=DB::table('pr_category')->get();
        $subCategory=DB::table('pr_subcategory')->get();
        return view('backend.products.new',['category'=>$category,'sub_category'=>$subCategory]);
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        $item=new Item();
        $item->item_code=$request->item_code;
        $item->item_name_en=$request->item_name_en;
        $item->descriptions_kh=$request->descriptions_kh;
        $item->descriptions_en=$request->descriptions_en;
        $item->descriptions_vn=$request->descriptions_vn;
        $item->descriptions_ch=$request->descriptions_ch;
        $item->brand=$request->brand;
        $item->cate_id=$request->category;
        $item->tag=$request->tag;
        $item->sub_cate_id=$request->sub_category;
        $item->creator_id=Auth::user()->id;
        $item->save();
        $item_id=$item->item_id;

        $iemp_price=ItemPrice::create([
            'item_id'=>$item_id,
            'unit_price'=>$request->unit_price,
            'creator_id'=>Auth::user()->id
        ]);
        

        //return response()->json($request->unit_price);



        DB::commit();


        //upload image 
        if($request->hasFile('photos'))
        {
            $allowedfileExtension=['jpeg','jpg','png','gif'];
            $files = $request->file('photos');
            foreach($files as $file){
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check=in_array($extension,$allowedfileExtension);
            //dd($check);
                if($check)
                {
                        //$filename = pathinfo($filename, PATHINFO_FILENAME);
                        $filenametostore = uniqid().'.'.$extension;
                        //get filename with extension
                        Storage::put('public/photos/'. $filenametostore, fopen($file, 'r+'));
                        Storage::put('public/photos/thumbnail/'. $filenametostore, fopen($file, 'r+'));
                        //echo $photo;
                            //Resize image here
                            $bigpath = public_path('storage/photos/'.$filenametostore);
                            $thumbnailpath = public_path('storage/photos/thumbnail/'.$filenametostore);
                            $img = Image::make($thumbnailpath)->resize(79, 99, function($constraint) {
                                $constraint->aspectRatio();
                            });
                            $img->save($thumbnailpath);

                            $imgBig=Image::make($bigpath)->resize(700, 885, function($constraint) {
                                $constraint->aspectRatio();
                            });
                            $imgBig->save($bigpath);
                            
                            
                    
                    ItemDetail::create([
                        'item_id' => $item_id,
                        'filename' => $filenametostore
                    ]);
                    
                
            }
            else
            {
                echo '<div class="alert alert-warning"><strong>Warning!</strong> Sorry Only Upload png , jpg , doc</div>';
            }
        }

    }
   
    //echo "Upload Successfully";
    return back()->with('success', 'Your images has been successfully');
}

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category=\App\Category::all();
        $sub_category=\App\SubCategory::all();
        $products=Item::with(['item_detail','items_price'])->where('item_id',$id)->first();
        return view('backend.products.edit',['product'=>$products,'category'=>$category,'sub_category'=>$sub_category]);
        //return response()->json($products);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $item=Item::find($id);
        $item->item_code=$request->item_code;
        $item->item_name_en=$request->item_name_en;
        $item->descriptions_kh=$request->descriptions_kh;
        $item->descriptions_en=$request->descriptions_en;
        $item->descriptions_vn=$request->descriptions_vn;
        $item->descriptions_ch=$request->descriptions_ch;
        $item->brand=$request->brand;
        $item->tag=$request->tag;
        $item->cate_id=$request->category;
        $item->sub_cate_id=$request->sub_category;
        $item->updator_id=Auth::user()->id;
        $item->save();
        DB::commit();

        if($request->hasFile('photos'))
        {
            $allowedfileExtension=['jpeg','jpg','png','gif'];
            $files = $request->file('photos');
            foreach($files as $file)
            {
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check=in_array($extension,$allowedfileExtension);
            //dd($check);
                if($check)
                {
                        //$filename = pathinfo($filename, PATHINFO_FILENAME);
                        $filenametostore = uniqid().'.'.$extension;
                        //get filename with extension
                        Storage::put('public/photos/'. $filenametostore, fopen($file, 'r+'));
                        Storage::put('public/photos/thumbnail/'. $filenametostore, fopen($file, 'r+'));
                        //echo $photo;
                            //Resize image here
                            $bigpath = public_path('storage/photos/'.$filenametostore);
                            $thumbnailpath = public_path('storage/photos/thumbnail/'.$filenametostore);
                            $img = Image::make($thumbnailpath)->resize(79, 99, function($constraint) {
                                $constraint->aspectRatio();
                            });
                            $img->save($thumbnailpath);

                            $imgBig=Image::make($bigpath)->resize(700, 885, function($constraint) {
                                $constraint->aspectRatio();
                            });
                            $imgBig->save($bigpath);
                            
                            
                    
                    ItemDetail::create([
                        'item_id' => $id,
                        'filename' => $filenametostore
                    ]);
                    
                
                }
                else
                {
                    echo '<div class="alert alert-warning"><strong>Warning!</strong> Sorry Only Upload png , jpg , doc</div>';
                }
            }
        }
        return back()->with('success', 'Your images has been successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disable($id)
    {
        $item=Item::find($id);
        $item->d_status=0;
        $item->save();
        return back()->with('success', 'You Product has been disabled');
    }

    public function new_item_price(Request $request,$item_id)
    {
        $data=['d_status'=>0];
        ItemPrice::where('item_id',$item_id)->update($data); // update old to inactive
        
        $item=new ItemPrice();
        $item->unit_price=$request->price;
        $item->item_id=$item_id;
        $item->save();
        return back()->with('success', 'Current Price for this item is added');
    }

    public function disable_detail($id)
    {
        ItemDetail::where('id',$id)->delete();
        return back()->with('success', 'Your images has been deleted successfully');
    }
}

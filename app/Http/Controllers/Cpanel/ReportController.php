<?php

namespace App\Http\Controllers\Cpanel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Exports\UserExport;
use Excel;
//use Maatwebsite\Excel\Facades\Excel;
class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientOrder = $this->getClientOrderByDate(date('Y-m-d'), date('Y-m-d'));
        return view('backend.reports.index', ['clientorder' => $clientOrder]);
    }

    function searchOrderReport($frdt, $todt)
    {
        $clientOrder = $this->getClientOrderByDate($frdt, $todt);
        return view('backend.reports.index', ['clientorder' => $clientOrder]);
    }

    function getClientOrderByDate($frdt,  $todt)
    {
        $clientOrder = DB::select(
            " select * from `cus_order` 
                inner join `cus_orderdetail` 
                on `cus_orderdetail`.`order_id` = `cus_order`.`order_id` 
                inner join `pr_item` 
                on `pr_item`.`item_id` = `cus_orderdetail`.`item_id` 
                inner join `front_user` 
                on `front_user`.`account_id` = `cus_order`.`account_id` 
                where `order_date` between '{$frdt}' and '{$todt}'"
        );
        
        return $clientOrder;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function download(Request $request, $frdt, $todt)
    {
        $data = $this->getClientOrderByDate($frdt, $todt);
        return Excel::download(new UserExport($frdt,$todt,$data), 'clientorder_report '.$frdt.'.xlsx'); 
    }

}

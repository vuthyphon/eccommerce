<?php

namespace App\Http\Controllers\Cpanel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserRole;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    function index(){
        $role = DB::table('sys_role')->get();
        //return response()->json($role, 200);
       return view('backend.user_role.index', ['role'=>$role]);
    }
}

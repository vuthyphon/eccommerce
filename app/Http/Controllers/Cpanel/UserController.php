<?php

namespace App\Http\Controllers\Cpanel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\UserRole;
use Hash;
use Validator;



class UserController extends Controller
{
   
    public function index()
    {
       $users = User::with('user_role:role_id,role_name')->get();
       //return response()->json($users[0]['user_role']['role_name'], 200);
       return view('backend.users.index',['users'=>$users]);
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'username' => 'required|unique:users|min:3',
            'email' => 'required|email',
            'password'=>'required|min:6'
        ])->validate();

        $user=new User();
        $user->username=$request->input('username');
        $user->email=$request->input('email');
        $user->password=Hash::make($request->input('password'));
        $user->role_id=$request->input('role');
        $user->save();

        $request->session()->flash('message','<div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Users Information added!</strong>.
      </div>');
        return redirect('cp/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function new()
    {
        $role=UserRole::all();
        return view('backend.users.new',['role'=>$role]);
    }

    public function edit($id)
    {
        $users=User::find($id);
        $role=UserRole::all();
        return view('backend.users.edit',['users'=>$users,'role'=>$role]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = $request->get('us_id');
        Validator::make($request->all(), [
            'username' => 'required|min:3',
            'email' => 'required|email',
        ])->validate();

        $data=array(
            'username'=>$request->input('username'),
            'email'=>$request->input('email'),
            'role_id'=>$request->input('role'),
            'active'=>$request->input('active')
        );
       if($request->get('password')!="")
        {
            $data['password']=Hash::make($request->get('password'));
            Validator::make($request->all(), [
                'password' => 'required|min:6',
                ])->validate();
        }
        
        User::where('id', $id)->update($data);

        $request->session()->flash('message','<div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Users Information updated!</strong>.
      </div>');
        return redirect('cp/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disable(Request $request,$id)
    {
        $user=User::find($id);
        $user->active=0;
        $user->save();
        $request->session()->flash('message','<div class="alert alert-warning">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Users has disabled!</strong>.
      </div>');
        return redirect('cp/users');
    }
}

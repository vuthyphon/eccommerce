<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$newproduct = $this->newproduct();
        $newproduct=Item::with(['item_price:item_id,unit_price','feature_image:item_id,filename'])->orderBy('created_at','DESC')->where('d_status',1)->limit(8)->get();
        $bestseller = $this->bestseller();
        $computer = $this->computer();
        $monitor = $this->monitor();
        $networking = $this->networking();
        $camera = $this->camera();
        $electronics = $this->electronics();
        $mycart=array();
        if(session('usr.account_id'))
        {
            //$cond=(session('usr.account_id')==""?'':' and account_id='.session('usr.account_id'));
        
            $mycart = DB::select("
                select 
                    *
                from fishing_cart
                where cart_status='p'
                and account_id=".session('usr.account_id')
                
            ); 
        }
        return view('frontend.pages.home', compact('newproduct','bestseller','computer','monitor','networking','camera','electronics','mycart'));
    }

    public function bestseller()
    {
        $bestseller=Item::with(['item_price:item_id,unit_price','feature_image:item_id,filename'])->orderBy('item_view','desc')->where('d_status',1)->limit(8)->get();
        return $bestseller;
    }

    function computer()
    {
        $bestseller = Item::with(['item_price:item_id,unit_price','feature_image:item_id,filename'])->where('cate_id',1)->orderBy('created_at','desc')->where('d_status',1)->limit(8)->get();
        return $bestseller;
    }

    function monitor()
    {
        $monitor = Item::with(['item_price:item_id,unit_price','feature_image:item_id,filename'])->where('cate_id',2)->orderBy('created_at','desc')->where('d_status',1)->limit(8)->get();
        return $monitor;
    }

    function networking()
    {
        $networking = Item::with(['item_price:item_id,unit_price','feature_image:item_id,filename'])->where('cate_id',4)->orderBy('created_at','desc')->where('d_status',1)->limit(8)->get();
        return $networking;
    }

    function camera()
    {
        $camera = Item::with(['item_price:item_id,unit_price','feature_image:item_id,filename'])->where('cate_id',4)->orderBy('created_at','desc')->where('d_status',1)->limit(8)->get();
        return $camera;
    }

    function electronics()
    {
        $electronics = Item::with(['item_price:item_id,unit_price','feature_image:item_id,filename'])->where('cate_id',4)->orderBy('created_at','desc')->where('d_status',1)->limit(8)->get();
        return $electronics;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $newproduct=Item::with(['item_price:item_id,unit_price','item_detail:item_id,filename'])->where('item_id',$id)->first();
        return response()->json($newproduct);
    }
    
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

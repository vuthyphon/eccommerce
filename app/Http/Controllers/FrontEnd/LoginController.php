<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\FrontEndModel\Frontuser;
use Hash;

class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $mycart=array();
        if(session('usr.account_id'))
        {
            //$cond=(session('usr.account_id')==""?'':' and account_id='.session('usr.account_id'));
        
            $mycart = DB::select("
                select 
                    *
                from fishing_cart
                where cart_status='p'
                and account_id=".session('usr.account_id')
                
            ); 
        }
        return view('frontend.pages.signin',['mycart'=>$mycart]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function login(Request $request)
    {
        $password = md5($request->password);
        $email = $request->e_mail;
        $result = Frontuser::where(array('e_mail'=>$email, 'uspassword'=>$password, 'is_activate'=>1))->first();
        
        if($result)
        {
            $request->session()->put('usr', $result);
            $usr = session('usr');
            return redirect('mycart');
        }
        else
        {
            $request->session()->flash('message','<div class="alert alert-danger">
                <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Invalid e-mail and password! Please try again.</strong>.
            </div>');
            return (redirect('signin'));
        }

    }
    
    function loginbyajax(Request $request)
    {
        $password = md5($request->password);
        $email = $request->e_mail;
        $result = Frontuser::where(array('e_mail'=>$email, 'uspassword'=>$password, 'is_activate'=>1))->first();
        
        if($result)
        {
            $request->session()->put('usr', $result);
            $usr = session('usr');
            echo 'LoginOk';
        }
        else
        {
            echo '<div class="alert alert-danger">
                    <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Invalid e-mail and password! Please try again.</strong>.
                </div>';
        }
    }
    
    function logout(Request $request)
    {
        $request->session()->forget(['usr']);
        return (redirect('signin'));
    }

}

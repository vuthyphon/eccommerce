<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Item;
use App\SubCategory;
use App\Category;

use App\FrontEndModel\Product;

class ProductController extends Controller
{
    public function show()
    {
        $mycart = $this->getItemCart();  
        $product=Item::with(['item_price:item_id,unit_price','feature_image:item_id,filename'])->where('d_status',1)->paginate(9);
        return view('frontend.pages.product',['product'=>$product, 'mycart'=>$mycart]);
    }

    public function sub_category($id)
    {
        $mycart = $this->getItemCart();  
        $data=SubCategory::select('cate_id','sub_cate_name')->find($id);
        if($data)
        {
            $sub_category=$data->sub_cate_name;
            $category=Category::select('cate_name','cate_id')->find($data->cate_id);
            $cate=$category->cate_name;
            $cate_id = $category->cate_id;
        }
        else{
            $sub_category=null;
            $cate=null;
        }
        
        $product=Item::with(['item_price:item_id,unit_price','feature_image:item_id,filename'])->where('sub_cate_id',$id)->paginate(9);
        return view('frontend.pages.product',['product'=>$product,'sub_category'=>$sub_category,'category'=>$cate,'cate_id'=>$cate_id,'mycart'=>$mycart]);
    }

    public function category($id)
    {
          $mycart = $this->getItemCart();  
        $category=Category::select('cate_name', 'cate_id')->find($id);
        if($category)
        {
            $cate=$category->cate_name;
            $cate_id = $category->cate_id;
        }
        else{
            $cate=null;
        }
        $product=Item::with(['item_price:item_id,unit_price','feature_image:item_id,filename'])->where(array('cate_id'=>$id,'d_status'=>1))->paginate(9);
        return view('frontend.pages.product',['product'=>$product,'category'=>$cate,'cate_id'=>$cate_id,'mycart'=>$mycart]);
    }

    /**
     * Dispaly product when user find product-
     * name in search box 
     */
    public function search(Request $request)
    {
        $mycart = $this->getItemCart();  
        
        $name=$request->filter;
        $product=Item::with(['item_price:item_id,unit_price','feature_image:item_id,filename'])->where('item_name_en','like','%'.$name.'%')->paginate(9);
        return view('frontend.pages.product_search',['product'=>$product, 'mycart'=>$mycart]);
    }

   
    public function detail($id=null)
    {
        $mycart = $this->getItemCart();  
        DB::table('pr_item')->where('item_id',$id)->increment('item_view');
        $product=Item::with(['item_price:item_id,unit_price','item_detail:item_id,filename,id'])->where('item_id',$id)->first();
        $related_product=Item::with(['item_price:item_id,unit_price','feature_image:item_id,filename,id'])->where('sub_cate_id',$product->sub_cate_id)->limit(8)->get();
        $category=Category::select('cate_name', 'cate_id')->find($product->cate_id);
        $subcate=SubCategory::select('sub_cate_name', 'sub_cateid')->find($product->sub_cate_id);

        return view('frontend.pages.productdetail',['product'=>$product,'related_product'=>$related_product,'category'=>$category,'subcate'=>$subcate, 'mycart'=>$mycart]);
        //return response()->json(['product'=>$product,'related_product',$related_product]);
    }

    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    function getItemCart()
    {
        $cond=(!session('usr.account_id')?' and `fishing_cart`.`account_id`=-1':' and `fishing_cart`.`account_id`='.session('usr.account_id'));
        $myCart = DB::select("
                select 
                    cart_id,
                    fishing_cart.item_id,
                    filename,
                    item_name_en,
                    unit_price,
                    quantity,
                    (unit_price * quantity) total
                from `fishing_cart` 
                inner join `front_user` 
                on `fishing_cart`.`account_id` = `front_user`.`account_id` 
                inner join `pr_item` 
                on `fishing_cart`.`item_id` = `pr_item`.`item_id` 
                inner join `pr_item_price` 
                on `pr_item_price`.`item_id` = `pr_item`.`item_id`
                and pr_item_price.price_id = (select max(price_id) from pr_item_price a where a.item_id = fishing_cart.item_id)
                left join pr_item_details pic
                on pic.item_id = fishing_cart.item_id
                and pic.id = (select max(id) from pr_item_details b where b.item_id = fishing_cart.item_id)
                where fishing_cart.cart_status='p' {$cond}"); 
        return $myCart;
    }
}

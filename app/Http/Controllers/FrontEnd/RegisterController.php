<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Hash;
use App\FrontEndModel\Frontuser;
use Illuminate\Support\Facades\Mail;
use App\Mail\VerifyRegister;

class RegisterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return redirect($this->create());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mycart=array();
        if(session('usr.account_id'))
        {
            //$cond=(session('usr.account_id')==""?'':' and account_id='.session('usr.account_id'));
        
            $mycart = DB::select("
                select 
                    *
                from fishing_cart
                where cart_status='p'
                and account_id=".session('usr.account_id')
                
            ); 
        }
        return view('frontend.pages.register',['mycart'=>$mycart]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if($request->input('uspassword')==$request->input('reuspassword'))
        {
            DB::beginTransaction();
            $frontuser = new Frontuser();
            $frontuser->first_name = $request->input('first_name');
            $frontuser->acc_no=uniqid();
            $frontuser->last_name = $request->input('last_name');
            $frontuser->e_mail = $request->input('e_mail');
            $frontuser->uspassword = md5($request->input('uspassword'));
            $frontuser->tel = $request->input('tel');
            $frontuser->address = $request->input('address');
            $frontuser->save();
            DB::commit();

            $request->session()->flash('message','<div class="alert alert-success">
                <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Your account has been created successfully.<br> You will recieve an email to confirm your registration. <b>please go to activate your account now. </strong>.
            </div>');
            $link=url('register/activate/'.$frontuser->acc_no);
            $emailData=array('url'=>$link,"to"=>$frontuser->e_mail);

            $this->mail($emailData);
        }
        else
        {
            $request->session()->flash('message','<div class="alert alert-danger">
                <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Re-Password does not match! Please try again.</strong>.
            </div>');
        }

        return (redirect('register'));
    }
    
    public function mail($data)
    {
        Mail::to($data['to'])->send(new VerifyRegister($data));
       // Mail::to('krunal@appdividend.com')->send(new SendMail($message));
        
        //return 'Email was sent';
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    function activate_account(Request $request,$user_code)
    {
        Frontuser::where('acc_no',$user_code)->update(array('is_activate'=>1));
        $request->session()->flash('message','<div class="alert alert-success">
                <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Your account has been activated successfully</strong>.
            </div>');
       return (redirect('register'));
    }
    
}

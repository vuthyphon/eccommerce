<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

//use App\FrontEndModel\Cart;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $myCart = $this->getItemCart();              
        return view('frontend.pages.mycart',['mycart'=>$myCart]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Get item cart 
     * goes below
     */

    function getItemCart()
    {
        $cond=(!session('usr.account_id')?' and `fishing_cart`.`account_id`=-1':' and `fishing_cart`.`account_id`='.session('usr.account_id'));
        $myCart = DB::select("
                select 
                    cart_id,
                    fishing_cart.item_id,
                    filename,
                    item_name_en,
                    unit_price,
                    quantity,
                    (unit_price * quantity) total
                from `fishing_cart` 
                inner join `front_user` 
                on `fishing_cart`.`account_id` = `front_user`.`account_id` 
                inner join `pr_item` 
                on `fishing_cart`.`item_id` = `pr_item`.`item_id` 
                inner join `pr_item_price` 
                on `pr_item_price`.`item_id` = `pr_item`.`item_id`
                and pr_item_price.price_id = (select max(price_id) from pr_item_price a where a.item_id = fishing_cart.item_id)
                left join pr_item_details pic
                on pic.item_id = fishing_cart.item_id
                and pic.id = (select max(id) from pr_item_details b where b.item_id = fishing_cart.item_id)
                where fishing_cart.cart_status='p' {$cond}"); 
        return $myCart;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * 
     * When user add product to cart
     */
    public function store(Request $request, $id)
    {
        
        if($request->session()->has('usr'))
        {

            // need to check duplicate pending item added by user
            // if duplocate, alert to client, else perform adding.

            $countrow = DB::table('fishing_cart')
                ->where([
                    ['account_id', session('usr.account_id')],
                    ['item_id', $id],
                    ['cart_status', 'p']
                ])
                ->count();

            if($countrow==0)
            {
                DB::table('fishing_cart')->insert([
                    'account_id' => session('usr.account_id'), 
                    'item_id' => $id,
                    'quantity'=> '1',
                    'cart_status' => 'p'
                ]);

                echo '<div class="alert alert-info" style="border-radius:0;">
                    <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Product has been added to your cart.</strong>.
                </div>';
                //echo 'Product has been added to your cart.';
            }    
            else
            {
                echo '<div class="alert alert-warning" style="border-radius:0;">
                    <a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>This product already exists in your cart.</strong>.
                </div>';
                //echo 'This product already exists in your cart.';
            }
        }
        
        else
        {
            echo 'login';
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove item from cart goes below
     * Remove by Ajax technology
     */

    function remove_itemcart(Request $request, $id)
    {
        DB::table('fishing_cart')
            ->where('cart_id',$id)
            ->update(['cart_status'=>'r']);

        $myCart = $this->getItemCart();  
        
        if($myCart)
        {
            $totalItem = 0;
            $totalPrice = 0;
            foreach($myCart as $item)
            {
                $totalItem+=$item->quantity;
                $totalPrice+=$item->total;

                echo '<tr id="cart_'.$item->cart_id.'">
                <td class="product-thumbnail">'.
                    (($item->filename!=''?'
                    <img class="primary-img" src="'.asset('storage/photos/'.$item->filename).'" alt="single-product">
                    ':'
                    <img class="primary-img" src="'.asset('frontend/img/no-image.png').'" alt="single-product">
                    ')).
                '</td>
                <td class="product-name"><a href="javascript:void(0)">'.$item->item_name_en.'</a></td>
                <td class="product-price"><span class="amount">'.$item->unit_price.'</span></td>
                <td class="product-quantity"><input type="number" min="1" max="100" value="'.$item->quantity.'" onblur="updatecart('.$item->cart_id.', this.value)"></td>
                <td class="product-subtotal">'.$item->total.'</td>
                <td class="product-remove"> <a onclick="removecart('.$item->cart_id.')" href="javascript:void(0)"><i class="zmdi zmdi-close"></i></a></td>
            </tr>';

            }

            echo '<tr style="font-weight:bold;">
                    <td colspan="3">Total:</td>
                    <td class="product-price">'.number_format($totalItem).'</td>
                    <td class="product-price">$ '.number_format($totalPrice,2).'</td>
                    <td class="product-remove"></td>
                </tr>';
        }
    }

    /**
     * Update item qty in cart
     */

    function update_itemcart(Request $request, $id, $quantity)
    {
        DB::table('fishing_cart')
            ->where('cart_id',$id)
            ->update(['quantity'=>$quantity]);

        $myCart = $this->getItemCart();  
        
        if($myCart)
        {
            $totalItem = 0;
            $totalPrice = 0;
            foreach($myCart as $item)
            {
                $totalItem+=$item->quantity;
                $totalPrice+=$item->total;

                echo '<tr id="cart_'.$item->cart_id.'">
                <td class="product-thumbnail">'.
                    (($item->filename!=''?'
                    <img class="primary-img" src="'.asset('storage/photos/'.$item->filename).'" alt="single-product">
                    ':'
                    <img class="primary-img" src="'.asset('frontend/img/no-image.png').'" alt="single-product">
                    ')).
                '</td>
                <td class="product-name"><a href="javascript:void(0)">'.$item->item_name_en.'</a></td>
                <td class="product-price"><span class="amount">'.$item->unit_price.'</span></td>
                <td class="product-quantity"><input type="number" min="1" max="100" value="'.$item->quantity.'" onblur="updatecart('.$item->cart_id.', this.value)"></td>
                <td class="product-subtotal">'.$item->total.'</td>
                <td class="product-remove"> <a onclick="removecart('.$item->cart_id.')" href="javascript:void(0)"><i class="zmdi zmdi-close"></i></a></td>
            </tr>';

            }

            echo '<tr style="font-weight:bold;">
                    <td colspan="3">Total:</td>
                    <td class="product-price">'.number_format($totalItem).'</td>
                    <td class="product-price">$ '.number_format($totalPrice,2).'</td>
                    <td class="product-remove"></td>
                </tr>';

        }
    }


    function checkout()
    {
        $myCart = $this->getItemCart();
        return view('frontend.pages.checkout',['mycart'=>$myCart]);
    }

    function addCheckOut(Request $request)
    {
        if($request->session()->has('usr')==false)
        {
            return (redirect('checkout'));
        }

        DB::beginTransaction();
        $myCart = $this->getItemCart();

        if($myCart)
        {
            $orderId = DB::table('cus_order')->insertGetId(
                [
                    'account_id' => session('usr.account_id'), 
                    'order_date' => date('Y-m-d'),
                    'ref' => $request->input('difaddress'),
                    'order_note' => $request->input('order_note')
                ]
            );

            foreach ($myCart as $key) {
                DB::table('cus_orderdetail')->insert(
                    [
                        'order_id' => $orderId, 
                        'item_id' => $key->item_id,
                        'qty' => $key->quantity,
                        'unit_price' => $key->unit_price
                    ]
                );

                DB::table('fishing_cart')
                    ->where('cart_id',$key->cart_id)
                    ->update(['cart_status'=>'a']);
            }

        }
        
        DB::commit();

        return (redirect('mycart'));
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table="pr_item";
    public $primaryKey="item_id";

    protected $hidden = [
        'created_at','updated_at','variant','creator_id','deletor_id'
    ];

    protected $fillable=[
        'item_id','item_code','tag','brand','sub_cate_id','cate_id','d_status','item_name_en','descriptions_en','descriptions_vn','descriptions_ch','descriptions_en'
    ];

    public function category(){
        return $this->hasOne('App\Category', 'cate_id', 'cate_id');
    }

    public function sub_category(){
        return $this->hasOne('App\SubCategory', 'sub_cateid', 'sub_cate_id');
    }

    public function user(Type $var = null)
    {
        return $this->hasOne('App\User', 'id', 'creator_id');
    }

    public function item_price()
    {
        return $this->hasOne('App\ItemPrice', 'item_id', 'item_id')->where('d_status',1);
    }

    public function item_detail()
    {
        return $this->hasMany('App\ItemDetail', 'item_id', 'item_id');
    }

    public function items_price()
    {
        return $this->hasMany('App\ItemPrice', 'item_id', 'item_id');
    }

    public function feature_image()
    {
        return $this->hasOne('App\ItemDetail', 'item_id', 'item_id');
    }

    
}

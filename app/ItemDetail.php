<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemDetail extends Model
{
    protected $table="pr_item_details";
    public $primaryKey="id";

    protected $hidden = [
        'created_at','updated_at'
    ];

    protected $fillable=[
        'item_id','filename'
    ];

    public function item()
    {
        return $this->belongsTo('App\Item', 'item_id','item_id');
    }
}

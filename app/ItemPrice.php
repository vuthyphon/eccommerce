<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemPrice extends Model
{
    protected $table="pr_item_price";
    public $primaryKey="price_id";

    protected $hidden = [
        'updated_at','effective_date','d_status','creator_id','deletor_id','item_id'
    ];

    protected $fillable=[
        'item_id','effective_date','unit_price','creator_id','deletor_id'
    ];

    public function item(Type $var = null)
    {
        return $this->belongsTo('App\Item', 'item_id','item_id');
    }
}

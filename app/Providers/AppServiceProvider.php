<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

use Session;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $myCart = array();
        if(session('usr.account_id'))
        {
            //$cond=(session('usr.account_id')==""?'':' and account_id='.session('usr.account_id'));
        
            $myCart = DB::select("
                select 
                    *
                from fishing_cart
                where cart_status='p'
                and account_id=".session('usr.account_id')
                
            ); 
        }
        
        $setting=\App\Setting::first();
        
        $data = array(
            'countcart' => $myCart,
            'setting' => $setting,
        );
        
        View::share('data',$data);
        
        //View::share(['setting', $setting],['countcart',$myCart]);
    }
}

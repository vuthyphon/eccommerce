<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table="sys_setting";
    public $primaryKey="id";

    protected $hidden = [
        'created_at','updated_at'
    ];

    protected $fillable=[
        'item_id','youtube_link','fb_link','mobile','telephone','email','website','instagram_link'
    ];
}

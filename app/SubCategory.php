<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
     protected $table="pr_subcategory";
     public $primaryKey="sub_cateid";

     
     protected $hidden = [
         'created_at','updated_at'
     ];

     public function category()
     {
        return $this->belongsTo('App\SubCategory', 'cate_id','cate_id');
     }

     public function item()
     {
         return $this->belongsTo('App\Item', 'sub_cate_id','sub_cateid');
     }
}

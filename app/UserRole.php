<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table="sys_role";
    public $primaryKey="role_id";

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    
    protected $hidden = [
        'role_id','created_at','updated_at'
    ];

    public function user()
    {
        return $this->hasOne('App\User', 'role_id', 'role_id');
    }
}

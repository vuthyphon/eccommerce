<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSubcategroy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pr_subcategroy', function (Blueprint $table) {
            $table->increments('sub_cateid');
            $table->integer('cate_id');
            $table->string('sub_cate_name');
            $table->integer('creator_id')->unsigned()->nullable();
            $table->integer('updator_id')->unsigned()->nullable();
            $table->integer('deletor_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pr_subcategroy');
    }
}

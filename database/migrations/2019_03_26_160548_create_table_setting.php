<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSetting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_setting', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fb_link')->null();
            $table->string('twitter_link')->null();
            $table->string('youtube_link')->null();
            $table->string('website_link')->null();
            $table->string('email_link')->null();
            $table->string('telephone')->null();
            $table->string('mobile')->null();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_setting');
    }
}

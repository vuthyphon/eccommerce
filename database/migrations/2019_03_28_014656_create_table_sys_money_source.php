<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSysMoneySource extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sys_money_source', function (Blueprint $table) {
            $table->increments('id');
            $table->string('money_source');
            $table->string('money_source_en')->null();
            $table->integer('creator_id')->null();
            $table->integer('updator_id')->null();
            $table->integer('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sys_money_source');
    }
}

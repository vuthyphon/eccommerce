<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Category;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
        Category::insert([
            [ 
                'cate_id'=>1,
                'cate_name' => 'Computer',
                'creator_id' => 1
            ],
            [ 
                'cate_id'=>2,
                'cate_name' => 'Computer Acessary',
                'creator_id' => 1
            ],
            [   
                'cate_id'=>3,
                'cate_name' => 'Monitor',
                'creator_id' => 1
            ],
            [   
                'cate_id'=>4,
                'cate_name' => 'Network',
                'creator_id' => 1
            ],
            [ 
                'cate_id'=>5,
                'cate_name' => 'Camera Security',
                'creator_id' => 1
            ],
            [ 
                'cate_id'=>6,
                'cate_name' => 'Printer',
                'creator_id' => 1
            ],
            [ 
                'cate_id'=>7,
                'cate_name' => 'Accessary',
                'creator_id' => 1
            ]
        ]);
           
    }
}

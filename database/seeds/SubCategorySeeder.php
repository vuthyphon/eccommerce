<?php

use Illuminate\Database\Seeder;
use App\SubCategory;

class SubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SubCategory::insert([
            [
                'cate_id'=>1,
                'sub_cate_name'=>'Laptop'
            ],
            [
                'cate_id'=>1,
                'sub_cate_name'=>'Desktop'
            ],
            [
                'cate_id'=>2,
                'sub_cate_name'=>'CPU'
            ],
            [
                'cate_id'=>2,
                'sub_cate_name'=>'RAM'
            ],
            [
                'cate_id'=>2,
                'sub_cate_name'=>'HDD/SSD'
            ],
            [
                'cate_id'=>2,
                'sub_cate_name'=>'Motherboard'
            ],
            [
                'cate_id'=>2,
                'sub_cate_name'=>'Case'
            ],
            [
                'cate_id'=>2,
                'sub_cate_name'=>'PowerSupply'
            ],
            [
                'cate_id'=>2,
                'sub_cate_name'=>'UPS'
            ],
            [
                'cate_id'=>3,
                'sub_cate_name'=>'IN Panel'
            ],
            [
                'cate_id'=>3,
                'sub_cate_name'=>'IPS Panel'
            ],
            [
                'cate_id'=>4,
                'sub_cate_name'=>'Router'
            ],
            [
                'cate_id'=>4,
                'sub_cate_name'=>'Switch'
            ],
            [
                'cate_id'=>4,
                'sub_cate_name'=>'Access Point'
            ],
            [
                'cate_id'=>4,
                'sub_cate_name'=>'USB Wireless'
            ],
            [
                'cate_id'=>4,
                'sub_cate_name'=>'PCI wireless case'
            ],
            [
                'cate_id'=>5,
                'sub_cate_name'=>'IP Camera'
            ],
            [
                'cate_id'=>5,
                'sub_cate_name'=>'Camera'
            ],
            [
                'cate_id'=>5,
                'sub_cate_name'=>'DVR/NVR'
            ],
            [
                'cate_id'=>5,
                'sub_cate_name'=>'Power Supply'
            ],
            [
                'cate_id'=>5,
                'sub_cate_name'=>'Camera Cable'
            ],
            [
                'cate_id'=>6,
                'sub_cate_name'=>'Network Printer'
            ],
            [
                'cate_id'=>6,
                'sub_cate_name'=>'USB Printer'
            ],
            [
                'cate_id'=>6,
                'sub_cate_name'=>'Toner'
            ],
            [
                'cate_id'=>6,
                'sub_cate_name'=>'Ink'
            ],
            [
                'cate_id'=>6,
                'sub_cate_name'=>'Ribbon'
            ],
            [
                'cate_id'=>7,
                'sub_cate_name'=>'Mouse'
            ],
            [
                'cate_id'=>7,
                'sub_cate_name'=>'Keyboard'
            ],
            [
                'cate_id'=>7,
                'sub_cate_name'=>'Mouse pad'
            ],
            [
                'cate_id'=>7,
                'sub_cate_name'=>'Webcam'
            ],
            [
                'cate_id'=>7,
                'sub_cate_name'=>'Connector'
            ],
        ]);
    }
}

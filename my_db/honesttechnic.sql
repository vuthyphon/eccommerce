-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 26, 2019 at 04:53 PM
-- Server version: 5.7.24
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `honesttechnic`
--

-- --------------------------------------------------------

--
-- Table structure for table `cus_order`
--

CREATE TABLE `cus_order` (
  `order_id` int(15) NOT NULL,
  `account_id` int(11) DEFAULT '0',
  `order_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `ref` varchar(255) DEFAULT '',
  `discount` float DEFAULT '0',
  `data_status` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cus_orderdetail`
--

CREATE TABLE `cus_orderdetail` (
  `order_detail_id` int(15) NOT NULL,
  `order_id` int(15) NOT NULL,
  `item_id` int(15) NOT NULL,
  `price_id` int(11) NOT NULL DEFAULT '0',
  `qty` float DEFAULT '0',
  `unit_price` float DEFAULT '0',
  `data_status` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `front_user`
--

CREATE TABLE `front_user` (
  `account_id` int(11) NOT NULL,
  `first_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `uspassword` varchar(255) NOT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `e_mail` varchar(255) DEFAULT NULL,
  `cur_address` varchar(255) DEFAULT NULL,
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `is_activate` int(1) DEFAULT '0',
  `data_status` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `img_main_slide`
--

CREATE TABLE `img_main_slide` (
  `imgsld_id` int(11) NOT NULL,
  `img_dir` varchar(500) DEFAULT NULL,
  `img_alt` varchar(255) DEFAULT NULL,
  `slid_title1_en` varchar(255) DEFAULT NULL,
  `slid_title2_en` varchar(255) DEFAULT NULL,
  `slid_title3_en` varchar(255) DEFAULT NULL,
  `slid_title1_ch` varchar(255) DEFAULT NULL,
  `slid_title2_ch` varchar(255) DEFAULT NULL,
  `slid_title3_ch` varchar(255) DEFAULT NULL,
  `slid_title1_vn` varchar(255) DEFAULT NULL,
  `slid_title2_vn` varchar(255) DEFAULT NULL,
  `slid_title3_vn` varchar(255) DEFAULT NULL,
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `input_by` int(11) DEFAULT '0',
  `delete_by` int(11) DEFAULT '0',
  `d_status` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `img_under_slide`
--

CREATE TABLE `img_under_slide` (
  `imgsld_id` int(11) NOT NULL,
  `img_alt` varchar(255) DEFAULT NULL,
  `img_dir` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_02_15_032017_create_table_role', 2),
(4, '2019_03_23_071544_category_table', 3),
(5, '2019_03_23_073058_create_table_subcategroy', 4),
(6, '2019_03_24_085116_create_table_item_detail', 5),
(7, '2019_03_26_160548_create_table_setting', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pr_category`
--

CREATE TABLE `pr_category` (
  `cate_id` int(10) UNSIGNED NOT NULL,
  `cate_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `updator_id` int(10) UNSIGNED DEFAULT NULL,
  `deletor_id` int(10) UNSIGNED DEFAULT NULL,
  `active` int(10) UNSIGNED DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pr_category`
--

INSERT INTO `pr_category` (`cate_id`, `cate_name`, `creator_id`, `updator_id`, `deletor_id`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Computer', 1, NULL, NULL, 1, NULL, NULL),
(2, 'Computer Acessary', 1, NULL, NULL, 1, NULL, NULL),
(3, 'Monitor', 1, NULL, NULL, 1, NULL, NULL),
(4, 'Network', 1, NULL, NULL, 1, NULL, NULL),
(5, 'Camera Security', 1, NULL, NULL, 1, NULL, NULL),
(6, 'Printer', 1, NULL, NULL, 1, NULL, NULL),
(7, 'Accessary', 1, NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pr_item`
--

CREATE TABLE `pr_item` (
  `item_id` int(11) NOT NULL,
  `item_code` varchar(255) NOT NULL,
  `tag` varchar(255) DEFAULT NULL COMMENT 'New Arrival or something',
  `item_name_en` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `item_name_ch` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `item_name_vn` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `descriptions_en` text CHARACTER SET utf8,
  `descriptions_ch` text CHARACTER SET utf8,
  `descriptions_vn` text CHARACTER SET utf8,
  `cate_id` int(11) DEFAULT NULL,
  `sub_cate_id` int(11) DEFAULT NULL,
  `variant` text CHARACTER SET utf8,
  `brand` varchar(255) NOT NULL,
  `item_view` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `updator_id` int(11) DEFAULT NULL,
  `deletor_id` int(11) DEFAULT NULL,
  `d_status` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pr_item`
--

INSERT INTO `pr_item` (`item_id`, `item_code`, `tag`, `item_name_en`, `item_name_ch`, `item_name_vn`, `descriptions_en`, `descriptions_ch`, `descriptions_vn`, `cate_id`, `sub_cate_id`, `variant`, `brand`, `item_view`, `created_at`, `updated_at`, `creator_id`, `updator_id`, `deletor_id`, `d_status`) VALUES
(6, 'A100392431', 'New', 'Acer Swift 1 Quad Core', NULL, NULL, '<p>-&nbsp; Operation System : Boot-up Linux<br>\r\n	-&nbsp; Processor : Intel® Celeron® Quad Core N4100 Processor&nbsp;&nbsp;<br>\r\n	&nbsp; &nbsp;(4M Cache, up to 2.40 GHz) 4 Streats<br>\r\n	-&nbsp; Monitor : 14\" FHD Acer ComfyView IPS LED LCD<br>\r\n	-&nbsp; Graphics : Intel® UHD Graphics<br>\r\n	-&nbsp; Memory : 4GB DDR4 Memory<br>\r\n	-&nbsp; Storage :&nbsp; SSD 256GB<br>\r\n	-&nbsp; Finger Print<br>\r\n	-&nbsp; Optical Drive : NA<br>\r\n	-&nbsp; WLAN/Bluetooth : 802.11ac + BT<br>\r\n	-&nbsp; Camera : HD Camera with 2Mic<br>\r\n	-&nbsp; Weight : 1.3 kg<br>\r\n	-&nbsp; Battery : 3-cell Li-Polymer battery<br>\r\n	-&nbsp; Warrenty : 1-year on spare part and 5-year on services</p>', NULL, NULL, 1, 1, NULL, 'Acer Laptop', 0, '2019-03-25 09:08:11', '2019-03-26 15:46:43', 1, 1, NULL, 1),
(9, 'A0991231', NULL, 'Macbook pro MR9U2 (2018) - i5', NULL, NULL, '<p>- CPU : 2.30 GHz Intel Core i5 Quad-Core&nbsp;<br>\r\n	- RAM : 8GB 2133MHz&nbsp;<br>\r\n	- HDD : 256GB PCIe&nbsp;<br>\r\n	- 13.3\" 2560 x 1600 Retina Display<br>\r\n	- VGA : Intel Iris Plus Graphics 655&nbsp;<br>\r\n	- True Tone Technology<br>\r\n	- 802.11ac Wi-Fi | Bluetooth 5.0<br>\r\n	- Touch Bar | Touch ID Sensor&nbsp;<br>\r\n	- 4 x Thunderbolt 3 (USB Type-C) Ports<br>\r\n	- OS: macOS High Sierra<br>\r\n	- Color : SILVER<br>\r\n	- 1 year warranty</p>', NULL, NULL, 1, 1, NULL, 'Apple', 0, '2019-03-25 09:49:55', '2019-03-25 09:49:55', 1, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pr_item_details`
--

CREATE TABLE `pr_item_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pr_item_details`
--

INSERT INTO `pr_item_details` (`id`, `item_id`, `filename`, `created_at`, `updated_at`) VALUES
(5, 9, '5c98a443b7dc8.jpg', '2019-03-25 02:49:56', '2019-03-25 02:49:56'),
(6, 9, '5c98a4442754c.jpg', '2019-03-25 02:49:56', '2019-03-25 02:49:56'),
(7, 9, '5c98a44476959.jpg', '2019-03-25 02:49:56', '2019-03-25 02:49:56'),
(8, 6, '5c9a46ba53528.jpg', '2019-03-26 08:35:22', '2019-03-26 08:35:22'),
(9, 6, '5c9a47411af34.jpg', '2019-03-26 08:37:37', '2019-03-26 08:37:37');

-- --------------------------------------------------------

--
-- Table structure for table `pr_item_discount`
--

CREATE TABLE `pr_item_discount` (
  `discount_id` int(11) NOT NULL,
  `discount_for` varchar(255) DEFAULT NULL COMMENT 'it value mus be pr_category | pr_item',
  `key_id` int(11) DEFAULT '0',
  `rate` float NOT NULL DEFAULT '0',
  `effective_date` date NOT NULL,
  `end_eff_date` date NOT NULL,
  `input_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `input_by` int(11) NOT NULL DEFAULT '0',
  `delete_by` int(11) NOT NULL DEFAULT '0',
  `d_status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pr_item_price`
--

CREATE TABLE `pr_item_price` (
  `price_id` int(11) NOT NULL,
  `item_id` int(11) DEFAULT '0',
  `unit_price` float DEFAULT '0',
  `effective_date` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `deletor_id` int(11) DEFAULT NULL,
  `d_status` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pr_item_price`
--

INSERT INTO `pr_item_price` (`price_id`, `item_id`, `unit_price`, `effective_date`, `created_at`, `updated_at`, `creator_id`, `deletor_id`, `d_status`) VALUES
(1, 9, 999, NULL, '2019-03-25 09:49:55', '2019-03-26 16:52:46', NULL, NULL, 0),
(2, 9, 1200, NULL, '2019-03-25 09:49:55', '2019-03-26 16:52:46', NULL, NULL, 0),
(6, 9, 1000, NULL, '2019-03-26 16:52:46', '2019-03-26 16:52:46', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pr_subcategory`
--

CREATE TABLE `pr_subcategory` (
  `sub_cateid` int(10) UNSIGNED NOT NULL,
  `cate_id` int(11) NOT NULL,
  `sub_cate_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `updator_id` int(10) UNSIGNED DEFAULT NULL,
  `deletor_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pr_subcategory`
--

INSERT INTO `pr_subcategory` (`sub_cateid`, `cate_id`, `sub_cate_name`, `creator_id`, `updator_id`, `deletor_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'Laptop', NULL, NULL, NULL, NULL, NULL),
(2, 1, 'Desktop', NULL, NULL, NULL, NULL, NULL),
(3, 2, 'CPU', NULL, NULL, NULL, NULL, NULL),
(4, 2, 'RAM', NULL, NULL, NULL, NULL, NULL),
(5, 2, 'HDD/SSD', NULL, NULL, NULL, NULL, NULL),
(6, 2, 'Motherboard', NULL, NULL, NULL, NULL, NULL),
(7, 2, 'Case', NULL, NULL, NULL, NULL, NULL),
(8, 2, 'PowerSupply', NULL, NULL, NULL, NULL, NULL),
(9, 2, 'UPS', NULL, NULL, NULL, NULL, NULL),
(10, 3, 'IN Panel', NULL, NULL, NULL, NULL, NULL),
(11, 3, 'IPS Panel', NULL, NULL, NULL, NULL, NULL),
(12, 4, 'Router', NULL, NULL, NULL, NULL, NULL),
(13, 4, 'Switch', NULL, NULL, NULL, NULL, NULL),
(14, 4, 'Access Point', NULL, NULL, NULL, NULL, NULL),
(15, 4, 'USB Wireless', NULL, NULL, NULL, NULL, NULL),
(16, 4, 'PCI wireless case', NULL, NULL, NULL, NULL, NULL),
(17, 5, 'IP Camera', NULL, NULL, NULL, NULL, NULL),
(18, 5, 'Camera', NULL, NULL, NULL, NULL, NULL),
(19, 5, 'DVR/NVR', NULL, NULL, NULL, NULL, NULL),
(20, 5, 'Power Supply', NULL, NULL, NULL, NULL, NULL),
(21, 5, 'Camera Cable', NULL, NULL, NULL, NULL, NULL),
(22, 6, 'Network Printer', NULL, NULL, NULL, NULL, NULL),
(23, 6, 'USB Printer', NULL, NULL, NULL, NULL, NULL),
(24, 6, 'Toner', NULL, NULL, NULL, NULL, NULL),
(25, 6, 'Ink', NULL, NULL, NULL, NULL, NULL),
(26, 6, 'Ribbon', NULL, NULL, NULL, NULL, NULL),
(27, 7, 'Mouse', NULL, NULL, NULL, NULL, NULL),
(28, 7, 'Keyboard', NULL, NULL, NULL, NULL, NULL),
(29, 7, 'Mouse pad', NULL, NULL, NULL, NULL, NULL),
(30, 7, 'Webcam', NULL, NULL, NULL, NULL, NULL),
(31, 7, 'Connector', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sys_module`
--

CREATE TABLE `sys_module` (
  `module_id` int(11) NOT NULL,
  `module_name_en` varchar(255) NOT NULL,
  `module_name_ch` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `icon_sign` varchar(255) DEFAULT NULL,
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `input_by` int(11) DEFAULT '0',
  `data_status` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sys_role`
--

CREATE TABLE `sys_role` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `role_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `updator_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sys_role`
--

INSERT INTO `sys_role` (`role_id`, `role_name`, `creator_id`, `updator_id`, `created_at`, `updated_at`) VALUES
(1, 'Admin', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sys_rolepermission`
--

CREATE TABLE `sys_rolepermission` (
  `rpm_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `is_view` int(1) DEFAULT '0',
  `is_create` int(1) DEFAULT '0',
  `is_update` int(1) DEFAULT '0',
  `is_delete` int(1) DEFAULT '0',
  `input_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `input_by` int(11) DEFAULT '0',
  `data_status` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sys_setting`
--

CREATE TABLE `sys_setting` (
  `id` int(10) UNSIGNED NOT NULL,
  `fb_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT 'text',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `creator_id` int(10) UNSIGNED DEFAULT NULL,
  `updator_id` int(10) UNSIGNED DEFAULT NULL,
  `deletor_id` int(10) UNSIGNED DEFAULT NULL,
  `active` int(10) UNSIGNED DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `full_name`, `email`, `email_verified_at`, `password`, `role_id`, `creator_id`, `updator_id`, `deletor_id`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Vuthy Phon', 'vuthyphon13@gmail.com', NULL, '$2y$10$0Zb0DomgATy/oBw4U7wzYujn7RQPmcw7sKRus/9o0VcF/Cvo2wuba', 1, 1, 1, 1, 1, NULL, '2019-03-20 07:00:20', '2019-03-20 07:00:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cus_order`
--
ALTER TABLE `cus_order`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `cus_orderdetail`
--
ALTER TABLE `cus_orderdetail`
  ADD PRIMARY KEY (`order_detail_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `price_id` (`price_id`);

--
-- Indexes for table `front_user`
--
ALTER TABLE `front_user`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `img_main_slide`
--
ALTER TABLE `img_main_slide`
  ADD PRIMARY KEY (`imgsld_id`);

--
-- Indexes for table `img_under_slide`
--
ALTER TABLE `img_under_slide`
  ADD PRIMARY KEY (`imgsld_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pr_category`
--
ALTER TABLE `pr_category`
  ADD PRIMARY KEY (`cate_id`),
  ADD UNIQUE KEY `tbl_category_cate_name_unique` (`cate_name`);

--
-- Indexes for table `pr_item`
--
ALTER TABLE `pr_item`
  ADD PRIMARY KEY (`item_id`);

--
-- Indexes for table `pr_item_details`
--
ALTER TABLE `pr_item_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pr_item_discount`
--
ALTER TABLE `pr_item_discount`
  ADD PRIMARY KEY (`discount_id`);

--
-- Indexes for table `pr_item_price`
--
ALTER TABLE `pr_item_price`
  ADD PRIMARY KEY (`price_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `pr_subcategory`
--
ALTER TABLE `pr_subcategory`
  ADD PRIMARY KEY (`sub_cateid`);

--
-- Indexes for table `sys_module`
--
ALTER TABLE `sys_module`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `sys_role`
--
ALTER TABLE `sys_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `sys_rolepermission`
--
ALTER TABLE `sys_rolepermission`
  ADD PRIMARY KEY (`rpm_id`),
  ADD KEY `role_id` (`role_id`),
  ADD KEY `module_id` (`module_id`);

--
-- Indexes for table `sys_setting`
--
ALTER TABLE `sys_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_name_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cus_order`
--
ALTER TABLE `cus_order`
  MODIFY `order_id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cus_orderdetail`
--
ALTER TABLE `cus_orderdetail`
  MODIFY `order_detail_id` int(15) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `front_user`
--
ALTER TABLE `front_user`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `img_main_slide`
--
ALTER TABLE `img_main_slide`
  MODIFY `imgsld_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `img_under_slide`
--
ALTER TABLE `img_under_slide`
  MODIFY `imgsld_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pr_category`
--
ALTER TABLE `pr_category`
  MODIFY `cate_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pr_item`
--
ALTER TABLE `pr_item`
  MODIFY `item_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pr_item_details`
--
ALTER TABLE `pr_item_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pr_item_discount`
--
ALTER TABLE `pr_item_discount`
  MODIFY `discount_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pr_item_price`
--
ALTER TABLE `pr_item_price`
  MODIFY `price_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pr_subcategory`
--
ALTER TABLE `pr_subcategory`
  MODIFY `sub_cateid` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `sys_module`
--
ALTER TABLE `sys_module`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sys_role`
--
ALTER TABLE `sys_role`
  MODIFY `role_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sys_rolepermission`
--
ALTER TABLE `sys_rolepermission`
  MODIFY `rpm_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sys_setting`
--
ALTER TABLE `sys_setting`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

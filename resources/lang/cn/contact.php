<?php
    return [
        'information'=>'資料',
        'require'=>'發送電子郵件，所有帶*的字段都是必填選項',
        'name'=>'名字',
        'email'=>'信箱',
        'subject'=>'標題',
        'message'=>'內容',
        'send_copy'=>'發送副本給自己',
        'phone'=>'電話',
        'submit'=>'提交',
        'website'=>'網址',
    ]
?>
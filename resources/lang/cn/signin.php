<?php
    return [
        'customer_login'=>'訪客登入',
        'register_customer'=>'訪客註冊',
        'new_customer'=>'新帳號',
        'new_acc_text'=>'創建一個帳戶有很多好處：檢查更快，保留多個地址，追蹤訂單等等',
        'create_acc'=>'創建帳號'
    ];
?>
<?php
    return [
        'information'=>'Information',
        'require'=>'Send an email. All fields with an * are required.',
        'name'=>'Name',
        'email'=>'Email',
        'subject'=>'Subject',
        'message'=>'Message',
        'send_copy'=>' Send copy to yourself   ',
        'phone'=>'Phone',
        'submit'=>'Submit',
        'website'=>'Website',
    ]
?>
<?php
    return [
        'home_menu'=>'Home',
        'product_menu'=>'Product',
        'about_menu'=>'About Us',
        'contact_menu'=>'Contact',
        'register_menu'=>'Register',
        'sign_in_menu'=>'Signin',
        'new_product'=>'New Products',
        'releated_product'=>'Related Products',
        'popular_product'=>'Popular Products',
        'my_account'=>'My Account',
        'checkout'=>'Checkout',
        'signin'=>'Sign In',
        'signout'=>'Sign Out',
        'vietnam'=>'Vietnam',
        'cambodia'=>'Cambodia',
        'support'=>'Support',
        'address'=>'Address',
        'stay_connect'=>'Stay Connect',
        'search_in_store'=>'Search in store here',
        'my_cart'=>'My Cart',
        'checkout'=>'Check out'
    ]
?>
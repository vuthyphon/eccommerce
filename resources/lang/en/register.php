<?php
    return[
        'create_new_account'=>'Create New Account',
        'personal_info'=>'Personal Information',
        'first_name'=>'First Name',
        'last_name'=>'Last Name',
        'tel'=>'TEL',
        'address'=>'Address',
        'signin_info'=>'Sign In Information',
        'email'=>'Email',
        'password'=>'Password',
        'confirm_password'=>'Confirm Password',
        'create_account'=>'Create Account',
    ]
?>
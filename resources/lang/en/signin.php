<?php
    return [
        'customer_login'=>'Customer Login',
        'register_customer'=>'Registered Customers',
        'new_customer'=>'New Customers',
        'new_acc_text'=>'Creating an account has many benefits: check out faster, keep more than one address, track orders and more.',
        'create_acc'=>'Create An Account'
    ];
?>
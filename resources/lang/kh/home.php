<?php
    return [
        'home_menu'=>'ទំព័រដើម',
        'product_menu'=>'ផលិតផល',
        'about_menu'=>'អំពីយើង',
        'contact_menu'=>'ទាក់ទងយើង',
        'register_menu'=>'ចុះឈ្មោះ',
        'sign_in_menu'=>'ចូលប្រើ',
        'new_product'=>'ផលិតផលថ្មីៗ',
        'releated_product'=>'ផលិតផលដែលស្រដៀង',
        'popular_product'=>'ផលិតផលពេញនិយម',
        'support'=>'ជំនួយការ',
        'cambodia'=>'កម្ពុជា',
        'vietnam'=>'វៀតណាម',
        'address'=>'អាសយដ្ឋាន',
        'stay_connect'=>'រក្សាទំនាក់ទំនង'
    ]
?>
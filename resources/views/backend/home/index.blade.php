@extends('backend.layouts.app')
@section('title','Honest Technic | Users')
@section('home','li_active')

@section('main-content')
    <main class="main">
        <ol class="breadcrumb">
           <li class="breadcrumb-item">
           <a href="#">{{Auth::user()->username}}</a>
           </li>
           <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        <div class="container-fluid">
                <div class="card row">
                        <div class="card-header">
                           <i class="fa fa-edit"></i> Setting Information
                           <div class="card-header-actions">
                          
                           </div>
                        </div>
                        <div class="card-body">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            
                            {!! session('message') !!}
                              
                        <form action="{{ url('home/update_setting') }}" method="post">
                                @csrf
                                
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="nf-email">Facebook</label>
                                    <input class="form-control" value="{{$setting->fb_link}}" required name="facebook" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">Instagram</label>
                                    <input class="form-control" value="{{$setting->instagram_link}}" required name="instagram" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">Twiiter</label>
                                    <input class="form-control" value="{{$setting->twitter_link}}" required name="twitter" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-password">Email</label>
                                    <input class="form-control" name="email" value="{{$setting->email_link}}" type="text" data-validation="[EMAIL]">
                                    </div>
                                    <div class="form-group">
                                            <label for="nf-password">Telephone</label>
                                    <input class="form-control" value="{{$setting->telephone}}" name="telephone" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">Phone</label>
                                    <input type="text" class="form-control" value="{{$setting->mobile}}" name="phone">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">Address</label>
                                    <input type="text" class="form-control" value="{{$setting->address}}" name="address" >
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary" style="margin-top:30px">Save</button>
                                    </div>
                                </div>
                            </div>
                            
                           
                        </form>
                           
                        
                    </div>
                </div>
        </div>
</main>
@endsection





     
     
@extends('backend.layouts.app')
@section('title','Honest Technic | Profile')
@section('user','li_active')

@section('main-content')
    <main class="main">
        <ol class="breadcrumb">
           <li class="breadcrumb-item">Home</li>
           <li class="breadcrumb-item">
              <a href="#">Profile</a>
           </li>
           <li class="breadcrumb-item active">Change</li>
        </ol>
        <div class="container-fluid">
                <div class="card row">
                        <div class="card-header">
                           <i class="fa fa-edit"></i> Update User Profile
                           <div class="card-header-actions">
                          
                           </div>
                        </div>
                        <div class="card-body">
                            {!! session('message') !!}
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            
                            <form action="{{ url('home/update_profile') }}" method="post">
                                @csrf
                                
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="nf-email">Username</label>
                                            <input type="hidden" name="us_id" value="{{ $users->id }}" />
                                            <input class="form-control" readonly value="{{ $users->username }}" required name="username" placeholder="ឈ្មោះអ្នកប្រើប្រាស់" type="text">
                                        </div>
                                        <div class="form-group">
                                            <label for="nf-password">Email</label>
                                            <input class="form-control" value="{{ $users->email }}" name="email" placeholder="អ៊ីមែល" type="text" data-validation="[EMAIL]">
                                        </div>
                                    
                                        
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                                <label for="nf-password">Password</label>
                                                <input class="form-control" value="" name="password" placeholder="មិនបំពេញមានន័យថាមិនប្តូរលេខសំងាត់" type="password">
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary" style="margin-top:30px">Save</button>
                                        </div>
                                    </div>
                                </div>                           
                        </form>
                           
                        
                    </div>
                </div>
        </div>
</main>
@endsection





     
     
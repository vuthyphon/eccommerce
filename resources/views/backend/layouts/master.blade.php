
<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
      <meta name="description" content="Honest Technic,Computer,Printer,Accessary,">
      <meta name="author" content="Phon Vuthy">
      <meta name="keyword" content="Honest Technic">
      <title>@yield('title')</title>
      @yield('mainCss')

      @yield('customCss')

     

   </head>
   <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
       @yield('header')

      <div class="app-body">
        <!-- left side bar -->
        @yield('left-side')
                 <!-- left side bar -->

        @yield('main-content')
      </div>
      <footer class="app-footer">
         <div>
            <span>© 2019 HonestTechnic</span>
         </div>
         <div class="ml-auto">
            <span></span>
         </div>
      </footer>
      @yield('customModal');
      @yield('mainJs')
      @yield('customJs')
   </body>
</html>


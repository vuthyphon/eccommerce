@extends('backend.layouts.app')
@section('title','Honest Technic | New Products')
@section('user','li_active')
@section('customCss')
   
@endsection

@section('main-content')
    <main class="main">
        <ol class="breadcrumb">
           <li class="breadcrumb-item">Home</li>
           <li class="breadcrumb-item">
              <a href="#">Product</a>
           </li>
           <li class="breadcrumb-item active">Update Product</li>
        </ol>
        <div class="container-fluid">
                <div class="card row">
                        <div class="card-header">
                           <i class="fa fa-edit"></i> Update Product Information
                           <div class="card-header-actions">
                          
                           </div>
                        </div>
                        <div class="card-body">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            
                            @if(session('success'))
                                <div class="alert alert-success">
                                {{ session('success') }}
                                </div> 
                            @endif
                              
                        <form action="{{ url('cp/products/update/'.$product['item_id']) }}" method="post" enctype="multipart/form-data">
                                @csrf
                                
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nf-email">Product Code</label>
                                        <input class="form-control" value="{{ $product['item_code'] }}"  required name="item_code" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">Product Name</label>
                                        <input class="form-control" value="{{ $product['item_name_en'] }}"  required name="item_name_en" required type="text">
                                    </div>
                                    <div class="form-group">
                                            <label for="nf-email">#Tag</label>
                                            <input class="form-control" value="{{ $product['tag'] }}" placeholder="New,Arrival,Promtion" name="tag" type="text">
                                    </div>
                                    
                                    
                                </div>
                                <div class="col-sm-6">
                                   
                                    <div class="form-group">
                                        <label for="nf-email">#Brands</label>
                                        <input class="form-control" value="{{ $product['brand'] }}" placeholder="Dell,Acer,Lenovo....."  required name="brand" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">Category</label>
                                        <select name="category" class="form-control">
                                            @foreach ($category as $item)
                                                <option value="{{$item->cate_id}}" {!! $item->cate_id==$product->cate_id ? 'selected':'' !!}>{{$item->cate_name}}</option>
                                            @endforeach
                                            
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">SubCategory</label>
                                        <select name="sub_category" class="form-control">
                                            @foreach ($sub_category as $item)
                                                <option value="{{$item->sub_cateid}}" {!! $item->sub_cateid==$product->sub_cate_id ? 'selected':'' !!}>{{$item->sub_cate_name}}</option>
                                            @endforeach
                                            
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                        <div class="card">
                                            
                                            <div class="card-header"><i class="fa fa-edit"></i>Description(Khmer)
                                                <div class="card-header-actions">
                                                    <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#kh" aria-expanded="true"><i class="icon-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="card-body collapse hide" id="kh" style="">
                                                <div class="form-group">
                                                <textarea class="summernote" name="descriptions_kh">{{ $product->descriptions_kh }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            
                                            <div class="card-header"><i class="fa fa-edit"></i>Description(English)
                                                <div class="card-header-actions">
                                                    <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#collapseExample" aria-expanded="true"><i class="icon-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="card-body collapse hide" id="collapseExample" style="">
                                                <div class="form-group">
                                                <textarea class="summernote" name="descriptions_en">{{ $product->descriptions_en }}</textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card">
                                                <div class="card-header"><i class="fa fa-edit"></i>Description(Vietnam)
                                                    <div class="card-header-actions">
                                                        <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#vn" aria-expanded="true"><i class="icon-arrow-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="card-body collapse hide" id="vn" style="">
                                                    <div class="form-group">
                                                        <textarea class="summernote" name="descriptions_vn">{{ $product->descriptions_vn }}</textarea>
                                                    </div>
                                                </div>
                                         </div>

                                         <div class="card">
                                                <div class="card-header"><i class="fa fa-edit"></i>Description(Chinese)
                                                    <div class="card-header-actions">
                                                        <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#cn" aria-expanded="true"><i class="icon-arrow-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="card-body collapse hide" id="cn" style="">
                                                    <div class="form-group">
                                                        <textarea class="summernote" name="descriptions_ch">{{ $product->descriptions_ch }}</textarea>
                                                    </div>
                                                </div>
                                         </div>

                                         <div class="form-group">
                                            <label for="nf-email">photos</label>
                                            <input type="file" class="form-control" name="photos[]" multiple />
                                        </div>
                                        <div class="form-group">
                                                <button type="submit" class="btn btn-primary" style="margin-top:30px">Save</button>
                                        </div>
                                        
                                    </div>

                                    
                                
                            </div>
                            
                           
                        </form>
                           
                        <div id="accordion" role="tablist">
                                <div class="card">
                                    <div class="card-header" id="headingOne" role="tab">
                                        <h5 class="mb-0"><a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">Product Image</a></h5>
                                    </div>
                                    <div class="collapse" id="collapseOne" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <th>No</th>
                                                    <th>Filename</th>
                                                    <th>Thumbnail</th>
                                                    <th>Feature Image</th>
                                                    <th>Created Date</th>
                                                    <th>Delete</th>
                                                </thead>
                                                <tbody>
                                                    @isset($product->item_detail)
                                                            @php
                                                                $i=0;
                                                            @endphp
                                                        @foreach ($product->item_detail as $item)
                                                            @php
                                                                $i++;
                                                            @endphp
                                                            <tr>
                                                                <td>{{$i}}</td>
                                                                <td>{{$item->filename}}</td>
                                                                <td><img src="{{asset('storage/photos/thumbnail/'.$item->filename)}}" /></td>
                                                                <td></td>
                                                                <td>{{$item->created_at}}</td>
                                                            <td><a href="{{url('cp/products/detail/disable/'.$item->id)}}" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure to delete this image?')"><i class="fas fa-trash-alt"></i></a></td>
                                                            </tr>
                                                        @endforeach
                                                    @endisset
                                                <tbody>
                                                
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo" role="tab">
                                        <h5 class="mb-0"><a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Product Prices</a></h5>
                                    </div>
                                    <div class="collapse" id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion" style="">
                                        <div class="card-body">
                                                    <button type="button" style="margin:10px;" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">
                                                       New Item Price
                                                      </button>
                                                <table class="table table-bordered">
                                                        <thead>
                                                            <th>No</th>
                                                            <th>Unit Price($)</th>
                                                            <th>Created Date</th>
                                                            <th>Status</th>
                                                            <th>Delete</th>
                                                        </thead>
                                                        <tbody>
                                                            @isset($product->items_price)
                                                                    @php
                                                                        $i=0;
                                                                    @endphp
                                                                @foreach ($product->items_price as $item)
                                                                    @php
                                                                    $i++;
                                                                    @endphp
                                                                   <tr>
                                                                        <td>{{$i}}</td>
                                                                        <td>{{number_format($item->unit_price,2)}}</td>
                                                                        <td>{{$item->created_at}}</td>
                                                                        <td>{!!$item->d_status==1 ? '<i class="far fa-check-circle"></i>':'<i class="far fa-times-circle"></i>'!!}</td>
                                                                        <td><button class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button></td>
                                                                    </tr>
                                                                @endforeach
                                                            @endisset
                                                           
                                                        </tbody>
                                                    </table>   
                                        
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
        </div>
</main>
@endsection

@section('customModal')

      
      <!-- The Modal -->
      <div class="modal" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content">
      
            <!-- Modal Header -->
          <form method="POST" action="{{url('cp/products/new_price/'.$product['item_id'])}}">
                @csrf
            <div class="modal-header">
              <h4 class="modal-title">Change Current of Product Price</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
      
            <!-- Modal body -->
            <div class="modal-body">

                    <div class="form-group">
                            <label for="nf-email">Item Price</label>
                            <input class="form-control" value="{{ old('price') }}" placeholder="new price" required name="price" type="text">
                    </div>
            </div>
      
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure to add new current price of this product?')" >Save</button>
            </div>
          </form>
      
          </div>
        </div>
      </div>
    
@endsection

@section('customJs')
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.js"></script>
    <script>
        $('.summernote').summernote({
        placeholder: 'Description in english',
        tabsize: 2,
        height: 100
      });
    </script>
@endsection





     
     
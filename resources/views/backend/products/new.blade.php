@extends('backend.layouts.app')
@section('title','Honest Technic | New Products')
@section('user','li_active')
@section('customCss')
   
@endsection

@section('main-content')
    <main class="main">
        <ol class="breadcrumb">
           <li class="breadcrumb-item">Home</li>
           <li class="breadcrumb-item">
              <a href="#">Product</a>
           </li>
           <li class="breadcrumb-item active">Add</li>
        </ol>
        <div class="container-fluid">
                <div class="card row">
                        <div class="card-header">
                           <i class="fa fa-edit"></i> Add Product Information
                           <div class="card-header-actions">
                          
                           </div>
                        </div>
                        <div class="card-body">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            
                            @if(session('success'))
                                <div class="alert alert-success">
                                {{ session('success') }}
                                </div> 
                            @endif
                              
                        <form action="{{ url('cp/products/add') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nf-email">Product Code</label>
                                        <input class="form-control" value="{{ old('item_code') }}"  required name="item_code" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">Product Name</label>
                                        <input class="form-control" value="{{ old('item_name_en') }}"  required name="item_name_en" required type="text">
                                    </div>
                                    <div class="form-group">
                                            <label for="nf-email">#Tag</label>
                                            <input class="form-control" value="{{ old('tag') }}" placeholder="New,Arrival,Promtion" required name="tag" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">#Brands</label>
                                        <input class="form-control" value="{{ old('brand') }}" placeholder="Dell,Acer,Lenovo....."  required name="brand" type="text">
                                    </div>
                                    
                                </div>
                                <div class="col-sm-6">
                                   
                                    <div class="form-group">
                                            <label for="nf-email">Unit Prices</label>
                                            <input class="form-control" value="{{ old('unit_price') }}"  required name="unit_price" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">Category</label>
                                        <select name="category" class="form-control" id="category">
                                            @foreach ($category as $item)
                                                <option value="{{$item->cate_id}}">{{$item->cate_name}}</option>
                                            @endforeach
                                            
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">SubCategory</label>
                                        <select name="sub_category" class="form-control" id="sub_cate">
                                            @foreach ($sub_category as $item)
                                                <option value="{{$item->sub_cateid}}">{{$item->sub_cate_name}}</option>
                                            @endforeach
                                            
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header"><i class="fa fa-edit"></i>Description(Khmer)
                                                <div class="card-header-actions">
                                                    <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#kh" aria-expanded="true"><i class="icon-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="card-body collapse hide" id="kh" style="">
                                                <div class="form-group">
                                                    <textarea class="summernote" name="descriptions_kh"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header"><i class="fa fa-edit"></i>Description(English)
                                                <div class="card-header-actions">
                                                    <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#collapseExample" aria-expanded="true"><i class="icon-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="card-body collapse hide" id="collapseExample" style="">
                                                <div class="form-group">
                                                    <textarea class="summernote" name="descriptions_en"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card">
                                                <div class="card-header"><i class="fa fa-edit"></i>Description(Vietnam)
                                                    <div class="card-header-actions">
                                                        <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#vn" aria-expanded="true"><i class="icon-arrow-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="card-body collapse hide" id="vn" style="">
                                                    <div class="form-group">
                                                        <textarea class="summernote" name="descriptions_vn"></textarea>
                                                    </div>
                                                </div>
                                         </div>

                                         <div class="card">
                                                <div class="card-header"><i class="fa fa-edit"></i>Description(Chinese)
                                                    <div class="card-header-actions">
                                                        <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#cn" aria-expanded="true"><i class="icon-arrow-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="card-body collapse hide" id="cn" style="">
                                                    <div class="form-group">
                                                        <textarea class="summernote" name="descriptions_cn"></textarea>
                                                    </div>
                                                </div>
                                         </div>

                                         <div class="form-group">
                                            <label for="nf-email">photos</label>
                                            <input type="file" class="form-control" name="photos[]" multiple />
                                        </div>
                                        <div class="form-group">
                                                <button type="submit" class="btn btn-primary" style="margin-top:30px">Save</button>
                                        </div>
                                        
                                    </div>

                                    
                                
                            </div>
                            
                           
                        </form>
                           
                        
                    </div>
                </div>
        </div>
</main>
@endsection

@section('customJs')
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.js"></script>
    <script>
        $('.summernote').summernote({
        placeholder: 'Description in english',
        tabsize: 2,
        height: 100
      });
    </script>

    <script>
        $("#category").change(function(){
            var cateId=$(this).val();
            $.ajax({
                url:"<?php echo url('cp/category/')?>/"+cateId+"/sub_category",
                method:'GET',
                dataType:'json',
                success:function(result){
                    var opt="";
                    $.each(result,function(k,v){
                        opt+="<option value='"+v.sub_cateid+"'>"+v.sub_cate_name+"</option>";
                    });
                    $("#sub_cate").html(opt);
                }

            });
        });
        
    </script>
@endsection





     
     
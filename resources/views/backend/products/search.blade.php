@extends('backend.layouts.app')
@section('title','Honest Technic | Products')
@section('user','li_active')

@section('main-content')
    <main class="main">
        <ol class="breadcrumb">
           <li class="breadcrumb-item">
              <a href="#">{{ Auth::user()->username }}</a>
           </li>
           <li class="breadcrumb-item active">Products</li>
           <li class="breadcrumb-menu d-md-down-none">
              <div class="btn-group" role="group" aria-label="Button group">
                 <a class="btn" href="#">
                 <i class="icon-speech"></i>
                 </a>
                 <a class="btn" href="index.html">
                 <i class="icon-graph"></i> &nbsp;Dashboard</a>
                 <a class="btn" href="#">
                 <i class="icon-settings"></i> &nbsp;Settings</a>
              </div>
           </li>
        </ol>
        <div class="container-fluid">
                <div class="card row">
                        <div class="card-header">

                            <div class="row">
                                <div class="col-sm-6"> <i class="fa fa-edit"></i> Product Information</div>
                                <div class="col-sm-6">
                                <form action="{{url('cp/products/search')}}" method="GET">
                                    <div class="row">
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-6"><input type="text" name="filter" placeholder="search in here" class="form-control"></div>
                                        <div class="col-sm-2"> <button class="btn btn-primary" type="submit">Search <i class="fa fa-search"></i></button></div> 
                                    </div>
                                </form>   

                                    
                                </div>
                            </div>
                        
                        </div>
                        <div class="card-body">
                        {!! session('message') !!}
                            
                        <table class="table table-striped table-bordered"  width="100%">
                            <thead>
                                <tr role="row">
                                    <th>No</th>
                                    <th>Product Code</th>
                                    <th>Product Name</th>
                                    <th>Brand</th>
                                    <th>UnitPrice($)</th>
                                    <th>Cateogory</th>
                                    <th>Sub Category</th>
                                    <th>Created Date</th>
                                    <th>By</th>
                                    <!--<th>Status</th>-->
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i=1;
                                @endphp
                                @isset($products)
                                   @foreach ($products as $item)
                                    <tr role="row">
                                        <td>{{ $i++ }}</td>
                                        <td>{{$item->item_code}}</td>
                                        <td>{{$item->item_name_en}}</td>
                                        <td>{{$item->brand}}</td>
                                        <td>{!!$item->item_price!=null ? number_format($item->item_price->unit_price,2):'0.00' !!}</td>
                                        <td>{{ $item->category->cate_name }}</td>
                                        <td>{{ $item->sub_category->sub_cate_name }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td>{{$item->user->username}}</td>
                                        <!--<td>{!! $item->d_status==1 ? "<i class='fa fa-check-circle'></i>" : '<i class="fa fa-ban"></i>' !!}</td>-->
                                        <td>
                                            <a class="btn btn-primary btn-sm btn-edit" href="{{ url('cp/products/edit/'.$item->item_id) }}"><i class="fa fa-edit"></i></a>
                                            <a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to disabled this product?')" href="{{ url('cp/products/disable/'.$item->item_id) }}"><i class="fas fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                   @endforeach
                                @endisset
                            </tbody>
                        </table>

                        <pagination v-if="pagination.last_page > 1" :pagination="pagination" :offset="5" @paginate="fetchPosts()"></pagination>
                    </div>
                </div>
        </div>
</main>
@endsection


@section('customJs')

<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="{{ asset('backend/vendors/dataTable/datatables.js') }}"></script>

<script>
    $(document).ready( function () {
        $('#myTable').DataTable({
            "autoWidth":false
        });
});
</script>

<script src="{{ asset('js/app.js') }}"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.0/js/all.js"></script>

@endsection


     
     
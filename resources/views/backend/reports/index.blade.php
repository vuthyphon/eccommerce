@extends('backend.layouts.app')
@section('title','Honest Technic | Products')
@section('user','li_active')

@section('main-content')
    <main class="main">
        <ol class="breadcrumb">
           <li class="breadcrumb-item">
              <a href="#">{{ Auth::user()->username }}</a>
           </li>
           <li class="breadcrumb-item active">Client Order Report</li>
      
           <li class="breadcrumb-menu d-md-down-none">
              <div class="btn-group" role="group" aria-label="Button group">
                 <a class="btn" href="#">
                 <i class="icon-speech"></i>
                 </a>
                 <a class="btn" href="index.html">
                 <i class="icon-graph"></i> &nbsp;Dashboard</a>
                 <a class="btn" href="#">
                 <i class="icon-settings"></i> &nbsp;Settings</a>
              </div>
           </li>
        </ol>
        
        <div class="container-fluid">
                <div class="card row">
                        <div class="card-header">
                           <i class="fa fa-edit"> Client Order Report</i>
                           <div class="card-header-actions">
                               <b>From:</b>
                           <input type="date" value="@if (Request::segment(3)==''){{date('Y-m-d')}}@else{{Request::segment(3)}}@endif" id="frdt" >
                               <b>&nbsp;&nbsp;&nbsp;To:</b>
                               <input type="date" value="@if (Request::segment(4)==''){{date('Y-m-d')}}@else{{Request::segment(4)}}@endif"  id="todt" >

                               <button class="btn-primary btn-sm" id="btn_search"><b><i class="fa fa-search"></i> Search</b></button>
                                &nbsp;&nbsp;
                                <a href="javascript:void(0);" id="btn_download" class=" btn btn-success btn-sm"><b><i class="fa fa-download"></i> Download</b></a>
                               
                           </div>
                           
                        </div>
                        <div class="card-body">
                        
                            
                        <table class="table table-bordered" width="100%">
                            <thead>
                                <tr role="row">
                                    <th>No</th>
                                    <th>Customer</th>
                                    <th>Tel</th>
                                    <th>Order Date</th>
                                    <th>Product Code</th>
                                    <th>Product Name</th>
                                    <th>Qty</th>
                                    <th>Unit Price($)</th>
                                    <th>Total</th>
                                    <th>Delivery to</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i=1;
                                    $key='';
                                    $total=0;
                                @endphp
                                @isset($clientorder)
                                   @foreach ($clientorder as $item)
                                   @if($item->order_id!==$key)
                                   @php $key=$item->order_id; @endphp
                                    <tr role="row">
                                        <td>{{$i}}</td>
                                        <td>{{$item->first_name.' '.$item->last_name}}</td>
                                        <td>{{$item->tel}}</td>
                                        <td>{{ date_format(date_create($item->order_date),"d-m-Y")}}</td>
                                        <td>{{$item->item_code}}</td>
                                        <td>{{$item->item_name_en}}</td>
                                        <td>{{$item->qty}}</td>
                                        <td>{{$item->unit_price}}</td>
                                        <td>{{$item->qty * $item->unit_price}}</td>
                                        <th>@if($item->ref!=='') {{$item->ref}} @else {{$item->address}} @endif</th>
                                    </tr>
                                    @else 
                                    <tr role="row">
                                            <td>{{$i}}</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>{{$item->item_code}}</td>
                                            <td>{{$item->item_name_en}}</td>
                                            <td>{{$item->qty}}</td>
                                            <td>{{$item->unit_price}}</td>
                                            <td>{{$item->qty * $item->unit_price}}</td>
                                            <td>-</td>
                                        </tr>
                                    @endif

                                    @php
                                        $total+=($item->qty * $item->unit_price);
                                        $i++;
                                    @endphp
                                   @endforeach

                                   <tr role="row">
                                        <th colspan="8">Total</th>
                                        <th>{{number_format($total,2)}}</th>
                                        <th></th>
                                    </tr>

                                @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
</main>
@endsection


@section('customJs')
    
<script>

    //$('#frdt').datePicker('yy-mm-d');

    $('#btn_search').click(function(){
        window.open("{{ url('cp/orderreport')}}/"+$('#frdt').val()+"/"+$('#todt').val(),'_parent');
    });

    $('#btn_download').click(function(){
        window.open("{{ url('cp/download')}}/"+$('#frdt').val()+"/"+$('#todt').val(),'_blank');
    });

</script>

@endsection


     
     
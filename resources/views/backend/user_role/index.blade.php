@extends('backend.layouts.app')
@section('title','ប្រព័ន្ធគ្រប់គ្រងបេសកកម្ម | អ្នកប្រើប្រាស់')
@section('user','li_active')

@section('main-content')
    <main class="main">
        <ol class="breadcrumb">
           <li class="breadcrumb-item">
              <a href="#">{{ Auth::user()->username }}</a>
           </li>
           <li class="breadcrumb-item active">សិទ្ធិប្រើប្រាស់</li>
           <li class="breadcrumb-menu d-md-down-none">
              <div class="btn-group" role="group" aria-label="Button group">
                 <a class="btn" href="#">
                 <i class="icon-speech"></i>
                 </a>
                 <a class="btn" href="index.html">
                 <i class="icon-graph"></i> &nbsp;Dashboard</a>
                 <a class="btn" href="#">
                 <i class="icon-settings"></i> &nbsp;Settings</a>
              </div>
           </li>
        </ol>
        <div class="container-fluid">
                <div class="card row">
                        <div class="card-header">
                           <i class="fa fa-edit"></i> ព័ត៌មានសិទ្ធិប្រើប្រាស់
                          <div class="card-header-actions">
                           <a href="#" class="btn btn-primary">បង្កើតថ្មី</a>
                           </div>
                        </div>
                        <div class="card-body">
                        {!! session('message') !!}
                            
                        <table class="table table-striped table-bordered" id="myTable" width="100%">
                            <thead>
                                <tr role="row">
                                    <th>ល.រ</th>
                                    <th>សិទ្ធីប្របាស់</th>
                                    <th>បង្កើតថ្ងៃ</th>
                                    <th>កែប្រែថ្ងៃ</th>
                                    <th>ជម្រើស</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $i=1;
                                @endphp
                                @isset($role)
                                    @foreach ($role as $item)
                                    <tr role="row">
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $item->role_name }}</td>
                                        <td>{{ $item->created_at }}</td>
                                        <td>{{ $item->updated_at }}</td>
                                        <td>
                                            <a class="btn btn-info btn-sm btn-edit" href="{{ url('cp/role/edit/'.$item->role_id) }}"><i class="fa fa-edit"></i></a>
                                            <a class="btn btn-danger btn-sm" href="{{ url('cp/role/disable/'.$item->role_id) }}"><i class="fas fa-user-times"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
</main>
@endsection


@section('customJs')

<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="{{ asset('backend/vendors/dataTable/datatables.js') }}"></script>

<script>
    $(document).ready( function () {
        $('#myTable').DataTable({
            "autoWidth":false
        });
});
</script>

@endsection


     
     
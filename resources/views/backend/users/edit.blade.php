@extends('backend.layouts.app')
@section('title','Invoice | Users')
@section('user','li_active')

@section('main-content')
    <main class="main">
        <ol class="breadcrumb">
           <li class="breadcrumb-item">
           <a href="#">{{Auth::user()->username}}</a>
           </li>
           <li class="breadcrumb-item active">កែប្រៃព័ត៌មានអ្នកប្រើប្រាស់</li>
        </ol>
        <div class="container-fluid">
                <div class="card row">
                        <div class="card-header">
                           <i class="fa fa-edit"></i> កែប្រៃព័ត៌មានអ្នកប្រើប្រាស់
                           <div class="card-header-actions">
                          
                           </div>
                        </div>
                        <div class="card-body">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif    
                              
                        <form action="{{ url('cp/users/update') }}" method="post">
                                @csrf
                                
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nf-email">អ្នកប្រើប្រាស់</label>
                                        <input type="hidden" name="us_id" value="{{ $users->id }}" />
                                        <input class="form-control" value="{{ $users->username }}" required name="username" placeholder="ឈ្មោះអ្នកប្រើប្រាស់" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-password">អ៊ីមែល</label>
                                        <input class="form-control" value="{{ $users->email }}" name="email" placeholder="អ៊ីមែល" type="text" data-validation="[EMAIL]">
                                    </div>
                                    <div class="form-group">
                                            <label for="nf-password">លេខសំងាត់</label>
                                            <input class="form-control" value="" name="password" placeholder="មិនបំពេញមានន័យថាមិនប្តូរលេខសំងាត់" type="password">
                                    </div>
                                   
                                    
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                            <label for="nf-email">សិទ្ធិប្រើប្រាស់</label>
                                            <select class="form-control" name="role">
                                                @foreach ($role as $item)
                                                    <option value="{{ $item->role_id}}" {!! $users->role==$item->role_id ? 'selected':'' !!}>{{$item->role_name}}</option>
                                                @endforeach
                                            </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="nf-password">ស្ថានភាពប្រើប្រាស់</label>
                                        <select name="active" class="form-control">
                                            <option value="1" {!! $users->active==1 ? 'selected':''  !!}>Active</option>
                                            <option value="0" {!! $users->active==0 ? 'selected':''  !!}>Inactive</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary" style="margin-top:30px">កែប្រែ</button>
                                    </div>
                                </div>
                            </div>
                            
                           
                        </form>
                           
                        
                    </div>
                </div>
        </div>
</main>
@endsection





     
     
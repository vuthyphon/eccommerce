@extends('backend.layouts.app')
@section('title','Invoice | Users')
@section('user','li_active')

@section('main-content')
    <main class="main">
        <ol class="breadcrumb">
           <li class="breadcrumb-item">Home</li>
           <li class="breadcrumb-item">
              <a href="#">Users</a>
           </li>
           <li class="breadcrumb-item active">Edit</li>
        </ol>
        <div class="container-fluid">
                <div class="card row">
                        <div class="card-header">
                           <i class="fa fa-edit"></i> Add Users Information
                           <div class="card-header-actions">
                          
                           </div>
                        </div>
                        <div class="card-body">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif    
                              
                        <form action="{{ url('cp/users/add') }}" method="post">
                                @csrf
                                
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nf-email">Username</label>
                                    <input class="form-control" value="{{ old('username') }}"  required name="username" placeholder="ឈ្មោះអ្នកប្រើប្រាស់" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-password">Email</label>
                                        <input class="form-control" value="{{ old('email') }}" name="email" placeholder="អ៊ីមែល" type="text" data-validation="[EMAIL]">
                                    </div>
                                    <div class="form-group">
                                            <label for="nf-password">Password</label>
                                            <input class="form-control" value="" name="password" placeholder="មិនបំពេញមានន័យថាមិនប្តូរលេខសំងាត់" type="password">
                                    </div>
                                   
                                    
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                            <label for="nf-email">Role</label>
                                            <select class="form-control" name="role">
                                                @foreach ($role as $item)
                                            <option value="{{ $item->role_id}}" >{{ $item->role_name }}</option>
                                                @endforeach
                                            </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="nf-password">Active</label>
                                        <select name="active" class="form-control">
                                            <option value="0">Inactive</option>
                                            <option value="1">Active</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary" style="margin-top:30px">Save</button>
                                    </div>
                                </div>
                            </div>
                            
                           
                        </form>
                           
                        
                    </div>
                </div>
        </div>
</main>
@endsection





     
     
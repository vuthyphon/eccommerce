

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="{{ asset('css/table.css') }}" rel="stylesheet">
    </head>

    
    <body>
            <h3>Customer Order Report</h3>
            <h4>From: {{ $frdt }} To: {{$todt}}</h4>
            
            <table class="table table-bordered" width="100%">
                <thead>
                    <tr role="row">
                        <th>No</th>
                        <th>Customer</th>
                        <th>Tel</th>
                        <th>Order Date</th>
                        <th>Product Code</th>
                        <th>Product Name</th>
                        <th>Qty</th>
                        <th>Unit Price($)</th>
                        <th>Total</th>
                        <th>Delivery to</th>
                    </tr>
                </thead>
                <tbody>

                    @isset($order_data)
                    @php
                        $i=1;
                        $key='';
                        $total=0;
                    @endphp
        
                    @foreach ($order_data as $item)
                    
                        @if($item->order_id!==$key)
                        @php $key=$item->order_id; @endphp
                        <tr role="row">
                            <td>{{$i}}</td>
                            <td>{{$item->first_name.' '.$item->last_name}}</td>
                            <td>{{$item->tel}}</td>
                            <td>{{ date_format(date_create($item->order_date),"d-m-Y")}}</td>
                            <td>{{$item->item_code}}</td>
                            <td>{{$item->item_name_en}}</td>
                            <td>{{$item->qty}}</td>
                            <td>{{$item->unit_price}}</td>
                            <td>{{$item->qty * $item->unit_price}}</td>
                            <th>@if($item->ref!=='') {{$item->ref}} @else {{$item->address}} @endif</th>
                        </tr>
                        @else 
                        <tr role="row">
                                <td>{{$i}}</td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td>{{$item->item_code}}</td>
                                <td>{{$item->item_name_en}}</td>
                                <td>{{$item->qty}}</td>
                                <td>{{$item->unit_price}}</td>
                                <td>{{$item->qty * $item->unit_price}}</td>
                                <td>-</td>
                            </tr>
                        @endif
                        @php
                            $total+=($item->qty * $item->unit_price);
                            $i++;
                        @endphp
                        @endforeach
                    @endisset                                                                      
                        
                                                           
                       <tr role="row">
                            <th colspan="8">Total</th>
                            <th>{{$total}}</th>
                            <td></td>
                        </tr>

                    </tbody>
            </table>
    </body>
</html>


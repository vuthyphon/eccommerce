  <!-- Header Middle Start -->
  <div class="header-middle ptb-20 white-bg">
    <div class="container">
        <div class="row">
            <!-- Logo Start -->
            <div class="col-lg-3 col-md-4">
                <div class="logo">
                    <a href="{{route('home')}}"><img src="{{ asset ('frontend/img/logo/logo.png')}}" alt="Honest Technic" title="Honest Technic"></a>
                </div>
            </div>
            <!-- Logo End -->
            <!-- Header Middle Menu Start -->
            <div class="col-lg-9 col-md-8">
                <div class="middle-menu hidden-sm hidden-xs">
                    <nav>
                        <ul class="middle-menu-list">
                            <li><a href="{{route('home')}}" class="@yield('active-home')" alt="Home" title="Home">@lang('home.home_menu')</a></li>
                            <li><a href="{{route('product')}}" class="@yield('active-product')" alt="Product" title="Product">@lang('home.product_menu')</a></li>
                            <li><a href="{{route('about')}}" class="@yield('active-about')" alt="About Honest Technic" title="About Honest Technic">@lang('home.about_menu')</a></li>
                            <li><a href="{{url('/contact')}}" class="@yield('active-contact')" alt="Contact" title="Contact">@lang('home.contact_menu')</a></li>
                            <li><a href="{{url('register')}}" class="@yield('active-register')" alt="Register" title="Register">@lang('home.register_menu')</a></li>
                            <li><a href="{{route('signin')}}" class="@yield('active-login')" alt="Signin" title="Signin">@lang('home.sign_in_menu')</a></li>
                            
                        </ul>
                    </nav>
                </div>

               <!-- Main Cart Box Start 
                Mobile view for check out
                -->
                <div class="cart-box visible-xs">
                    <ul>
                        <li>
                           
                            
                            <a href="{{route('mycart')}}">
                            
                                    @if(count($mycart)>0)
                                        <span style="position: absolute;
                                        right: -3px;
                                        top: -7px;
                                        background: red;
                                        color: #fff;
                                        width: 16px;
                                        height: 16px;
                                        border-radius: 50%;
                                        text-align: center;
                                        font-size: 10px;
                                        line-height: 16px;"> {{ count($mycart)}}</span>
                                    @endif    
                            </a> 
                            
                        </li>
                    </ul>
                </div>
                <!-- Main Cart Box End -->

                
                <!-- Mobile Menu  Start -->
                <div class="mobile-menu visible-sm visible-xs">
                        <nav>
                            <ul>
                                <li><a href="{{route('home')}}">@lang('home.home_menu')</a></li>
                                <li><a href="{{url('product/cate-10')}}">@lang('category.paper')</a><!--Paper!-->
                                   
                                    <ul>
                                        <li><a href="{{url('product/category/53')}}">@lang('category.printer_paper')</a></li>
                                        <li><a href="{{url('product/category/54')}}">@lang('category.color_paper')</a></li>
                                        <li><a href="{{url('product/category/55')}}">@lang('category.notebook')</a></li>
                                        <li><a href="{{url('product/category/56')}}">@lang('category.post_it_related')</a></li>
                                        <li><a href="{{url('product/category/57')}}">@lang('category.subpoena')</a></li>
                                        <li><a href="{{url('product/category/58')}}">@lang('category.envelope')</a></li>
                                        <li><a href="{{url('product/category/59')}}">@lang('category.cardboard')</a></li>
                                        <li><a href="{{url('product/category/60')}}">@lang('category.label_sticker')</a></li>
                                        <li><a href="{{url('product/category/61')}}">@lang('category.inspection_paper')</a></li>
                                    </ul>
                                  
                                <li><a href="{{url('product/cate-11')}}">@lang('category.office_supplies')</a>
                                    <!-- Office Suplies-->
                                    <ul>
                                        <li><a href="{{url('product/category/62')}}">@lang('category.hole_puncher')</a></li>
                                        <li><a href="{{url('product/category/63')}}">@lang('category.stapler')</a></li>
                                        <li><a href="{{url('product/category/64')}}">@lang('category.scissor_paper_cutter')</a></li>
                                        <li><a href="{{url('product/category/65')}}">@lang('category.utility_knife')</a></li>
                                        <li><a href="{{url('product/category/66')}}">@lang('category.correction_tap')</a></li>
                                        <li><a href="{{url('product/category/67')}}">@lang('category.glue')</a></li>
                                        <li><a href="{{url('product/category/68')}}">@lang('category.various_tap')</a></li>
                                        <li><a href="{{url('product/category/69')}}">@lang('category.measuring')</a></li>
                                        <li><a href="{{url('product/category/70')}}">@lang('category.clip_item')</a></li>
                                        <li><a href="{{url('product/category/71')}}">@lang('category.stamps')</a></li>
                                        <li><a href="{{url('product/category/72')}}">@lang('category.guard_film')</a></li>
                                        <li><a href="{{url('product/category/73')}}">@lang('category.stationeries')</a></li>
                                        <li><a href="{{url('product/category/74')}}">@lang('category.package_items')</a></li>
                                    </ul>
                                    
                                </li>
                                <li><a href="{{url('product/cate-12')}}">@lang('category.business_supplies')</a>
                                    <!-- Business Supplies -->
                                    <ul>
                                        <li><a href="{{url('product/category/75')}}">@lang('category.calculator')</a></li>
                                        <li><a href="{{url('product/category/76')}}">@lang('category.battery')</a></li>
                                        <li><a href="{{url('product/category/77')}}">@lang('category.usb_mouse_')</a></li>
                                        <li><a href="{{url('product/category/78')}}">@lang('category.flash_disc')</a></li>
                                        <li><a href="{{url('product/category/79')}}">@lang('category.printing_devices')</a></li>
                                    </ul>
                                    
                                </li>
                                <li><a href="{{url('product/cate-13')}}">@lang('category.pen')</a>
                                    <!--Pen -->
                                    <ul>
                                        <li><a href="{{url('product/category/80')}}">@lang('category.gel_pen')</a></li>
                                        <li><a href="{{url('product/category/81')}}">@lang('category.ball_pen')</a></li>
                                        <li><a href="{{url('product/category/82')}}">@lang('category.mike_oil')</a></li>
                                        <li><a href="{{url('product/category/83')}}">@lang('category.pencil_')</a></li>
                                        <li><a href="{{url('product/category/84')}}">@lang('category.automatic_')</a></li>
                                        <li><a href="{{url('product/category/85')}}">@lang('category.fountain_pen')</a></li>
                                        <li><a href="{{url('product/category/86')}}">@lang('category.paint_')</a></li>
                                        <li><a href="{{url('product/category/87')}}">@lang('category.other_pens')</a></li>
                                        <li><a href="{{url('product/category/88')}}">@lang('category.pencil_case')</a></li>
                                    </ul>
                                    
                                </li>
                                <li><a href="{{url('product/cate-14')}}">@lang('category.conference_supplies')</a>
                                    <!-- conference_supplies 	 -->
                                    <ul>
                                        <li><a href="{{url('product/category/89')}}">@lang('category.various_board_')</a></li>
                                        <li><a href="{{url('product/category/90')}}">@lang('category.bulletin_')</a></li>
                                        <li><a href="{{url('product/category/91')}}">@lang('category.magnet')</a></li>
                                        <li><a href="{{url('product/category/92')}}">@lang('category.office_appliance')</a></li>
                                        <li><a href="{{url('product/category/93')}}">@lang('category.hole_')</a></li>
                                        <li><a href="{{url('product/category/94')}}">@lang('category.data_class_')</a></li>
                                        <li><a href="{{url('product/category/95')}}">@lang('category.information_bag')</a></li>
                                        <li><a href="{{url('product/category/96')}}">@lang('category.file_box')</a></li>
                                        <li><a href="{{url('product/category/97')}}">@lang('category.business_card')</a></li>
                                        <li><a href="{{url('product/category/98')}}">@lang('category.hole_guard')</a></li>
                                        <li><a href="{{url('product/category/99')}}">@lang('category.seperate_')</a></li>
                                    </ul>
                                    
                                </li>

                                <li><a href="{{url('product/cate-18')}}">@lang('category.office_appliances')</a>
                                    <!-- Office appliance 	 -->
                                    <ul>
                                        <li><a href="{{url('product/category/114')}}">@lang('category.hole_iron')</a></li>
                                        <li><a href="{{url('product/category/115')}}">@lang('category.data_class_')</a></li>
                                        <li><a href="{{url('product/category/116')}}">@lang('category.info_bag')</a></li>
                                        <li><a href="{{url('product/category/117')}}">@lang('category.file_box_')</a></li>
                                        <li><a href="{{url('product/category/118')}}">@lang('category.business_card_box')</a></li>
                                        <li><a href="{{url('product/category/119')}}">@lang('category.hole_guard')</a></li>
                                        <li><a href="{{url('product/category/120')}}">@lang('category.separate_')</a></li>
                                    </ul>
                                    
                                </li>
                                <li><a href="{{url('product/cate-15')}}">@lang('category.tool')</a>
                                    <!-- Tool -->
                                    <ul>
                                        <li><a href="{{url('product/category/100')}}">@lang('category.tool_set')</a></li>
                                        <li><a href="{{url('product/category/101')}}">@lang('category.hand_tool')</a></li>
                                    </ul>
                                    
                                </li>
                                <li><a href="{{url('product/cate-16')}}">@lang('category.cleaning_supplies')</a>
                                    <!-- Cleaning Supplies -->
                                    <ul>
                                        <li><a href="{{url('product/category/102')}}">@lang('category.labor_')</a></li>
                                        <li><a href="{{url('product/category/103')}}">@lang('category.detergents_')</a></li>
                                        <li><a href="{{url('product/category/104')}}">@lang('category.toilet_paper')</a></li>
                                        <li><a href="{{url('product/category/105')}}">@lang('category.mop_broom_')</a></li>
                                        <li><a href="{{url('product/category/106')}}">@lang('category.rag')</a></li>
                                        <li><a href="{{url('product/category/107')}}">@lang('category.cotton_gloves_')</a></li>
                                        <li><a href="{{url('product/category/108')}}">@lang('category.brush_melon_')</a></li>
                                        <li><a href="{{url('product/category/109')}}">@lang('category.other_')</a></li>
                                        <li><a href="{{url('product/category/110')}}">@lang('category.air_purifier')</a></li>
                                    </ul>
                                    
                                </li>
                                <li><a href="{{url('product/cate-17')}}">@lang('category.light_')</a>
                                    <!-- Light -->
                                    <ul>
                                        <li><a href="{{url('product/category/111')}}">@lang('category.led')</a></li>
                                        <li><a href="{{url('product/category/112')}}">@lang('category.extenstion_cord')</a></li>
                                        <li><a href="{{url('product/category/113')}}">@lang('category.other_electronics')</a></li>
                                    </ul>
                                    
                                </li>
                                <li><a href="{{url('product/cate-19')}}">@lang('category.table_chair')</a>
                                    <ul>
                                        <li><a href="{{url('product/category/121')}}">@lang('category.table_group')</a></li>
                                        <li><a href="{{url('product/category/122')}}">@lang('category.file_cabinet')</a></li>
                                        <li><a href="{{url('product/category/123')}}">@lang('category.seat')</a></li>
                                        <li><a href="{{url('product/category/124')}}">@lang('category.public_equipment')</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{route('about')}}">@lang('home.about_menu')</a></li>
                                <li><a href="{{url('/contact')}}">@lang('home.contact_menu')</a></li>
                                <li><a href="{{ route('signout')}}">@lang('home.signout')</a></li>
                            </ul>
                        </nav>
                    </div>
                <!-- Mobile Menu  End -->
                
                
                
            </div>
            <!-- Header Middle Menu End -->
            
            
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- Header Middle End -->



            <!-- Header Bottom Start -->
            <div class="header-bottom ptb-10 blue-bg">
                <div class="container">
                    <div class="row">

                    
                        <!-- Primary Vertical-Menu Start -->
                        <div class="col-lg-4 col-md-4 col-sm-4 hidden-sm hidden-xs">
                                <div class="vertical-menu">
                                    <span class="categorie-title">@lang('category.cate_menu')</span>
                                    <nav>
                                        @if (View::hasSection('home'))
                                            <ul class="vertical-menu-list">
                                        
                                        @else
                                            <ul class="vertical-menu-list menu-hideen">
                                        @endif
                                        
                                        
                                            <li><a href="{{url('product/cate-10')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/computer.png')}}" ></span>@lang('category.paper')</a>
                                                
                                                <ul class="ht-dropdown megamenu">
                                                  
                                                    <li class="megamenu-three-column pb-20 fix">
                                                        <ul>
                                                            
                                                            <li>
                                                                <ul>
                                                                    <li><a href="{{url('product/category/53')}}">@lang('category.printer_paper')</a></li>
                                                                    <li><a href="{{url('product/category/54')}}">@lang('category.color_paper')</a></li>
                                                                    <li><a href="{{url('product/category/55')}}">@lang('category.notebook')</a></li>
                                                                    <li><a href="{{url('product/category/56')}}">@lang('category.post_it_related')</a></li>
                                                                    <li><a href="{{url('product/category/57')}}">@lang('category.subpoena')</a></li>
                                                                    <li><a href="{{url('product/category/58')}}">@lang('category.envelope')</a></li>
                                                                    <li><a href="{{url('product/category/59')}}">@lang('category.cardboard')</a></li>
                                                                    <li><a href="{{url('product/category/60')}}">@lang('category.label_sticker')</a></li>
                                                                    <li><a href="{{url('product/category/61')}}">@lang('category.inspection_paper')</a></li>
                                                                </ul>
                                                            </li>
                                                            
                                                        </ul>
                                                    </li>
                                                
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="{{url('product/cate-11')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/monitors.png')}}" ></span>@lang('category.office_supplies')</a>
                                                
                                                <ul class="ht-dropdown megamenu">
                                                  
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            
                                                            <li>
                                                            
                                                                <ul>
                                                                    <li><a href="{{url('product/category/62')}}">@lang('category.hole_puncher')</a></li>
                                                                    <li><a href="{{url('product/category/63')}}">@lang('category.stapler')</a></li>
                                                                    <li><a href="{{url('product/category/64')}}">@lang('category.scissor_paper_cutter')</a></li>
                                                                    <li><a href="{{url('product/category/65')}}">@lang('category.utility_knife')</a></li>
                                                                    <li><a href="{{url('product/category/66')}}">@lang('category.correction_tap')</a></li>
                                                                    <li><a href="{{url('product/category/67')}}">@lang('category.glue')</a></li>
                                                                    <li><a href="{{url('product/category/68')}}">@lang('category.various_tap')</a></li>
                                                                    <li><a href="{{url('product/category/69')}}">@lang('category.measuring')</a></li>
                                                                    <li><a href="{{url('product/category/70')}}">@lang('category.clip_item')</a></li>
                                                                    <li><a href="{{url('product/category/71')}}">@lang('category.stamps')</a></li>
                                                                    <li><a href="{{url('product/category/72')}}">@lang('category.guard_film')</a></li>
                                                                    <li><a href="{{url('product/category/73')}}">@lang('category.stationeries')</a></li>
                                                                    <li><a href="{{url('product/category/74')}}">@lang('category.package_items')</a></li>
                                                                </ul>
                                                            </li>
                                                            
                                                                                                    
                                                        </ul>
                                                    </li>
                                                    <!-- Mega-Menu Three Column End -->
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="{{url('product/cate-12')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/component.png')}}" ></span>@lang('category.business_supplies')</a>
                                                
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Two Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            
                                                            <li>
                                                                <ul>
                                                                   
                                                                    <li><a href="{{url('product/category/75')}}">@lang('category.calculator')</a></li>
                                                                    <li><a href="{{url('product/category/76')}}">@lang('category.battery')</a></li>
                                                                    <li><a href="{{url('product/category/77')}}">@lang('category.usb_mouse_')</a></li>
                                                                    <li><a href="{{url('product/category/78')}}">@lang('category.flash_disc')</a></li>
                                                                    <li><a href="{{url('product/category/79')}}">@lang('category.printing_devices')</a></li>
                                                                    
                                                                </ul>
                                                            </li>
                                                            
                                                            
                                                        </ul>
                                                    </li>
                                                    
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="{{url('product/cate-13')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/networking.png')}}" ></span>@lang('category.pen')</a>
                                                
                                                <ul class="ht-dropdown megamenu">
                                                  
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            
                                                            <li>
                                                                <ul>
                                                                    <li><a href="{{url('product/category/80')}}">@lang('category.gel_pen')</a></li>
                                                                    <li><a href="{{url('product/category/81')}}">@lang('category.ball_pen')</a></li>
                                                                    <li><a href="{{url('product/category/82')}}">@lang('category.mike_oil')</a></li>
                                                                    <li><a href="{{url('product/category/83')}}">@lang('category.pencil_')</a></li>
                                                                    <li><a href="{{url('product/category/84')}}">@lang('category.automatic_')</a></li>
                                                                    <li><a href="{{url('product/category/85')}}">@lang('category.fountain_pen')</a></li>
                                                                    <li><a href="{{url('product/category/86')}}">@lang('category.paint_')</a></li>
                                                                    <li><a href="{{url('product/category/87')}}">@lang('category.other_pens')</a></li>
                                                                    <li><a href="{{url('product/category/88')}}">@lang('category.pencil_case')</a></li>
                                                                </ul>
                                                            </li>
                                                            
                                                            
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="{{url('product/cate-14')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/camera.png')}}" ></span>@lang('category.conference_supplies')</a>
                                                
                                                <ul class="ht-dropdown megamenu">
                                                  
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            
                                                            <li>
                                                            
                                                                <ul>
                                                                    <li><a href="{{url('product/category/89')}}">@lang('category.various_board_')</a></li>
                                                                    <li><a href="{{url('product/category/90')}}">@lang('category.bulletin_')</a></li>
                                                                    <li><a href="{{url('product/category/91')}}">@lang('category.magnet')</a></li>
                                                                    <li><a href="{{url('product/category/92')}}">@lang('category.office_appliance')</a></li>
                                                                    <li><a href="{{url('product/category/93')}}">@lang('category.hole_')</a></li>
                                                                    <li><a href="{{url('product/category/94')}}">@lang('category.data_class_')</a></li>
                                                                    <li><a href="{{url('product/category/95')}}">@lang('category.information_bag')</a></li>
                                                                    <li><a href="{{url('product/category/96')}}">@lang('category.file_box')</a></li>
                                                                    <li><a href="{{url('product/category/97')}}">@lang('category.business_card')</a></li>
                                                                    <li><a href="{{url('product/category/98')}}">@lang('category.hole_guard')</a></li>
                                                                    <li><a href="{{url('product/category/99')}}">@lang('category.seperate_')</a></li>
                                                                </ul>
                                                            </li>
                                                            
                                                            
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>



                                            <!--Office Supplies -->
                                            <li><a href="{{url('product/cate-18')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/camera.png')}}" ></span>@lang('category.office_appliances')</a>
                                                
                                                <ul class="ht-dropdown megamenu">
                                                  
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <li>
                                                                <ul>
                                                                    <li><a href="{{url('product/category/114')}}">@lang('category.hole_iron')</a></li>
                                                                    <li><a href="{{url('product/category/115')}}">@lang('category.data_class_')</a></li>
                                                                    <li><a href="{{url('product/category/116')}}">@lang('category.info_bag')</a></li>
                                                                    <li><a href="{{url('product/category/117')}}">@lang('category.file_box_')</a></li>
                                                                    <li><a href="{{url('product/category/118')}}">@lang('category.business_card_box')</a></li>
                                                                    <li><a href="{{url('product/category/119')}}">@lang('category.hole_guard')</a></li>
                                                                    <li><a href="{{url('product/category/120')}}">@lang('category.separate_')</a></li>
                                                                </ul>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>



                                            <li><a href="{{url('product/cate-15')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/printer.png')}}" ></span>@lang('category.tool')</a>
                                                
                                                <ul class="ht-dropdown megamenu">
                                                  
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            
                                                            <li>
                                                                <ul>
                                                                    <li><a href="{{url('product/category/100')}}">@lang('category.tool_set')</a></li>
                                                                    <li><a href="{{url('product/category/101')}}">@lang('category.hand_tool')</a></li>
                                                                </ul>
                                                            </li>
                                                            
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="{{url('product/cate-16')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/storage.png')}}" ></span>@lang('category.cleaning_supplies')</a>
                                                <ul class="ht-dropdown megamenu">
                                                  
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            
                                                            <li>
                                                                <ul>
                                                                    <li><a href="{{url('product/category/102')}}">@lang('category.labor_')</a></li>
                                                                    <li><a href="{{url('product/category/103')}}">@lang('category.detergents_')</a></li>
                                                                    <li><a href="{{url('product/category/104')}}">@lang('category.toilet_paper')</a></li>
                                                                    <li><a href="{{url('product/category/105')}}">@lang('category.mop_broom_')</a></li>
                                                                    <li><a href="{{url('product/category/106')}}">@lang('category.rag')</a></li>
                                                                    <li><a href="{{url('product/category/107')}}">@lang('category.cotton_gloves_')</a></li>
                                                                    <li><a href="{{url('product/category/108')}}">@lang('category.brush_melon_')</a></li>
                                                                    <li><a href="{{url('product/category/109')}}">@lang('category.other_')</a></li>
                                                                    <li><a href="{{url('product/category/110')}}">@lang('category.air_purifier')</a></li>
                                                                </ul>
                                                            </li>
                                                            
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href="{{url('product/cate-17')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/storage.png')}}" ></span>@lang('category.light_')</a>
                                                <ul class="ht-dropdown megamenu">
                                                  
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            
                                                            <li>
                                                                <ul>
                                                                    <li><a href="{{url('product/category/111')}}">@lang('category.led')</a></li>
                                                                    <li><a href="{{url('product/category/112')}}">@lang('category.extenstion_cord')</a></li>
                                                                    <li><a href="{{url('product/category/113')}}">@lang('category.other_electronics')</a></li>
                                                                 </ul>
                                                            </li>
                                                            
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href="{{url('product/cate-19')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/storage.png')}}" ></span>@lang('category.table_chair')</a>
                                                <ul class="ht-dropdown megamenu">
                                                  
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <li>
                                                                <li><a href="{{url('product/category/121')}}">@lang('category.table_group')</a></li>
                                                                <li><a href="{{url('product/category/122')}}">@lang('category.file_cabinet')</a></li>
                                                                <li><a href="{{url('product/category/123')}}">@lang('category.seat')</a></li>
                                                                <li><a href="{{url('product/category/124')}}">@lang('category.public_equipment')</a></li>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            
                                            <!-- More Categoies Start -->
                                            <li id="cate-toggle" class="category-menu">
                                                <ul>
                                                    <li class="has-sub"> <a href="#">More Categories</a>
                                                        <ul class="category-sub">
                                                            <li><span><img src="{{ asset ('frontend/img/vertical-menu/5.png')}}" ></span> <a href="javascript:void(0)">No more categories</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <!-- More Categoies End -->
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        <!-- Primary Vertical-Menu End -->
                        <!-- Search Box Start -->
                        <div class="col-lg-6 col-md-5 col-sm-8">
                            <div class="search-box-view fix">
                            <form action="{{url('product/search')}}" method="GET">
                                    <input class="email" type="text" placeholder="@lang('home.search_in_store')" name="filter" id="search">
                                    <button type="submit" class="submit"></button>
                                </form>
                            </div>
                        </div>
                        <!-- Search Box End -->
                        <!-- Cartt Box Start -->
                        <div class="col-lg-2 col-md-3 col-sm-4">
                            <div class="cart-box hidden-xs">
                                <ul>
                                    <li>
                                    <a href="{{ route('mycart')}}"><span class="cart-text">@lang('home.my_cart')</span>
                                            <span class="cart-counter">
                                                    @isset($mycart) {{ count($mycart)}} @endisset item(s)
                                            </span>
                                        </a>
                                        
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Cartt Box End -->
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Container End -->
            </div>
            <!-- Header Bottom End -->
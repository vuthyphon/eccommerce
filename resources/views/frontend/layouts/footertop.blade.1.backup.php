

<!-- Footer Start -->
<footer>
    <!-- Footer Top Start -->
    <div class="footer-top ptb-40 white-bg">
        <div class="container">
            <div class="row">
                <!-- Single Footer Start -->
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="single-footer">
                        <div class="footer-logo mb-20">
                            <a href="#"><img class="img" src="{{ asset('frontend/img/logo/logo.png')}}" alt="logo-img"></a>
                        </div>
                        <div class="footer-content">
                            <ul class="footer-list first-content">
                                <li><i class="zmdi zmdi-phone-in-talk"></i> +(1234) 567 890</li>
                                <li><i class="zmdi zmdi-email"></i><a href="#">mailto:info@roadthemes.com</a></li>
                                <li>
                                    <i class="zmdi zmdi-pin-drop"></i> Address : No 40 Baria Sreet 133/2 <br> NewYork City, NY, United States.
                                </li>
                                <li></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Single Footer Start -->
                <!-- Single Footer Start -->
                <div class="col-lg-3 col-md-2 col-sm-6">
                    <div class="single-footer">
                        <h3 class="footer-title">my account</h3>
                        <div class="footer-content">
                            <ul class="footer-list">
                                <li><a href="#">sitemap</a></li>
                                <li><a href="#">privacy policy</a></li>
                                <li><a href="#">your account</a></li>
                                <li><a href="#">adanced search</a></li>
                                <li><a href="#">contact us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Single Footer Start -->
                <!-- Single Footer Start -->
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="single-footer">
                        <h3 class="footer-title">Payment & Shipping</h3>
                        <div class="footer-content">
                            <ul class="footer-list">
                                <li><a href="#">terms of use</a></li>
                                <li><a href="#">payment method</a></li>
                                <li><a href="#">shipping guide</a></li>
                                <li><a href="#">locations we ship to</a></li>
                                <li><a href="#">estimated delivery time</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Single Footer Start -->
                <!-- Single Footer Start -->
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="single-footer">
                        <h3 class="footer-title">customer service</h3>
                        <div class="footer-content">
                            <ul class="footer-list">
                                <li><a href="#">shipping policy</a></li>
                                <li><a href="#">Compensation First</a></li>
                                <li><a href="#">my account</a></li>
                                <li><a href="#">return policy</a></li>
                                <li><a href="#">contact us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Single Footer Start -->
            </div>
            <!-- Row End -->
        </div>
        <!-- Container End -->
    </div>
    <!-- Footer Top End -->
    <!-- Footer Middle Start -->
    <div class="footer-middle ptb-30">
        <div class="container">
            <div class="row">
                <!-- Single Payment Service Start -->
                <div class="col-sm-4">
                    <div class="single-service">
                        <div class="service-des">
                            <h3>safe-payments</h3>
                            <p>Pay with the world’s most popular and secure payment methods.</p>
                        </div>
                    </div>
                </div>
                <!-- Single Payment Service End -->
                <!-- Single Payment Service Start -->
                <div class="col-sm-4">
                    <div class="single-service serve-two">
                        <div class="service-des">
                            <h3>Worldwide Delivery</h3>
                            <p>With sites in 5 languages, we shop to over 100 countries and regions.</p>
                        </div>
                    </div>
                </div>
                <!-- Single Payment Service End -->
                <!-- Single Payment Service Start -->
                <div class="col-sm-4">
                    <div class="single-service serve-three">
                        <div class="service-des">
                            <h3>24/7 Help Center</h3>
                            <p>Round-the-clock assistance for a smooth shopping experience.</p>
                        </div>
                    </div>
                </div>
                <!-- Single Payment Service End -->
            </div>
            <!-- Row End -->
        </div>
        <!-- Container End -->
    </div>
    <!-- Footer Middle End -->
    <!-- Footer Bottom Start -->
    <div class="footer-bottom ptb-30 white-bg">
        <div class="container">
            <p class="f-left">Copyright © 2019 <a href="https://honesttechnic.com/">Honesttechnic</a> All Rights Reserved.</p>
            
        </div>
        <!-- Container End -->
    </div>
    <!-- Footer Bottom End -->
</footer>
<!-- Footer End -->
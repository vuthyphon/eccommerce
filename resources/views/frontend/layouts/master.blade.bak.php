<!doctype html>
<html class="no-js" lang="en">

<head>
<meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="Default Description">
    <meta name="keywords" content="E-commerce" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('frontend/img/icon/favicon.png')}}">
    
    <!-- Google Font Open Sans -->
    <!--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,900" rel="stylesheet">-->
    <!-- mobile menu css -->
    
    <link rel="stylesheet" href="{{ asset ('frontend/css/meanmenu.min.css')}}">
    <!-- animate css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/animate.css')}}">
    <!-- nivo slider css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/nivo-slider.css')}}">
    <!-- owl carousel css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/owl.carousel.min.css')}}">
    <!-- price slider css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/jquery-ui.min.css')}}">
    <!-- fancybox css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/jquery.fancybox.css')}}">
    <!-- material design css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/material-design-iconic-font.min.css')}}">
    <link rel="stylesheet" href="{{ asset ('frontend/css/font-awesome.min.css')}}">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/bootstrap.min.css')}}">
    <!-- default css  -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/default.css')}}">
    <!-- style css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/style.css')}}">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/responsive.css')}}">
    <!-- modernizr js -->
    <script src="{{ asset ('frontend/js/vendor/modernizr-2.8.3.min.js')}}"></script>

    @yield('functionalscript') 

</head>

<body>
    <?php 
        $langauge=array('en'=>'English','kh'=>'Khmer','cn'=>'Chinese','vn'=>'Vietnam');
        $lang=session('locale');
    ?>
    

    

    
    <div class="wrapper">
               
        <!-- Header Area Start -->
        <header>
            <!-- Header Top Start -->
            <div class="header-top white-bg">
                <div class="container">
                    <div class="row">
                        <!-- Header Top left Start -->
                        <div class="col-md-3 col-sm-4 col-xs-12">
                            <div class="header-top-left f-left">
                                <ul class="header-list-menu">
                                    <!-- Language Start -->
                                    <li><a href="javascript:void(0);"><?php echo $lang ? $lang :'EN' ?></a>
                                        <ul class="ht-dropdown">
                                            <li><a href="{{url('locale/kh')}}">Khmer</a></li>
                                            <li><a href="{{url('locale/en')}}">English</a></li>
                                            <li><a href="{{url('locale/cn')}}">Chinese</a></li>
                                            <li><a href="{{url('locale/vn')}}">Vietnam</a></li>
                                        </ul>
                                    </li>
                                    
                                </ul>
                                <!-- Header-list-menu End -->
                            </div>
                        </div>
                        <!-- Header Top left End -->
                        
                        <!-- Header Top Right Start -->
                        <div class="col-md-9 col-sm-8 col-xs-12">
                            <div class="header-top-right f-right header-top-none">
                                <ul class="header-list-menu right-menu">
                                    <li><a href="#">@lang('home.my_account')</a>
                                        <ul class="ht-dropdown ht-account" style="text-transform: lowercase;">
                                            @if(Session::has('usr'))
                                                <li><a href="#"> {{ 'Hi! '.session('usr.first_name')}}</a></li>
                                                <li><a href="{{ url('mycart')}}"> @lang('home.my_cart')</a></li>
                                                <li><a href="{{ route('signout')}}"> Sign Out</a></li>
                                            @else
                                                <li><a href="{{ route('signin')}}"> Sign In</a></li>
                                            @endif
                                            
                                        </ul>
                                    </li>
                                    
                                    <li><a href="{{route('checkout')}}" class="@yield('active-checkout')">@lang('home.checkout')</a></li>
                                </ul>
                                <!-- Header-list-menu End -->
                            </div>
                            
                            <div class="small-version">
                                <div class="header-top-right f-right">
                                    <ul class="header-list-menu right-menu">
                                        
                                        <li><a href="{{route('signin')}}"><i class="fa fa-user"></i></a></li>
                                        <li><a href="{{url('register')}}"><i class="fa fa-registered"></i></a></li>
                                        <li><a href="{{route('checkout')}}"><i class="fa fa-usd"></i></a></li>
                                        <li><a href="javascript:void(0);"><?php echo $lang ? $lang :'EN' ?></a>
                                        <ul class="ht-dropdown">
                                            <li><a href="{{url('locale/kh')}}">Khmer</a></li>
                                            <li><a href="{{url('locale/en')}}">English</a></li>
                                            <li><a href="{{url('locale/cn')}}">Chinese</a></li>
                                            <li><a href="{{url('locale/vn')}}">Vietnam</a></li>
                                        </ul>
                                    </li>
                                    </ul>
                                    <!-- Header-list-menu End -->
                                </div>
                            </div>
                            
                        </div>
                        <!-- Header Top Right End -->
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Container End -->
            </div>
            <!-- Header Top End -->


            <!-- Header Middle Start -->
            <div class="header-middle ptb-20 white-bg">
                <div class="container">
                    <div class="row">
                        <!-- Logo Start -->
                        <div class="col-lg-3 col-md-4">
                            <div class="logo">
                                <a href="{{route('home')}}"><img src="{{ asset ('frontend/img/logo/logo.png')}}" alt="Honest Technic" title="Honest Technic"></a>
                            </div>
                        </div>
                        <!-- Logo End -->
                        <!-- Header Middle Menu Start -->
                        <div class="col-lg-9 col-md-8">
                            <div class="middle-menu hidden-sm hidden-xs">
                                <nav>
                                    <ul class="middle-menu-list">
                                        <li><a href="{{route('home')}}" class="@yield('active-home')" alt="Home" title="Home">@lang('home.home_menu')</a></li>
                                        <li><a href="{{route('product')}}" class="@yield('active-product')" alt="Product" title="Product">@lang('home.product_menu')</a></li>
                                        <li><a href="{{route('about')}}" class="@yield('active-about')" alt="About Honest Technic" title="About Honest Technic">@lang('home.about_menu')</a></li>
                                        <li><a href="{{url('/contact')}}" class="@yield('active-contact')" alt="Contact" title="Contact">@lang('home.contact_menu')</a></li>
                                        <li><a href="{{url('register')}}" class="@yield('active-register')" alt="Register" title="Register">@lang('home.register_menu')</a></li>
                                        <li><a href="{{route('signin')}}" class="@yield('active-login')" alt="Signin" title="Signin">@lang('home.sign_in_menu')</a></li>
                                        
                                    </ul>
                                </nav>
                            </div>

                           <!-- Main Cart Box Start 
                            Mobile view for check out
                            -->
                            <div class="cart-box visible-xs">
                                <ul>
                                    <li>
                                       
                                        
                                        <a href="{{route('mycart')}}">
                                        
                                                @if(count($mycart)>0)
                                                    <span style="position: absolute;
                                                    right: -3px;
                                                    top: -7px;
                                                    background: red;
                                                    color: #fff;
                                                    width: 16px;
                                                    height: 16px;
                                                    border-radius: 50%;
                                                    text-align: center;
                                                    font-size: 10px;
                                                    line-height: 16px;"> {{ count($mycart)}}</span>
                                                @endif    
                                        </a> 
                                        
                                    </li>
                                </ul>
                            </div>
                            <!-- Main Cart Box End -->

                            
                            <!-- Mobile Menu  Start -->
                            <div class="mobile-menu visible-sm visible-xs">
                                    <nav>
                                        <ul>
                                            <li><a href="{{route('home')}}">@lang('home.home_menu')</a></li>
                                            <li><a href="{{url('product/cate-1')}}">@lang('category.computer')</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="{{url('product/category/1')}}">@lang('category.all_in_one')</a></li>
                                                    <li><a href="{{url('product/category/2')}}">@lang('category.desktop')</a></li>
                                                    <li><a href="{{url('product/category/3')}}">@lang('category.laptop')</a></li>
                                                    <li><a href="{{url('product/category/4')}}">@lang('category.server')</a></li>
                                                    <li><a href="{{url('product/category/7')}}">@lang('category.workstation')</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="{{url('product/cate-2')}}">@lang('category.monitor_display')</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="{{url('product/category/5')}}">@lang('category.tn_panel')</a></li>
                                                    <li><a href="{{url('product/category/6')}}">@lang('category.ips_panel')</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="{{url('product/cate-3')}}">@lang('category.comphonent_periperal')</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="{{url('product/category/8')}}">@lang('category.computer_case')</a></li>
                                                    <li><a href="{{url('product/category/9')}}">CPU</a></li>
                                                    <li><a href="{{url('product/category/10')}}">@lang('category.graphic_card')</a></li>
                                                    <li><a href="{{url('product/category/11')}}">@lang('category.hdd')</a></li>
                                                    <li><a href="{{url('product/category/12')}}">@lang('category.motherboard')</a></li>
                                                    <li><a href="{{url('product/category/13')}}">@lang('category.power_supply')</a></li>
                                                    <li><a href="{{url('product/category/14')}}">@lang('category.ram')</a></li>
                                                    <li><a href="{{url('product/category/15')}}">@lang('category.ups')</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="{{url('product/cate-4')}}">@lang('category.networking')</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="{{url('product/category/16')}}">@lang('category.media_converter')</a></li>
                                                    <li><a href="{{url('product/category/17')}}">@lang('category.mikrotik')</a></li>
                                                    <li><a href="{{url('product/category/18')}}">@lang('category.network_cable')</a></li>
                                                    <li><a href="{{url('product/category/19')}}">@lang('category.network_switch')</a></li>
                                                    <li><a href="{{url('product/category/20')}}">@lang('category.patch_panel')</a></li>
                                                    <li><a href="{{url('product/category/21')}}">@lang('category.pci_wireless_card')</a></li>
                                                    <li><a href="{{url('product/category/22')}}">@lang('category.pdu')</a></li>
                                                    <li><a href="{{url('product/category/23')}}">@lang('category.rj45')</a></li>
                                                    <li><a href="{{url('product/category/24')}}">@lang('category.router_wireless')</a></li>
                                                    <li><a href="{{url('product/category/25')}}">@lang('category.server_rack')</a></li>
                                                    <li><a href="{{url('product/category/26')}}">@lang('category.usb_adapter_wireless')</a></li>
                                                    <li><a href="{{url('product/category/27')}}">@lang('category.wifi_rage_extender')</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="{{url('product/cate-5')}}">@lang('category.security_camera')</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="{{url('product/category/28')}}">@lang('category.camera')</a></li>
                                                    <li><a href="{{url('product/category/29')}}">@lang('category.camera_cable')</a></li>
                                                    <li><a href="{{url('product/category/30')}}">@lang('category.cctv_cable_balun')</a></li>
                                                    <li><a href="{{url('product/category/31')}}">@lang('category.dvr_nvr')</a></li>
                                                    <li><a href="{{url('product/category/32')}}">@lang('category.ip_camera')</a></li>
                                                    <li><a href="{{url('product/category/33')}}">@lang('category.power_supply')</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="{{url('product/cate-6')}}">@lang('category.printer')</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="{{url('product/category/34')}}">@lang('category.b_w_printer')</a></li>
                                                    <li><a href="{{url('product/category/35')}}">@lang('category.color_printer')</a></li>
                                                    <li><a href="{{url('product/category/36')}}">@lang('category.dot_matrix_printer')</a></li>
                                                    <li><a href="{{url('product/category/37')}}">@lang('category.printer_ink')</a></li>
                                                    <li><a href="{{url('product/category/38')}}">@lang('category.printer_ribbon')</a></li>
                                                    <li><a href="{{url('product/category/39')}}">@lang('category.printer_tonner')</a></li>
                                                    <li><a href="{{url('product/category/40')}}">@lang('category.scanner')</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="{{url('product/cate-7')}}">@lang('category.accessories')</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="{{url('product/category/41')}}">@lang('category.connector')</a></li>
                                                    <li><a href="{{url('product/category/42')}}">@lang('category.keyboard')</a></li>
                                                    <li><a href="{{url('product/category/43')}}">@lang('category.mouse')</a></li>
                                                    <li><a href="{{url('product/category/44')}}">@lang('category.mouse_pad')</a></li>
                                                    <li><a href="{{url('product/category/45')}}">@lang('category.webcam')</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="{{url('product/cate-8')}}">@lang('category.stationery')</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="{{url('product/category/46')}}">@lang('category.pen')</a></li>
                                                    <li><a href="{{url('product/category/47')}}">@lang('category.paper')</a></li>
                                                    <li><a href="{{url('product/category/48')}}">@lang('category.office_accessory')</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="{{url('product/cate-9')}}">@lang('category.factory_accessory')</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="{{url('product/category/49')}}">@lang('category.tool')</a></li>
                                                    <li><a href="{{url('product/category/50')}}">@lang('category.cleaning')</a></li>
                                                    <li><a href="{{url('product/category/51')}}">@lang('category.light')</a></li>
                                                    <li><a href="{{url('product/category/52')}}">@lang('category.accessories')</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="{{route('about')}}">@lang('home.about_menu')</a></li>
                                            <li><a href="{{url('/contact')}}">@lang('home.contact_menu')</a></li>
                                            <li><a href="{{ route('signout')}}">@lang('home.signout')</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            <!-- Mobile Menu  End -->
							
							
							
                        </div>
                        <!-- Header Middle Menu End -->
						
						
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Container End -->
            </div>
            <!-- Header Middle End -->


            <!-- Header Bottom Start -->
            <div class="header-bottom ptb-10 blue-bg">
                <div class="container">
                    <div class="row">

                    
                        <!-- Primary Vertical-Menu Start -->
                        <div class="col-lg-4 col-md-4 col-sm-4 hidden-sm hidden-xs">
                                <div class="vertical-menu">
                                    <span class="categorie-title">@lang('category.cate_menu')</span>
                                    <nav>
                                        @if (View::hasSection('home'))
                                            <ul class="vertical-menu-list">
                                        
                                        @else
                                            <ul class="vertical-menu-list menu-hideen">
                                        @endif
                                        
                                        
                                            <li><a href="{{url('product/cate-1')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/computer.png')}}" alt="menu-icon"></span>@lang('category.computer')</a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column pb-20 fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="{{url('product/category/1')}}">@lang('category.all_in_one')</a></li>
                                                                    <li><a href="{{url('product/category/2')}}">@lang('category.desktop')</a></li>
                                                                    <li><a href="{{url('product/category/3')}}">@lang('category.laptop')</a></li>
                                                                    <li><a href="{{url('product/category/4')}}">@lang('category.server')</a></li>
                                                                    <li><a href="{{url('product/category/7')}}">@lang('category.workstation')</a></li>
                                                                    
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                        </ul>
                                                    </li>
                                                
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="{{url('product/cate-2')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/monitors.png')}}" alt="menu-icon"></span>@lang('category.monitor_display')</a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                            
                                                                <ul>
                                                                    <li><a href="{{url('product/category/5')}}">@lang('category.tn_panel')</a></li>
                                                                    <li><a href="{{url('product/category/6')}}">@lang('category.ips_panel')</a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                                                                    
                                                        </ul>
                                                    </li>
                                                    <!-- Mega-Menu Three Column End -->
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="{{url('product/cate-3')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/component.png')}}" alt="menu-icon"></span>@lang('category.comphonent_periperal')</a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Two Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="{{url('product/category/8')}}">@lang('category.computer_case')</a></li>
                                                                    <li><a href="{{url('product/category/9')}}">CPU</a></li>
                                                                    <li><a href="{{url('product/category/10')}}">@lang('category.graphic_card')</a></li>
                                                                    <li><a href="{{url('product/category/11')}}">@lang('category.hdd')</a></li>
                                                                    <li><a href="{{url('product/category/12')}}">@lang('category.motherboard')</a></li>
                                                                    <li><a href="{{url('product/category/13')}}">@lang('category.power_supply')</a></li>
                                                                    <li><a href="{{url('product/category/14')}}">@lang('category.ram')</a></li>
                                                                    <li><a href="{{url('product/category/15')}}">@lang('category.ups')</a></li>
                                                                    
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                            
                                                        </ul>
                                                    </li>
                                                    
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="{{url('product/cate-4')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/networking.png')}}" alt="menu-icon"></span>@lang('category.networking')</a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="{{url('product/category/16')}}">@lang('category.media_converter')</a></li>
                                                                    <li><a href="{{url('product/category/17')}}">@lang('category.mikrotik')</a></li>
                                                                    <li><a href="{{url('product/category/18')}}">@lang('category.network_cable')</a></li>
                                                                    <li><a href="{{url('product/category/19')}}">@lang('category.network_switch')</a></li>
                                                                    <li><a href="{{url('product/category/20')}}">@lang('category.patch_panel')</a></li>
                                                                    <li><a href="{{url('product/category/21')}}">@lang('category.pci_wireless_card')</a></li>
                                                                    <li><a href="{{url('product/category/22')}}">@lang('category.pdu')</a></li>
                                                                    <li><a href="{{url('product/category/23')}}">@lang('category.rj45')</a></li>
                                                                    <li><a href="{{url('product/category/24')}}">@lang('category.router_wireless')</a></li>
                                                                    <li><a href="{{url('product/category/25')}}">@lang('category.server_rack')</a></li>
                                                                    <li><a href="{{url('product/category/26')}}">@lang('category.usb_adapter_wireless')</a></li>
                                                                    <li><a href="{{url('product/category/27')}}">@lang('category.wifi_rage_extender')</a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                            
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="{{url('product/cate-5')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/camera.png')}}" alt="menu-icon"></span>@lang('category.security_camera')</a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                            
                                                                <ul>
                                                                    <li><a href="{{url('product/category/28')}}">@lang('category.camera')</a></li>
                                                                    <li><a href="{{url('product/category/29')}}">@lang('category.camera_cable')</a></li>
                                                                    <li><a href="{{url('product/category/30')}}">@lang('category.cctv_cable_balun')</a></li>
                                                                    <li><a href="{{url('product/category/31')}}">@lang('category.dvr_nvr')</a></li>
                                                                    <li><a href="{{url('product/category/32')}}">@lang('category.ip_camera')</a></li>
                                                                    <li><a href="{{url('product/category/33')}}">@lang('category.power_supply')</a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                            
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="{{url('product/cate-6')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/printer.png')}}" alt="menu-icon"></span>@lang('category.printer')</a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="{{url('product/category/34')}}">@lang('category.b_w_printer')</a></li>
                                                                    <li><a href="{{url('product/category/35')}}">@lang('category.color_printer')</a></li>
                                                                    <li><a href="{{url('product/category/36')}}">@lang('category.dot_matrix_printer')</a></li>
                                                                    <li><a href="{{url('product/category/37')}}">@lang('category.printer_ink')</a></li>
                                                                    <li><a href="{{url('product/category/38')}}">@lang('category.printer_ribbon')</a></li>
                                                                    <li><a href="{{url('product/category/39')}}">@lang('category.printer_tonner')</a></li>
                                                                    <li><a href="{{url('product/category/40')}}">@lang('category.scanner')</a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="{{url('product/cate-7')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/storage.png')}}" alt="menu-icon"></span>@lang('category.accessories')</a>
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="{{url('product/category/41')}}">@lang('category.connector')</a></li>
                                                                    <li><a href="{{url('product/category/42')}}">@lang('category.keyboard')</a></li>
                                                                    <li><a href="{{url('product/category/43')}}">@lang('category.mouse')</a></li>
                                                                    <li><a href="{{url('product/category/44')}}">@lang('category.mouse_pad')</a></li>
                                                                    <li><a href="{{url('product/category/45')}}">@lang('category.webcam')</a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href="{{url('product/cate-8')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/storage.png')}}" alt="menu-icon"></span>@lang('category.stationery')</a>
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="{{url('product/category/46')}}">@lang('category.pen')</a></li>
                                                                    <li><a href="{{url('product/category/47')}}">@lang('category.paper')</a></li>
                                                                    <li><a href="{{url('product/category/48')}}">@lang('category.office_accessory')</a></li>
                                                                 </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                             <li><a href="{{url('product/cate-9')}}"><span><img src="{{ asset ('frontend/img/vertical-menu/storage.png')}}" alt="menu-icon"></span>@lang('category.factory_accessory')</a>
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="{{url('product/category/49')}}">@lang('category.tool')</a></li>
                                                                    <li><a href="{{url('product/category/50')}}">@lang('category.cleaning')</a></li>
                                                                    <li><a href="{{url('product/category/51')}}">@lang('category.light')</a></li>
                                                                    <li><a href="{{url('product/category/52')}}">@lang('category.accessories')</a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                             
                                            
                                            <!-- More Categoies Start -->
                                            <li id="cate-toggle" class="category-menu">
                                                <ul>
                                                    <li class="has-sub"> <a href="#">More Categories</a>
                                                        <ul class="category-sub">
                                                            <li><span><img src="{{ asset ('frontend/img/vertical-menu/5.png')}}" alt="menu-icon"></span> <a href="javascript:void(0)">No more categories</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <!-- More Categoies End -->
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        <!-- Primary Vertical-Menu End -->
                        <!-- Search Box Start -->
                        <div class="col-lg-6 col-md-5 col-sm-8">
                            <div class="search-box-view fix">
                            <form action="{{url('product/search')}}" method="GET">
                                    <input class="email" type="text" placeholder="@lang('home.search_in_store')" name="filter" id="search">
                                    <button type="submit" class="submit"></button>
                                </form>
                            </div>
                        </div>
                        <!-- Search Box End -->
                        <!-- Cartt Box Start -->
                        <div class="col-lg-2 col-md-3 col-sm-4">
                            <div class="cart-box hidden-xs">
                                <ul>
                                    <li>
                                    <a href="{{ route('mycart')}}"><span class="cart-text">@lang('home.my_cart')</span>
                                            <span class="cart-counter">
                                                    @isset($mycart) {{ count($mycart)}} @endisset item(s)
                                            </span>
                                        </a>
                                        
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Cartt Box End -->
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Container End -->
            </div>
            <!-- Header Bottom End -->
            
        </header>
        <!-- Header Area End -->

        
        

        @yield('content')
        
		
		
		
		
        <!-- Newsletter& Subscribe Start -->
        <div class="subscribe black-bg ptb-15">
            <div class="container">
                <div class="row">
                    <!-- Subscribe Box Start -->
                    <div class="col-sm-6">
                        <div class="search-box-view fix">
                            <form action="#">
                                <label for="email-two">Subscribe</label>
                                <input autocomplete="off" type="text" class="email" placeholder="Enter your email address" name="email" id="email-two">
                                <button type="submit" class="submit"></button>
                            </form>
                        </div>
                    </div>
                    <!-- Subscribe Box End -->
                    <!-- Social Follow Start -->
                    <div class="col-sm-6">
                        <div class="social-follow f-right">
                            <h3>@lang('home.stay_connect')</h3>
                            <!-- Follow Box End -->
                            <ul class="follow-box">
                                <li><a href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-google"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-youtube"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                            </ul>
                            <!-- Follow Box End -->
                        </div>
                    </div>
                    <!-- Social Follow Start -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>
        <!-- Newsletter& Subscribe End -->


        @yield('footertop')
        
        @yield('loginmodal')

    
    </div>
    <!-- Wrapper End -->

    <!-- jquery 3.12.4 -->
    <script src="{{ asset ('frontend/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <!-- mobile menu js  -->
    <script src="{{ asset ('frontend/js/jquery.meanmenu.min.js')}}"></script>
    <!-- scroll-up js -->
    <script src="{{ asset ('frontend/js/jquery.scrollUp.js')}}"></script>
    <!-- owl-carousel js -->
    <script src="{{ asset ('frontend/js/owl.carousel.min.js')}}"></script>
    <!-- countdown js -->
    <script src="{{ asset ('frontend/js/jquery.countdown.min.js')}}"></script>
    <!-- wow js -->
    <script src="{{ asset ('frontend/js/wow.min.js')}}"></script>
    <!-- price slider js -->
    <script src="{{ asset ('frontend/js/jquery-ui.min.js')}}"></script>
    <!-- fancybox js -->
    <script src="{{ asset ('frontend/js/jquery.fancybox.min.js')}}"></script>
    <!-- nivo slider js -->
    <script src="{{ asset ('frontend/js/jquery.nivo.slider.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{ asset ('frontend/js/bootstrap.min.js')}}"></script>
    <!-- plugins -->
    <script src="{{ asset ('frontend/js/plugins.js')}}"></script>
    <!-- main js -->
    <script src="{{ asset ('frontend/js/main.js')}}"></script>
    
    @yield('customJs')
</body>

</html>
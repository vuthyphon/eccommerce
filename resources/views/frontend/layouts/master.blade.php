<!doctype html>
<html class="no-js" lang="en">

<head>
<meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="Default Description">
    <meta name="keywords" content="E-commerce" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('frontend/img/icon/favicon.png')}}">
    
    <!-- Google Font Open Sans -->
    <!--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,900" rel="stylesheet">-->
    <!-- mobile menu css -->
    
    <link rel="stylesheet" href="{{ asset ('frontend/css/meanmenu.min.css')}}">
    <!-- animate css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/animate.css')}}">
    <!-- nivo slider css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/nivo-slider.css')}}">
    <!-- owl carousel css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/owl.carousel.min.css')}}">
    <!-- price slider css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/jquery-ui.min.css')}}">
    <!-- fancybox css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/jquery.fancybox.css')}}">
    <!-- material design css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/material-design-iconic-font.min.css')}}">
    <link rel="stylesheet" href="{{ asset ('frontend/css/font-awesome.min.css')}}">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/bootstrap.min.css')}}">
    <!-- default css  -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/default.css')}}">
    <!-- style css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/style.css')}}">
    <!-- responsive css -->
    <link rel="stylesheet" href="{{ asset ('frontend/css/responsive.css')}}">
    <!-- modernizr js -->
    <script src="{{ asset ('frontend/js/vendor/modernizr-2.8.3.min.js')}}"></script>

    @yield('functionalscript') 

</head>

<body>
    <?php 
        $langauge=array('en'=>'English','kh'=>'Khmer','cn'=>'Chinese','vn'=>'Vietnam');
        $lang=session('locale');
    ?>
    

    

    
    <div class="wrapper">
               
        <!-- Header Area Start -->
        <header>
            <!-- Header Top Start -->
            <div class="header-top white-bg">
                <div class="container">
                    <div class="row">
                        <!-- Header Top left Start -->
                        <div class="col-md-3 col-sm-4 col-xs-12">
                            <div class="header-top-left f-left">
                                <ul class="header-list-menu">
                                    <!-- Language Start -->
                                    <li><a href="javascript:void(0);"><?php echo $lang ? $lang :'EN' ?></a>
                                        <ul class="ht-dropdown">
                                            <li><a href="{{url('locale/kh')}}">Khmer</a></li>
                                            <li><a href="{{url('locale/en')}}">English</a></li>
                                            <li><a href="{{url('locale/cn')}}">Chinese</a></li>
                                            <li><a href="{{url('locale/vn')}}">Vietnam</a></li>
                                        </ul>
                                    </li>
                                    
                                </ul>
                                <!-- Header-list-menu End -->
                            </div>
                        </div>
                        <!-- Header Top left End -->
                        
                        <!-- Header Top Right Start -->
                        <div class="col-md-9 col-sm-8 col-xs-12">
                            <div class="header-top-right f-right header-top-none">
                                <ul class="header-list-menu right-menu">
                                    <li><a href="#">@lang('home.my_account')</a>
                                        <ul class="ht-dropdown ht-account" style="text-transform: lowercase;">
                                            @if(Session::has('usr'))
                                                <li><a href="#"> {{ 'Hi! '.session('usr.first_name')}}</a></li>
                                                <li><a href="{{ url('mycart')}}"> @lang('home.my_cart')</a></li>
                                                <li><a href="{{ route('signout')}}"> Sign Out</a></li>
                                            @else
                                                <li><a href="{{ route('signin')}}"> Sign In</a></li>
                                            @endif
                                            
                                        </ul>
                                    </li>
                                    
                                    <li><a href="{{route('checkout')}}" class="@yield('active-checkout')">@lang('home.checkout')</a></li>
                                </ul>
                                <!-- Header-list-menu End -->
                            </div>
                            
                            <div class="small-version">
                                <div class="header-top-right f-right">
                                    <ul class="header-list-menu right-menu">
                                        
                                        <li><a href="{{route('signin')}}"><i class="fa fa-user"></i></a></li>
                                        <li><a href="{{url('register')}}"><i class="fa fa-registered"></i></a></li>
                                        <li><a href="{{route('checkout')}}"><i class="fa fa-usd"></i></a></li>
                                        <li><a href="javascript:void(0);"><?php echo $lang ? $lang :'EN' ?></a>
                                        <ul class="ht-dropdown">
                                            <li><a href="{{url('locale/kh')}}">Khmer</a></li>
                                            <li><a href="{{url('locale/en')}}">English</a></li>
                                            <li><a href="{{url('locale/cn')}}">Chinese</a></li>
                                            <li><a href="{{url('locale/vn')}}">Vietnam</a></li>
                                        </ul>
                                    </li>
                                    </ul>
                                    <!-- Header-list-menu End -->
                                </div>
                            </div>
                            
                        </div>
                        <!-- Header Top Right End -->
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Container End -->
            </div>
            <!-- Header Top End -->

        
        @include('frontend.layouts.category_menu')
            
        </header>
        <!-- Header Area End -->

        
        

        @yield('content')
        
		
		
		
		
        <!-- Newsletter& Subscribe Start -->
        <div class="subscribe black-bg ptb-15">
            <div class="container">
                <div class="row">
                    <!-- Subscribe Box Start -->
                    <div class="col-sm-6">
                        <div class="search-box-view fix">
                            <form action="#">
                                <label for="email-two">Subscribe</label>
                                <input autocomplete="off" type="text" class="email" placeholder="Enter your email address" name="email" id="email-two">
                                <button type="submit" class="submit"></button>
                            </form>
                        </div>
                    </div>
                    <!-- Subscribe Box End -->
                    <!-- Social Follow Start -->
                    <div class="col-sm-6">
                        <div class="social-follow f-right">
                            <h3>@lang('home.stay_connect')</h3>
                            <!-- Follow Box End -->
                            <ul class="follow-box">
                                <li><a href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-google"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-youtube"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                            </ul>
                            <!-- Follow Box End -->
                        </div>
                    </div>
                    <!-- Social Follow Start -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>
        <!-- Newsletter& Subscribe End -->


        @yield('footertop')
        
        @yield('loginmodal')

    
    </div>
    <!-- Wrapper End -->

    <!-- jquery 3.12.4 -->
    <script src="{{ asset ('frontend/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <!-- mobile menu js  -->
    <script src="{{ asset ('frontend/js/jquery.meanmenu.min.js')}}"></script>
    <!-- scroll-up js -->
    <script src="{{ asset ('frontend/js/jquery.scrollUp.js')}}"></script>
    <!-- owl-carousel js -->
    <script src="{{ asset ('frontend/js/owl.carousel.min.js')}}"></script>
    <!-- countdown js -->
    <script src="{{ asset ('frontend/js/jquery.countdown.min.js')}}"></script>
    <!-- wow js -->
    <script src="{{ asset ('frontend/js/wow.min.js')}}"></script>
    <!-- price slider js -->
    <script src="{{ asset ('frontend/js/jquery-ui.min.js')}}"></script>
    <!-- fancybox js -->
    <script src="{{ asset ('frontend/js/jquery.fancybox.min.js')}}"></script>
    <!-- nivo slider js -->
    <script src="{{ asset ('frontend/js/jquery.nivo.slider.js')}}"></script>
    <!-- bootstrap -->
    <script src="{{ asset ('frontend/js/bootstrap.min.js')}}"></script>
    <!-- plugins -->
    <script src="{{ asset ('frontend/js/plugins.js')}}"></script>
    <!-- main js -->
    <script src="{{ asset ('frontend/js/main.js')}}"></script>
    
    @yield('customJs')
</body>

</html>

@extends('frontend/layouts.master')
@section('title', 'Home | Welcome to Honest Technic')

@section('functionalscript')

    <script>

        function addtocart(id)
        {
            
            $.ajax({
                type: "get",
                url: "{{ url('addtocart')}}/"+id,
                //data:'_token = <?php echo csrf_token() ?>',
                success: function(msg) {
                    if(msg=='login')
                    {
                        $('#loginModal').modal(); 
                    }
                    else
                    {
                        $('#toast').html(msg);
                        $('#toast').show(1000);
                    }
                }
            });
        }
        
        function loginAjax()
        {
            $.ajax({
                type: "get",
                url: "{{ url('loginajax')}}",
                data:{
                    e_mail: $('#email').val(),
                    password: $('#paswd').val()
                },
                success: function(msg) {
                    if(msg=='LoginOk'){
                        $('#loginModal').modal('hide');
                    }
                    $('#loginmsg').html(msg);
                }
            });
        }
        
    </script>  

@endsection

@section('active-home', 'nav-active')
@section ('home')
@endsection

@section ('content')

@include('frontend.layouts.slider')

        <!-- New Products Start -->
        <div class="new-single-products pb-50" style="background:#fbfbfb;">
                <br><br>
            <div class="container">
                <!-- Group Title Start -->
                <div class="group-title">
                    <h2>@lang('home.new_product')</h2>
                </div>
                <!-- Group Title End -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- New Products Activaion Start -->
                        <div class="new-products second-featured-pro owl-carousel">
                            
                            @foreach ($newproduct as $item)

                            <!-- Single Products Start -->
                            <div class="single-product" style="height:229.118px">
                                    <!-- Product Image Start -->
                                    <div class="pro-img">
                                        <a href="{{url('product/detail/'.$item->item_id)}}">
                                            @if($item->feature_image)
                                                <img class="primary-img" src="{{asset('storage/photos/'.$item->feature_image->filename)}}" alt="single-product">
                                            @else
                                               <img class="primary-img" src="{{asset('frontend/img/no-image.png')}}" alt="single-product">
                                            @endif                                           
                                        </a>
                                        <div class="quick-view">
                                        <a href="{{url('product/detail/'.$item->item_id)}}" alt="">Quick View</a>
    
                                        </div>
                                    </div>
                                    <!-- Product Image End -->
                                    <!-- Product Content Start -->
                                    <div class="pro-content">
                                        <h4><a href="{{url('product/detail/'.$item->item_id)}}">{{$item->item_name_en}}</a></h4>
                                            <p><span>${!! $item->item_price? number_format($item->item_price->unit_price,2) : 0.00 !!}</span></p>
                                        <div class="pro-actions">
                                            <div class="actions-primary">
                                                    <a href="javascript:void(0)" onclick="addtocart({{$item->item_id}});" data-toggle="tooltip" title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            </div>
                                        </div>

                                        <?php
                                            if($item->tag!='')
                                                echo "<span class='sticker-new pro-sticker'>$item->tag</span>";
                                                ?>
                                        
                                    </div>
                                    <!-- Product Content End -->
                                </div>
                                <!-- Single Products End -->
                            @endforeach
                            
                        </div>
                        <!-- New Products Carousel Activaion End -->
                    </div>
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>
        <!-- New Products End -->
        <!-- Best Seller Product Start -->
        <div class="best-seller-product pb-50" style="background:#fbfbfb;">
            <div class="container">
                <div class="row">
			
                    <!-- Hot Deal Start -->
                    <div class="col-md-4">
                        <div class="sidebar-ads" style="height:417px; background: #fff;">
                            @if($item->feature_image)
                                <a href="javascript:void()"><img class="full-img" src="{{asset('storage/photos/'.$item->feature_image->filename)}}" alt="ads-image"></a>
                            @elseif($item->item_price==false)
                                <img class="primary-img" src="{{asset('frontend/img/no-image.png')}}" alt="single-product">
                            @endif
                        </div>
                    </div>
                    <!-- Hot Deal End -->
					
                    <!-- Best Seller Start -->
                    <div class="col-md-8">
                        <!-- Group Title Start -->
                        <div class="group-title mts">
                            <h2>@lang('home.popular_product')</h2>
                        </div>
                        <!-- Group Title End -->
                        <div class="best-seller owl-carousel">

                            <div class="double-product">

                            <?php $i=0;?>

                            @foreach ($bestseller as $item)
                                
                            <?php $i+=1;?>
                            @if($i%2==0)
                            
                                <!-- Single Products Start -->
                                <div class="single-product" style="height:183.61px;">
                                        <!-- Product Image Start -->
                                        <div class="pro-img">
                                            @if($item->feature_image)
                                                <a href="{{url('product/detail/'.$item->item_id)}}"><img class="full-img" src="{{asset('storage/photos/'.$item->feature_image->filename)}}" alt="ads-image"></a>
                                            @elseif($item->item_price==false)
                                                <img class="primary-img" src="{{asset('frontend/img/no-image.png')}}" alt="single-product">
                                            @endif
                                            <div class="quick-view">
                                                <a href="{{url('product/detail/'.$item->item_id)}}" data-toggle="modal" data-target="#myModal">Quick View</a>
                                            </div>
                                        </div>
                                        <!-- Product Image End -->
                                        <!-- Product Content Start -->
                                        <div class="pro-content">
                                            <h4><a href="{{url('product/detail/'.$item->item_id)}}">{{$item->item_name_en}}</a></h4>
                                            <p><span>${!! $item->item_price ? number_format($item->item_price->unit_price,2) : '0.00' !!}</span></p>
                                            
                                            <div class="pro-actions">
                                                <div class="actions-primary">
                                                    <a href="javascript:void()" data-toggle="tooltip" title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                </div>
                                                <div class="actions-secondary">
                                                    <a href="javascript:void(0)" onclick="addwishlist({{$item->item_id}});" data-toggle="tooltip" title="Add Favourite"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                </div>
                                            </div>
                                            <span class="sticker-new pro-sticker">new</span>
                                        </div>
                                        <!-- Product Content End -->
                                    </div>
                                    <!-- Single Products End -->
                                </div> 

                                <!-- End Double Product -->

                                <div class="double-product">
                            
                            @else
                            
                                <!-- Single Products Start -->
                                <div class="single-product" style="height:183.61px;">
                                        <!-- Product Image Start -->
                                        <div class="pro-img">
                                            @if($item->feature_image)
                                                <a href="{{url('product/detail/'.$item->item_id)}}"><img class="full-img" src="{{asset('storage/photos/'.$item->feature_image->filename)}}" alt="ads-image"></a>
                                            @elseif($item->item_price==false)
                                                <img class="primary-img" src="{{asset('frontend/img/no-image.png')}}" alt="single-product">
                                            @endif
                                            <div class="quick-view">
                                                <a href="{{url('product/detail/'.$item->item_id)}}" data-toggle="modal" data-target="#myModal">Quick View</a>
                                            </div>
                                        </div>
                                        <!-- Product Image End -->
                                        <!-- Product Content Start -->
                                        <div class="pro-content">
                                        <h4><a href="{{url('product/detail/'.$item->item_id)}}">{{$item->item_name_en}}</a></h4>
                                            <p><span>${!! $item->item_price ? number_format($item->item_price->unit_price,2) : '0.00' !!}</span></p>
                                            
                                            <div class="pro-actions">
                                                <div class="actions-primary">
                                                    <a href="javascript:void()" data-toggle="tooltip" title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                </div>
                                                <div class="actions-secondary">
                                                    <a href="javascript:void(0)" onclick="addwishlist({{$item->item_id}});" data-toggle="tooltip" title="Add Favourite"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                </div>
                                            </div>
                                            <span class="sticker-new pro-sticker">new</span>
                                        </div>
                                        <!-- Product Content End -->
                                    </div>
                                    <!-- Single Products End -->
                            
                            @endif

                            @endforeach
                            
                            </div>
                            
                            
                    </div>
                    <!-- Best Seller End -->
                </div>
            </div>
        </div>
        <!-- Best Seller Product End -->

        <br><br>
        <!-- Featured Products Start -->
        <div class="first-featured-products pb-50" style="background:#fbfbfb;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Featured Product List Item Start -->
                        <ul class="product-list mb-30">
                            <li class="active"><a data-toggle="tab" href="#computer">@lang('category.computer')</a></li>
                            <li><a data-toggle="tab" href="#monitor">@lang('category.monitor_display')</a></li>
                            <li><a data-toggle="tab" href="#network">@lang('category.networking')</a></li>
                            <li><a data-toggle="tab" href="#camera">@lang('category.camera')</a></li>
                            <li><a data-toggle="tab" href="#electronic">@lang('category.office_accessory')</a></li>
                        </ul>
                        <!-- Featured Product List Item End -->
                    </div>
                </div>
                <div class="tab-content">
                    <div id="computer" class="tab-pane fade in active" style="background:#fbfbfb;">
                        <div class="row">
                            <?php $i=0?>
                            @foreach ($computer as $item)
                            <?php $i++;?>
                                    <div class="col-sm-3">
                                        <div class="single-product" style="height:349.5px;">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                @if($item->feature_image)
                                                    <a href="{{url('product/detail/'.$item->item_id)}}"><img class="primary-image" src="{{asset('storage/photos/'.$item->feature_image->filename)}}" alt="ads-image"></a>
                                                @else
                                                <a href="javascript:void()">
                                                   <img class="primary-image" src="{{asset('frontend/img/no-image.png')}}" alt="single-product">
                                                @endif
                                                </a>
                                                <div class="quick-view text-center">
                                                <a href="{{url('product/detail/'.$item->item_id)}}">Quick View</a>
                                                </div>
                                            </div>
                                            <!-- Product Image End -->

                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4><a href="{{url('product/detail/'.$item->item_id)}}">{{$item->item_name_en}}</a></h4>
                                                <p><span>${!! $item->item_price ? number_format($item->item_price->unit_price,2) : '0.00' !!}</span></p>
                                                <div class="rating">
                                                    <i class="">&nbsp;</i>
                    
                                                </div>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                    </div>
                                                    <div class="actions-secondary">
                                                        <a href="javascript:void(0)" onclick="addtocart({{$item->item_id}});" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </div>
                                                </div>
                                                <span class="sticker-new pro-sticker">new</span>
                                            </div>
                                            <!-- Product Content End -->
                                        </div>
                                    </div>
                                    <?php if($i%4==0 && $i<8) 
                                        echo "</div><div class='row'>";
                                    ?>
                                @endforeach
                                    <!-- Single Products End -->
                            </div>
                        </div>
                                        <!-- Row End --> 
                        <!-- #computer End -->
                        <div id="monitor" class="tab-pane fade">
                            <div class="row">
                                    <?php $i=0?>
                                @foreach ($monitor as $item)
                                    <?php $i++;?>
                                        <div class="col-md-3 col-sm-6">
                                            <div class="single-product" style="height:349.5px;">
                                                <!-- Product Image Start -->
                                                <div class="pro-img">
                                                    @if($item->feature_image)
                                                        <a href="{{url('product/detail/'.$item->item_id)}}"><img class="full-img" src="{{asset('storage/photos/'.$item->feature_image->filename)}}" alt="ads-image"></a>
                                                    @else
                                                    <a href="javascript:void()">
                                                        <img class="full-img" src="{{asset('frontend/img/no-image.png')}}" alt="single-product">
                                                    @endif
                                                    </a>
                                                    <div class="quick-view text-center">
                                                    <a href="{{url('product/detail/'.$item->item_id)}}">Quick View</a>
                                                    </div>
                                                </div>
                                                <!-- Product Image End -->
    
                                                <!-- Product Content Start -->
                                                <div class="pro-content">
                                                    <h4><a href="{{url('product/detail/'.$item->item_id)}}">{{$item->item_name_en}}</a></h4>
                                                    <p><span>${!! $item->item_price ? number_format($item->item_price->unit_price,2) : '0.00' !!}</span></p>
                                                    <div class="rating">
                                                        <i class="">&nbsp;</i>
                        
                                                    </div>
                                                    <div class="pro-actions">
                                                        <div class="actions-primary">
                                                        </div>
                                                        <div class="actions-secondary">
                                                            <a href="javascript:void(0)" onclick="addtocart({{$item->item_id}});" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                        </div>
                                                    </div>
                                                    <span class="sticker-new pro-sticker">new</span>
                                                </div>
                                                <!-- Product Content End -->
                                            </div>
                                        </div>
                                        <?php if($i%4==0 && $i<8) 
                                            echo "</div><div class='row'>";
                                        ?>
                                    @endforeach
                                                <!-- Single Products End -->
                            </div>
                        </div>
                            <!-- #monitor End -->
                        <div id="network" class="tab-pane fade">
                                <div class="row">
                                    <?php $i=0?>
                                    @foreach ($networking as $item)
                                        <?php $i++;?>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="single-product" style="height:349.5px;">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                @if($item->feature_image)
                                                    <a href="{{url('product/detail/'.$item->item_id)}}"><img class="full-img" src="{{asset('storage/photos/'.$item->feature_image->filename)}}" alt="ads-image"></a>
                                                @else
                                                <a href="javascript:void()">
                                                    <img class="full-img" src="{{asset('frontend/img/no-image.png')}}" alt="single-product">
                                                @endif
                                                </a>
                                                <div class="quick-view text-center">
                                                <a href="{{url('product/detail/'.$item->item_id)}}">Quick View</a>
                                                </div>
                                            </div>
                                            <!-- Product Image End -->

                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4><a href="{{url('product/detail/'.$item->item_id)}}">{{$item->item_name_en}}</a></h4>
                                                <p><span>${!! $item->item_price ? number_format($item->item_price->unit_price,2) : '0.00' !!}</span></p>
                                                <div class="rating">
                                                    <i class="">&nbsp;</i>
                    
                                                </div>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                    </div>
                                                    <div class="actions-secondary">
                                                        <a href="javascript:void(0)" onclick="addtocart({{$item->item_id}});" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </div>
                                                </div>
                                                <span class="sticker-new pro-sticker">new</span>
                                            </div>
                                        <!-- Product Content End -->
                                    </div>
                                </div>
                                <?php if($i%4==0 && $i<8) 
                                            echo "</div><div class='row'>";
                                        ?>
                                @endforeach
                                                <!-- Single Products End -->
                            </div>
                        </div>
                            <!-- #network End -->
                            <div id="camera" class="tab-pane fade">
                                    <div class="row">
                                            <?php $i=0?>
                                    @foreach ($camera as $item)
                                        <?php $i++;?>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="single-product" style="height:349.5px;">
                                                    <!-- Product Image Start -->
                                                    <div class="pro-img">
                                                        @if($item->feature_image)
                                                            <a href="{{url('product/detail/'.$item->item_id)}}"><img class="full-img" src="{{asset('storage/photos/'.$item->feature_image->filename)}}" alt="ads-image"></a>
                                                        @elseif($item->feature_image==false)
                                                        <a href="javascript:void()">
                                                            <img class="full-img" src="{{asset('frontend/img/no-image.png')}}" alt="single-product">
                                                        @endif
                                                        </a>
                                                        <div class="quick-view text-center">
                                                        <a href="{{url('product/detail/'.$item->item_id)}}">Quick View</a>
                                                        </div>
                                                    </div>
                                                    <!-- Product Image End -->
        
                                                    <!-- Product Content Start -->
                                                    <div class="pro-content">
                                                        <h4><a href="{{url('product/detail/'.$item->item_id)}}">{{$item->item_name_en}}</a></h4>
                                                        <p><span>${!! $item->item_price ? number_format($item->item_price->unit_price,2) : '0.00' !!}</span></p>
                                                        <div class="rating">
                                                            <i class="">&nbsp;</i>
                            
                                                        </div>
                                                        <div class="pro-actions">
                                                            <div class="actions-primary">
                                                            </div>
                                                            <div class="actions-secondary">
                                                                <a href="javascript:void(0)" onclick="addtocart({{$item->item_id}});" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                            </div>
                                                        </div>
                                                        <span class="sticker-new pro-sticker">new</span>
                                                    </div>
                                                    <!-- Product Content End -->
                                                </div>
                                            </div>
                                            <?php if($i%4==0 && $i<8) 
                                                echo "</div><div class='row'>";
                                            ?>
                                        @endforeach
                                                    <!-- Single Products End -->
                                </div>
                            </div>
                            <!-- #camera End -->
                            <div id="electronic" class="tab-pane fade">

                                    <div class="row">
                                            <?php $i=0?>
                                            @foreach ($electronics as $item)
                                                <?php $i++;?>
                                                    <div class="col-md-3 col-sm-6">
                                                        <div class="single-product" style="height:349.5px;">
                                                            <!-- Product Image Start -->
                                                            <div class="pro-img">
                                                                @if($item->feature_image)
                                                                    <a href="{{url('product/detail/'.$item->item_id)}}"><img class="full-img" src="{{asset('storage/photos/'.$item->feature_image->filename)}}" alt="ads-image"></a>
                                                                @elseif($item->feature_image==false)
                                                                <a href="javascript:void()">
                                                                    <img class="full-img" src="{{asset('frontend/img/no-image.png')}}" alt="single-product">
                                                                @endif
                                                                </a>
                                                                <div class="quick-view text-center">
                                                                <a href="{{url('product/detail/'.$item->item_id)}}">Quick View</a>
                                                                </div>
                                                            </div>
                                                            <!-- Product Image End -->
                
                                                            <!-- Product Content Start -->
                                                            <div class="pro-content">
                                                                <h4><a href="{{url('product/detail/'.$item->item_id)}}">{{$item->item_name_en}}</a></h4>
                                                                <p><span>${!! $item->item_price ? number_format($item->item_price->unit_price,2) : '0.00' !!}</span></p>
                                                                <div class="rating">
                                                                    <i class="">&nbsp;</i>
                                    
                                                                </div>
                                                                <div class="pro-actions">
                                                                    <div class="actions-primary">
                                                                    </div>
                                                                    <div class="actions-secondary">
                                                                        <a href="javascript:void(0)" onclick="addtocart({{$item->item_id}});" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                                    </div>
                                                                </div>
                                                                <span class="sticker-new pro-sticker">new</span>
                                                            </div>
                                                            <!-- Product Content End -->
                                                        </div>
                                                    </div>
                                                    <?php if($i%4==0 && $i<8) 
                                                        echo "</div><div class='row'>";
                                                    ?>
                                                @endforeach
                                                            <!-- Single Products End -->
                                        </div>
                            </div>
                                   
                            <!-- #electronic End -->
                        </div>
                        <!-- Tab-Content End -->
                    </div>
            </div>
            <!-- Container End -->
        </div>
        <!-- Featured Products End -->
        <!-- Ads Banner Start -->
	

@endsection   

@section('footertop')

    @include('frontend.layouts.footertop')

@endsection

@section('loginmodal')

    @include('frontend.layouts.loginmodal')

@endsection


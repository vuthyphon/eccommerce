
@extends('frontend/layouts.master')

@section('title', 'Home | Welcome to Honest Technic')
@section('active-home', 'nav-active')

@section ('home')
@endsection

@section ('content')

@include('frontend.layouts.slider')

        <!-- New Products Start -->
        <div class="new-single-products pb-50" style="background:#fff;">
                <br><br>
            <div class="container">
                <!-- Group Title Start -->
                <div class="group-title">
                    <h2>new products</h2>
                </div>
                <!-- Group Title End -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- New Products Activaion Start -->
                        <div class="new-products second-featured-pro owl-carousel">
                            
                            @foreach ($newproduct as $item)

                            <!-- Single Products Start -->
                            <div class="single-product">
                                    <!-- Product Image Start -->
                                    <div class="pro-img">
                                        <a href="#">
                                            @if($item->feature_image)
                                                <img class="primary-img" src="{{asset('storage/photos/'.$item->feature_image->filename)}}" alt="single-product">
                                            @elseif($item->item_price==false)
                                               <img class="primary-img" src="{{asset('frontend/img/no-image.png')}}" alt="single-product">
                                            @endif
                                            
                                            
                                            
                                           
                                        </a>
                                        <div class="quick-view">
                                        <a href="{{url('product/id/')}}" alt="">Quick View</a>
    
                                        </div>
                                    </div>
                                    <!-- Product Image End -->
                                    <!-- Product Content Start -->
                                    <div class="pro-content">
                                        <h4><a href="#">{{$item->item_name_en}}</a></h4>
                                       
                                            <p><span>${!! $item->item_price? number_format($item->item_price->unit_price,2) : 0.00 !!}</span></p>
                                      
                                        
                                        <div class="pro-actions">
                                            <div class="actions-primary">
                                            
                                            </div>
                                            <div class="actions-secondary">
                                                <a href="javascript:void(0)" onclick="addtocart({{$item->item_id}});" data-toggle="tooltip" title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            </div>
                                        </div>

                                        <?php
                                            
                                            if($item->tag!='')
                                                echo "<span class='sticker-new pro-sticker'>$item->tag</span>";                                        ?>
                                        
                                    </div>
                                    <!-- Product Content End -->
                                </div>
                                <!-- Single Products End -->
                            @endforeach
                            
                        </div>
                        <!-- New Products Carousel Activaion End -->
                    </div>
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>
        <!-- New Products End -->
        
        <!-- Best Seller Product Start -->
        <div class="best-seller-product pb-50" style="background:#fff;">
            <div class="container">
                <div class="row">
			
                    <!-- Hot Deal Start -->
                    <div class="col-md-4">
                        <div class="sidebar-ads mtb-40 zoom">
                            @if($item->feature_image)
                                <a href="#"><img class="full-img" src="{{asset('storage/photos/'.$item->feature_image->filename)}}" alt="ads-image"></a>
                            @elseif($item->item_price==false)
                                <img class="primary-img" src="{{asset('frontend/img/no-image.png')}}" alt="single-product">
                            @endif
                        </div>
                    </div>
                    <!-- Hot Deal End -->
					
                    <!-- Best Seller Start -->
                    <div class="col-md-8">
                        <!-- Group Title Start -->
                        <div class="group-title mts">
                            <h2>Best Seller Products</h2>
                        </div>
                        <!-- Group Title End -->
                        <div class="best-seller owl-carousel">

                            <div class="double-product">

                            <?php $i=0;?>

                            @foreach ($bestseller as $item)
                                
                            <?php $i+=1;?>
                            @if($i%2==0)
                            
                                <!-- Single Products Start -->
                                <div class="single-product">
                                        <!-- Product Image Start -->
                                        <div class="pro-img">
                                            <a href="#">
                                                <img class="primary-img" src="{{ asset('frontend/img/best-seller/4_1.jpg')}}" alt="single-product">
                                                <img class="secondary-img" src="{{ asset('frontend/img/best-seller/4_2.jpg')}}" alt="single-product">
                                            </a>
                                            <div class="quick-view">
                                                <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                            </div>
                                        </div>
                                        <!-- Product Image End -->
                                        <!-- Product Content Start -->
                                        <div class="pro-content">
                                            <h4><a href="#">Go-Get'r Pushup Grips</a></h4>
                                            <p><span>$19.00</span></p>
                                            
                                            <div class="pro-actions">
                                                <div class="actions-primary">
                                                    
                                                </div>
                                                <div class="actions-secondary">
                                                    <a href="javascript:void(0)" onclick="addtocart({{$item->item_id}});" data-toggle="tooltip" title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                </div>
                                            </div>
                                            <span class="sticker-new pro-sticker">new</span>
                                        </div>
                                        <!-- Product Content End -->
                                    </div>
                                    <!-- Single Products End -->
                                </div> 

                                <!-- End Double Product -->

                                <div class="double-product">
                            
                            @else
                            
                                <!-- Single Products Start -->
                                <div class="single-product">
                                        <!-- Product Image Start -->
                                        <div class="pro-img">
                                            <a href="#">
                                                <img class="primary-img" src="{{ asset('frontend/img/best-seller/4_1.jpg')}}" alt="single-product">
                                                <img class="secondary-img" src="{{ asset('frontend/img/best-seller/4_2.jpg')}}" alt="single-product">
                                            </a>
                                            <div class="quick-view">
                                                <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                            </div>
                                        </div>
                                        <!-- Product Image End -->
                                        <!-- Product Content Start -->
                                        <div class="pro-content">
                                            <h4><a href="#">Go-Get'r Pushup Grips</a></h4>
                                            <p><span>$19.00</span></p>
                                            
                                            <div class="pro-actions">
                                                <div class="actions-primary">
                                                </div>
                                                <div class="actions-secondary">
                                                    <a href="javascript:void(0)" onclick="addtocart({{$item->item_id}});" data-toggle="tooltip" title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                </div>
                                            </div>
                                            <span class="sticker-new pro-sticker">new</span>
                                        </div>
                                        <!-- Product Content End -->
                                    </div>
                                    <!-- Single Products End -->
                            
                            @endif

                            @endforeach
                            
                            </div>
                            
                            
                    </div>
                    <!-- Best Seller End -->
                </div>
            </div>
        </div>
        <!-- Best Seller Product End -->


        <!-- Featured Products Start -->
        <div class="first-featured-products pb-50" style="background:#fff;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Featured Product List Item Start -->
                        <ul class="product-list mb-30">
                            <li class="active"><a data-toggle="tab" href="#computer">Computer</a></li>
                            <li><a data-toggle="tab" href="#monitor">Monitor</a></li>
                            <li><a data-toggle="tab" href="#network">Networking</a></li>
                            <li><a data-toggle="tab" href="#camera">Camera</a></li>
                            <li><a data-toggle="tab" href="#electronic">Office Electronics</a></li>
                        </ul>
                        <!-- Featured Product List Item End -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="tab-content">
                            <div id="computer" class="tab-pane fade in active">
                                <div class="first-featured-pro recent-post-active owl-carousel">
                                        
                                        <div class="row">

                                            @foreach ($computer as $com)
                                                
                                            <!-- Single Products Start -->
                                            
                                            <div class="col-md-3 col-sm-6">
                                                <div class="single-product">
                                                    <!-- Product Image Start -->
                                                    <div class="pro-img">
                                                        <a href="#">
                                                            <img class="primary-img" src="{{ asset('frontend/img/best-seller/4_1.jpg')}}" alt="single-product">
                                                            <img class="secondary-img" src="{{ asset('frontend/img/best-seller/4_2.jpg')}}" alt="single-product">
                                                        </a>
                                                        <div class="quick-view text-center">
                                                            <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                        </div>
                                                    </div>
                                                    <!-- Product Image End -->

                                                    <!-- Product Content Start -->
                                                    <div class="pro-content">
                                                        <h4><a href="#">Go-Get'r Pushup Grips</a></h4>
                                                        <p><span>$19.00</span></p>
                                                        <div class="rating">
                                                            <i class="">&nbsp;</i>
                            
                                                        </div>
                                                        <div class="pro-actions">
                                                            <div class="actions-primary">
                                                            </div>
                                                            <div class="actions-secondary">
                                                                <a href="javascript:void(0)" onclick="addtocart({{$com->item_id}});" data-toggle="tooltip" title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                            </div>
                                                        </div>
                                                        <span class="sticker-new pro-sticker">new</span>
                                                    </div>
                                                    <!-- Product Content End -->
                                                </div>
                                            </div>
                                            <!-- Single Products End -->

                                            @endforeach

                                        </div>
                                        <!-- Row End -->
                                        
                                    </div>
                                </div>
                            <!-- #computer End -->
                            <div id="monitor" class="tab-pane fade">
                                <div class="first-featured-pro recent-post-active owl-carousel">
                                    <div class="row">

                                            @foreach ($monitor as $com)
                                                
                                            <!-- Single Products Start -->
                                            
                                            <div class="col-md-3 col-sm-6">
                                                <div class="single-product">
                                                    <!-- Product Image Start -->
                                                    <div class="pro-img">
                                                        <a href="#">
                                                            <img class="primary-img" src="{{ asset('frontend/img/best-seller/4_1.jpg')}}" alt="single-product">
                                                            <img class="secondary-img" src="{{ asset('frontend/img/best-seller/4_2.jpg')}}" alt="single-product">
                                                        </a>
                                                        <div class="quick-view text-center">
                                                            <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                        </div>
                                                    </div>
                                                    <!-- Product Image End -->

                                                    <!-- Product Content Start -->
                                                    <div class="pro-content">
                                                        <h4><a href="#">Go-Get'r Pushup Grips</a></h4>
                                                        <p><span>$19.00</span></p>
                                                        <div class="rating">
                                                            <i class="">&nbsp;</i>
                            
                                                        </div>
                                                        <div class="pro-actions">
                                                            <div class="actions-primary">
                                                            </div>
                                                            <div class="actions-secondary">
                                                                <a href="javascript:void(0)" onclick="addtocart({{$com->item_id}});" data-toggle="tooltip" title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                            </div>
                                                        </div>
                                                        <span class="sticker-new pro-sticker">new</span>
                                                    </div>
                                                    <!-- Product Content End -->
                                                </div>
                                            </div>
                                            <!-- Single Products End -->

                                            @endforeach

                                        </div>
                                        <!-- Row End -->
                                </div>
                                <!-- monitor Active End -->
                            </div>
                            <!-- #monitor End -->
                            <div id="network" class="tab-pane fade">
                                <div class="first-featured-pro recent-post-active owl-carousel">
                                        <div class="row">

                                                @foreach ($networking as $com)
                                                    
                                                <!-- Single Products Start -->
                                                
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="single-product">
                                                        <!-- Product Image Start -->
                                                        <div class="pro-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="{{ asset('frontend/img/best-seller/4_1.jpg')}}" alt="single-product">
                                                                <img class="secondary-img" src="{{ asset('frontend/img/best-seller/4_2.jpg')}}" alt="single-product">
                                                            </a>
                                                            <div class="quick-view text-center">
                                                                <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                            </div>
                                                        </div>
                                                        <!-- Product Image End -->
    
                                                        <!-- Product Content Start -->
                                                        <div class="pro-content">
                                                            <h4><a href="#">Go-Get'r Pushup Grips</a></h4>
                                                            <p><span>$19.00</span></p>
                                                            <div class="rating">
                                                                <i class="">&nbsp;</i>
                                
                                                            </div>
                                                            <div class="pro-actions">
                                                                <div class="actions-primary">
                                                                </div>
                                                                <div class="actions-secondary">
                                                                    <a href="javascript:void(0)" onclick="addtocart({{$com->item_id}});" data-toggle="tooltip" title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                                </div>
                                                            </div>
                                                            <span class="sticker-new pro-sticker">new</span>
                                                        </div>
                                                        <!-- Product Content End -->
                                                    </div>
                                                </div>
                                                <!-- Single Products End -->
    
                                                @endforeach
    
                                            </div>
                                            <!-- Row End -->
                                </div>
                                <!-- network-active -->
                            </div>
                            <!-- #network End -->
                            <div id="camera" class="tab-pane fade">
                                <div class="first-featured-pro recent-post-active owl-carousel">
                                        <div class="row">

                                                @foreach ($camera as $com)
                                                    
                                                <!-- Single Products Start -->
                                                
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="single-product">
                                                        <!-- Product Image Start -->
                                                        <div class="pro-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="{{ asset('frontend/img/best-seller/4_1.jpg')}}" alt="single-product">
                                                                <img class="secondary-img" src="{{ asset('frontend/img/best-seller/4_2.jpg')}}" alt="single-product">
                                                            </a>
                                                            <div class="quick-view text-center">
                                                                <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                            </div>
                                                        </div>
                                                        <!-- Product Image End -->
    
                                                        <!-- Product Content Start -->
                                                        <div class="pro-content">
                                                            <h4><a href="#">Go-Get'r Pushup Grips</a></h4>
                                                            <p><span>$19.00</span></p>
                                                            <div class="rating">
                                                                <i class="">&nbsp;</i>
                                
                                                            </div>
                                                            <div class="pro-actions">
                                                                <div class="actions-primary">
                                                                </div>
                                                                <div class="actions-secondary">
                                                                    <a href="javascript:void(0)" onclick="addtocart({{$com->item_id}});" data-toggle="tooltip" title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                                </div>
                                                            </div>
                                                            <span class="sticker-new pro-sticker">new</span>
                                                        </div>
                                                        <!-- Product Content End -->
                                                    </div>
                                                </div>
                                                <!-- Single Products End -->
    
                                                @endforeach
    
                                            </div>
                                            <!-- Row End -->
                                </div>
                                <!-- camera Active End -->
                            </div>
                            <!-- #camera End -->
                            <div id="electronic" class="tab-pane fade">
                                <div class="first-featured-pro recent-post-active owl-carousel">
                                        <div class="row">

                                                @foreach ($electronics as $com)
                                                    
                                                <!-- Single Products Start -->
                                                
                                                <div class="col-md-3 col-sm-6">
                                                    <div class="single-product">
                                                        <!-- Product Image Start -->
                                                        <div class="pro-img">
                                                            <a href="#">
                                                                <img class="primary-img" src="{{ asset('frontend/img/best-seller/4_1.jpg')}}" alt="single-product">
                                                                <img class="secondary-img" src="{{ asset('frontend/img/best-seller/4_2.jpg')}}" alt="single-product">
                                                            </a>
                                                            <div class="quick-view text-center">
                                                                <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                            </div>
                                                        </div>
                                                        <!-- Product Image End -->
    
                                                        <!-- Product Content Start -->
                                                        <div class="pro-content">
                                                            <h4><a href="#">Go-Get'r Pushup Grips</a></h4>
                                                            <p><span>$19.00</span></p>
                                                            <div class="rating">
                                                                <i class="">&nbsp;</i>
                                
                                                            </div>
                                                            <div class="pro-actions">
                                                                <div class="actions-primary">
                                                                </div>
                                                                <div class="actions-secondary">
                                                                    <a href="javascript:void(0)" onclick="addtocart({{$com->item_id}});" data-toggle="tooltip" title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                                </div>
                                                            </div>
                                                            <span class="sticker-new pro-sticker">new</span>
                                                        </div>
                                                        <!-- Product Content End -->
                                                    </div>
                                                </div>
                                                <!-- Single Products End -->
    
                                                @endforeach
    
                                            </div>
                                            <!-- Row End -->
                                </div>
                                <!-- electronic Active End -->
                            </div>
                            <!-- #electronic End -->
                        </div>
                        <!-- Tab-Content End -->
                    </div>
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>
        <!-- Featured Products End -->
        <!-- Ads Banner Start -->
        <div class="ads-banner pb-30">
            <div class="container">
                <div class="row">
                    <!-- Single Ads Start -->
                    <div class="col-sm-4">
                        <div class="single-ads mb-10 zoom">
                            <a href="#"><img class="img" src="{{ asset('frontend/img/ads/1.jpg')}}" alt="ads-banner"></a>
                        </div>
                        <div class="single-ads mb-10 zoom">
                            <a href="#"><img class="img" src="{{ asset('frontend/img/ads/2.jpg')}}" alt="ads-banner"></a>
                        </div>
                    </div>
                    <!-- Single Ads End -->
                    <!-- Single Ads Start -->
                    <div class="col-sm-8">
                        <div class="single-ads mb-10 zoom">
                            <a href="#"><img class="img" src="{{ asset('frontend/img/ads/3.jpg')}}" alt="ads-banner"></a>
                        </div>
                    </div>
                    <!-- Single Ads End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>
        <!-- Ads Banner End -->
		
	

@endsection   

<!-- call footer -->

@section('footertop')

    @include('frontend.layouts.footertop')

@endsection

<!-- call login modal -->

@section('loginmodal')

    @include('frontend.layouts.loginmodal')

@endsection


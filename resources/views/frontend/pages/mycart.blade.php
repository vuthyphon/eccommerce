@extends('frontend/layouts.master')

@section('title', 'Wishlist | Welcome to Honest Technic')

@section('functionalscript')

<script>
        function removecart(id)
        {
            var cmd=confirm('Warning! \nAre you sure you want remove this item from your cart?');
            if(cmd==true)
            {
                $.ajax({
                    type: "get",
                    url: "{{ url('removecart')}}/"+id,
                    success: function(msg) {    
                        $('#cart_'+id).remove(); 
                        $('#tbmycart').html(msg);
                        $('#toast').html('<div class="alert alert-info" style="border-radius:0;"><a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Product has been removed from your cart.</strong>.</div>');
                        $('#toast').show(1000);
                    }
                });
            }
            
        }
    
        function updatecart(id, amt)
        {   
            $.ajax({
                type: "get",
                url: "{{ url('updatecart')}}/"+id+"/"+amt,
                success: function(msg) { 
                    $('#tbmycart').html(msg);
                    $('#toast').html('<div class="alert alert-info" style="border-radius:0;"><a href="javascript:void(0)" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Your cart has been updated!.</strong>.</div>');
                    $('#toast').show(1000);
                }
            });  
        }
    
    </script>

@endsection

@section('active-wishlist', 'nav-active')

@section ('content')

<!-- Header Breadcumb Start -->
<div class="header-bradcrubm pb-20" style="background:#fbfbfb;">
    <div class="container">
        <div class="row">
            <!-- Product Categorie List Start -->
            <div class="col-md-12">
                <div class="main-categorie">
                    <!-- Breadcrumb Start -->
                    <div class="main-breadcrumb">
                        <ul class="ptb-15 breadcrumb-list">
                            <li><a href="{{route('home')}}">home</a></li>
                            <li class="active"><a href="javascript:void(0)">cart</a></li>
                        </ul>
                    </div>
                    <!-- Breadcrumb End -->
                </div>
            </div>
            <!-- product Categorie List End -->
        </div>
        <!-- Row End -->
    </div>
</div>
<!-- Header Breadcumb End -->
<!-- cart-main-area & wish list start -->
<div class="cart-main-area pb-50" style="background:#fbfbfb;">
    <div class="container">
       <!-- Section Title Start -->
        <div class="section-title mb-20">
            <h2>My shopping cart</h2>
        </div>
        <!-- Section Title Start End -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <!-- Form Start -->
                <form onsubmit="return false;">
                    <!-- Table Content Start -->
                    <div class="table-content table-responsive mb-50">
                        <table>
                            <thead>
                                <tr>
                                    <th class="product-thumbnail">Image</th>
                                    <th class="product-name">Product</th>
                                    <th class="product-price">Price</th>
                                    <th class="product-quantity">Quantity</th>
                                    <th class="product-subtotal">Total</th>
                                    <th class="product-remove">Remove</th>
                                </tr>
                            </thead>
                            <tbody id="tbmycart">
                                
                                @isset($mycart)
                                    <?php 

                                        $totalItem=0;
                                        $totalPrice = 0;
                                    
                                    ?>
                                    @foreach ($mycart as $item)
                                        <?php 
                                            $totalItem+=$item->quantity;
                                            $totalPrice +=$item->total;
                                        ?>    
                                        <tr id="cart_{{$item->cart_id}}">
                                            <td class="product-thumbnail">
                                                @if($item->filename)
                                                    <img class="primary-img" src="{{asset('storage/photos/'.$item->filename)}}" alt="single-product">
                                                @else
                                                    <img class="primary-img" src="{{asset('frontend/img/no-image.png')}}" alt="single-product">
                                                @endif
                                            </td>
                                            <td class="product-name"><a href="javascript:void(0)">{{$item->item_name_en}}</a></td>
                                            <td class="product-price"><span class="amount">{{$item->unit_price}}</span></td>
                                            <td class="product-quantity"><input type="number" min="1" max="100" value="{{$item->quantity}}" onblur="updatecart({{$item->cart_id}}, this.value)"></td>
                                            <td class="product-subtotal">{{$item->total}}</td>
                                            <td class="product-remove"> <a onclick="removecart({{$item->cart_id}})" href="javascript:void(0)"><i class="zmdi zmdi-close"></i></a></td>
                                        </tr>

                                    @endforeach

                                        <tr style="font-weight:bold;">
                                            <td class="product-quantity" colspan="3">Total:</td>
                                            <td class="product-quantity">{{ number_format($totalItem)}}</td>
                                            <td class="product-subtotal">$ {{number_format($totalPrice,2)}}</td>
                                            <td class="product-remove"></td>
                                        </tr>

                                @endisset

                            </tbody>
                        </table>
                    </div>
                    <!-- Table Content Start -->
                    <div class="row">
                       <!-- Cart Button Start -->
                        <div class="col-md-8 col-sm-7 col-xs-12">
                            <div class="buttons-cart">
                                <a class="wc-proceed-to-checkout" href="{{ route('checkout')}}">Proceed to Checkout</a>
                                <a href="{{ route('home')}}">Continue Shopping</a>
                            </div>
                            
    
                            
                        </div>
                        <!-- Cart Button Start -->
                    
                    </div>
                    <!-- Row End -->
                </form>
                <!-- Form End -->
            </div>
        </div>
         <!-- Row End -->
    </div>
</div>
<!-- cart-main-area & wish list end -->

@endsection


@section('footertop')

    @include('frontend.layouts.footertop')

@endsection

    


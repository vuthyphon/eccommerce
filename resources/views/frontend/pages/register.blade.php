
@extends('frontend/layouts.master')

@section('title', 'Register | Welcome to Honest Technic')
@section('active-register', 'nav-active')

@section ('content')

<div class="sign-up ptb-50" style="background:#fbfbfb;">
    <div class="container">
        <!-- Section Title Start -->
        <div class="section-title mb-50">

            {!! session('message') !!}

         

    

            <h2>@lang('register.create_new_account')</h2>
        </div>
        <!-- Section Title Start End -->
        <div class="row">
            <!-- Create Account Start -->
            

            <div class="col-sm-12">

                <form method="post" action="{{ url('register/add')}}" autocomplete="off">
                    @csrf
                    <div class="create-account riview-field">
                        <!-- Personal Information Start -->
                        <div class="personal-info fix">
                            <h4 class="mb-30">@lang('register.personal_info')</h4>
                            <div class="form-group">
                                <label class="req" for="f-name">@lang('register.first_name')</label>
                                <input type="text" name="first_name" class="form-control" id="f-name" required="required">
                            </div>
                            <div class="form-group">
                                <label class="req" for="l-name">@lang('register.last_name')</label>
                                <input type="text" name="last_name" class="form-control" id="l-name" required="required">
                            </div>
                            
                            <div class="form-group">
                                <label class="req" for="l-name">@lang('register.tel')</label>
                                <input type="text" name="tel" class="form-control" id="tel" required="required">
                            </div>
                            
                            <div class="form-group">
                                <label class="req" for="l-name">@lang('register.address')</label>
                                <input type="text" name="address" class="form-control" id="address" required="required">
                            </div>
                            
                        </div>
                        <!-- Personal Information End -->
                        <!-- Sign-in Information Start -->
                        <div class="sign-in">
                            <h4 class="mb-30">@lang('register.signin_info')</h4>
                            <div class="form-group">
                                <label class="req" for="email">@lang('register.email')</label>
                                <input type="email" name="e_mail" class="form-control" id="email" data-validation="[EMAIL]" required="required">
                            </div>
                            <div class="form-group">
                                <label class="req" for="password">@lang('register.password')</label>
                                <input type="password" name="uspassword" class="form-control" id="password" required="required">
                            </div>
                            <div class="form-group">
                                <label class="req" for="sure-password">@lang('register.confirm_password')</label>
                                <input type="password" name="reuspassword" class="form-control" id="sure-password" required="required">
                            </div>
                            <button class="mt-10" type="submit">@lang('register.create_account')</button>
                        </div>
                        <!-- Sign-in Information End -->
                    </div>
                </form>
            </div>
            <!-- Create Account End -->
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>

@endsection


@section('footertop')

    @include('frontend.layouts.footertop')

@endsection

@extends('frontend/layouts.master')

@section('title', 'Product | Welcome to Honest Technic')
@section('active-product', 'nav-active')


@section ('content')


<div class="accessories ptb-50" style="background:#fbfbfb;">
        <div class="container">
            <!-- Group Title Start -->
            <div class="section-title mb-50">
                <h2>Shop</h2>
            </div>
            <!-- Group Title End -->
            <div class="row">
                <!-- Product Categorie List Start -->
                <div class="col-lg-9 col-lg-push-3 col-md-9 col-md-push-3 col-sm-8 col-sm-push-4">
                    <div class="main-categorie">
                        <!-- Breadcrumb Start -->
                        <div class="main-breadcrumb mb-30">
                            <ul class="ptb-15 breadcrumb-list">
                                <li><a href="{{route('home')}}">home</a></li>
                            <li class="active"><a href="javascript:void(0)">Shop</a></li>
                            </ul>
                        </div>
                        <!-- Breadcrumb End -->
                  
                        <div class="tab-content">
                            <div id="grid-view" class="tab-pane fade in active">
                                  <div class="row">
                                    <!-- Single Products Start -->
                                    <div class="col-md-4 col-sm-6">
                                        <div class="single-product">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                <a href="product-page.html">
                                                    <img class="primary-img" src="{{ asset('frontend/img/best-seller/6_1.jpg')}}" alt="single-product">
                                                    <img class="secondary-img" src="{{ asset('frontend/img/best-seller/6_2.jpg')}}" alt="single-product">
                                                </a>
                                                <div class="quick-view text-center">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                </div>
                                            </div>
                                            <!-- Product Image End -->
                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4><a href="product-page.html">Sprite Foam Yoga Brick</a></h4>
                                                <p><span>$32.00</span><del>$5.00</del></p>
                                                <div class="rating">
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                </div>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </div>
                                                    <div class="actions-secondary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add Favourite"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                         
                                                    </div>
                                                </div>
                                                <span class="sticker-sale pro-sticker">-20%</span>
                                            </div>
                                            <!-- Product Content End -->
                                        </div>
                                    </div>
                                    <!-- Single Products End -->
                                    <!-- Single Products Start -->
                                    <div class="col-md-4 col-sm-6">
                                        <div class="single-product">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                <a href="product-page.html">
                                                    <img class="primary-img" src="{{ asset('frontend/img/accessories/1_1.jpg')}}" alt="single-product">
                                                    <img class="secondary-img" src="{{ asset('frontend/img/accessories/1_2.jpg')}}" alt="single-product">
                                                </a>
                                                <div class="quick-view text-center">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                </div>
                                            </div>
                                            <!-- Product Image End -->
                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4><a href="product-page.html">Joust Duffle Bag</a></h4>
                                                <p><span>$36.00</span></p>
                                                <div class="rating">
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                </div>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </div>
                                                    <div class="actions-secondary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add Favourite"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                         
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Product Content End -->
                                        </div>
                                    </div>
                                    <!-- Single Products End -->
                                    <!-- Single Products Start -->
                                    <div class="col-md-4 col-sm-6">
                                        <div class="single-product">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                <a href="product-page.html">
                                                    <img class="primary-img" src="{{ asset('frontend/img/best-seller/1_1.jpg')}}" alt="single-product">
                                                    <img class="secondary-img" src="{{ asset('frontend/img/best-seller/1_2.jpg')}}" alt="single-product">
                                                </a>
                                                <div class="quick-view text-center">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                </div>
                                            </div>
                                            <!-- Product Image End -->
                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4><a href="product-page.html">Strive Shoulder Pack</a></h4>
                                                <p><span>$21.00</span></p>
                                                <div class="rating">
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                </div>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </div>
                                                    <div class="actions-secondary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add Favourite"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                         
                                                    </div>
                                                </div>
                                                <span class="sticker-sale pro-sticker">-6%</span>
                                            </div>
                                            <!-- Product Content End -->
                                        </div>
                                    </div>
                                    <!-- Single Products End -->
                                    <!-- Single Products Start -->
                                    <div class="col-md-4 col-sm-6">
                                        <div class="single-product">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                <a href="product-page.html">
                                                    <img class="primary-img" src="{{ asset('frontend/img/best-seller/11_1.jpg')}}" alt="single-product">
                                                    <img class="secondary-img" src="{{ asset('frontend/img/best-seller/11_2.jpg')}}" alt="single-product">
                                                </a>
                                                <div class="quick-view text-center">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                </div>
                                            </div>
                                            <!-- Product Image End -->
                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4><a href="product-page.html">Impulse Duffle</a></h4>
                                                <p><span>$4.00</span><del>$9.00</del></p>
                                                <div class="rating">
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                </div>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </div>
                                                    <div class="actions-secondary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add Favourite"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                         
                                                    </div>
                                                </div>
                                                <span class="sticker-sale pro-sticker">-5%</span>
                                            </div>
                                            <!-- Product Content End -->
                                        </div>
                                    </div>
                                    <!-- Single Products End -->
                                    <!-- Single Products Start -->
                                    <div class="col-md-4 col-sm-6">
                                        <div class="single-product">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                <a href="product-page.html">
                                                    <img class="primary-img" src="{{ asset('frontend/img/accessories/2_1.jpg')}}" alt="single-product">
                                                    <img class="secondary-img" src="{{ asset('frontend/img/accessories/2_2.jpg')}}" alt="single-product">
                                                </a>
                                                <div class="quick-view text-center">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                </div>
                                            </div>
                                            <!-- Product Image End -->
                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4><a href="product-page.html">Driven Backpack</a></h4>
                                                <p><span>$25.00</span><del>$5.00</del></p>
                                                <div class="rating">
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                </div>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </div>
                                                    <div class="actions-secondary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add Favourite"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                         
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Product Content End -->
                                        </div>
                                    </div>
                                    <!-- Single Products End -->
                                    <!-- Single Products Start -->
                                    <div class="col-md-4 col-sm-6">
                                        <div class="single-product">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                <a href="product-page.html">
                                                    <img class="primary-img" src="{{ asset('frontend/img/best-seller/9_1.jpg')}}" alt="single-product">
                                                    <img class="secondary-img" src="{{ asset('frontend/img/best-seller/9_2.jpg')}}" alt="single-product">
                                                </a>
                                                <div class="quick-view text-center">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                </div>
                                            </div>
                                            <!-- Product Image End -->
                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4><a href="product-page.html">Zing Jump Rope</a></h4>
                                                <p><span>$4.00</span></p>
                                                <div class="rating">
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                </div>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </div>
                                                    <div class="actions-secondary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add Favourite"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                         
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Product Content End -->
                                        </div>
                                    </div>
                                    <!-- Single Products End -->
                                    <!-- Single Products Start -->
                                    <div class="col-md-4 col-sm-6">
                                        <div class="single-product">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                <a href="product-page.html">
                                                    <img class="primary-img" src="{{ asset('frontend/img/best-seller/4_1.jpg')}}" alt="single-product">
                                                    <img class="secondary-img" src="{{ asset('frontend/img/best-seller/4_2.jpg')}}" alt="single-product">
                                                </a>
                                                <div class="quick-view text-center">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                </div>
                                            </div>
                                            <!-- Product Image End -->
                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4><a href="product-page.html">Summit</a></h4>
                                                <p><span>$41.00</span><del>$15.00</del></p>
                                                <div class="rating">
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                </div>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </div>
                                                    <div class="actions-secondary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add Favourite"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                         
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Product Content End -->
                                        </div>
                                    </div>
                                    <!-- Single Products End -->
                                    <!-- Single Products Start -->
                                    <div class="col-md-4 col-sm-6">
                                        <div class="single-product">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                <a href="product-page.html">
                                                    <img class="primary-img" src="{{ asset('frontend/img/best-seller/9_1.jpg')}}" alt="single-product">
                                                    <img class="secondary-img" src="{{ asset('frontend/img/best-seller/9_2.jpg')}}" alt="single-product">
                                                </a>
                                                <div class="quick-view text-center">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                </div>
                                            </div>
                                            <!-- Product Image End -->
                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4><a href="product-page.html">Sprite Foam Yoga Brick</a></h4>
                                                <p><span>$4.00</span></p>
                                                <div class="rating">
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                </div>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </div>
                                                    <div class="actions-secondary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add Favourite"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                         
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Product Content End -->
                                        </div>
                                    </div>
                                    <!-- Single Products End -->
                                    <!-- Single Products Start -->
                                    <div class="col-md-4 col-sm-6">
                                        <div class="single-product">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                <a href="product-page.html">
                                                    <img class="primary-img" src="{{ asset('frontend/img/best-seller/6_1.jpg')}}" alt="single-product">
                                                    <img class="secondary-img" src="{{ asset('frontend/img/best-seller/6_2.jpg')}}" alt="single-product">
                                                </a>
                                                <div class="quick-view text-center">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                </div>
                                            </div>
                                            <!-- Product Image End -->
                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4><a href="product-page.html">Rival Field Messenger</a></h4>
                                                <p><span>$34.00</span><del>$5.00</del></p>
                                                <div class="rating">
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                </div>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </div>
                                                    <div class="actions-secondary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add Favourite"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                         
                                                    </div>
                                                </div>
                                                <span class="sticker-new pro-sticker">new</span>
                                            </div>
                                            <!-- Product Content End -->
                                        </div>
                                    </div>
                                    <!-- Single Products End -->
                                    <!-- Single Products Start -->
                                    <div class="col-md-4 col-sm-6">
                                        <div class="single-product">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                <a href="product-page.html">
                                                    <img class="primary-img" src="{{ asset('frontend/img/best-seller/13_1.jpg')}}" alt="single-product">
                                                    <img class="secondary-img" src="{{ asset('frontend/img/best-seller/13_2.jpg')}}" alt="single-product">
                                                </a>
                                                <div class="quick-view text-center">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                </div>
                                            </div>
                                            <!-- Product Image End -->
                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4><a href="product-page.html">Rival Field Messenger</a></h4>
                                                <p><span>$34.00</span><del>$5.00</del></p>
                                                <div class="rating">
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                </div>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </div>
                                                    <div class="actions-secondary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add Favourite"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                         
                                                    </div>
                                                </div>
                                                <span class="sticker-new pro-sticker">new</span>
                                            </div>
                                            <!-- Product Content End -->
                                        </div>
                                    </div>
                                    <!-- Single Products End -->
                                    <!-- Single Products Start -->
                                    <div class="col-md-4 col-sm-6 hidden-sm">
                                        <div class="single-product">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                <a href="product-page.html">
                                                    <img class="primary-img" src="{{ asset('frontend/img/best-seller/5_1.jpg')}}" alt="single-product">
                                                    <img class="secondary-img" src="{{ asset('frontend/img/best-seller/13_2.jpg')}}" alt="single-product">
                                                </a>
                                                <div class="quick-view text-center">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                </div>
                                            </div>
                                            <!-- Product Image End -->
                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4><a href="product-page.html">Rival Field Messenger</a></h4>
                                                <p><span>$34.00</span><del>$5.00</del></p>
                                                <div class="rating">
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                </div>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </div>
                                                    <div class="actions-secondary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add Favourite"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                         
                                                    </div>
                                                </div>
                                                <span class="sticker-new pro-sticker">new</span>
                                            </div>
                                            <!-- Product Content End -->
                                        </div>
                                    </div>
                                    <!-- Single Products End -->
                                    <!-- Single Products Start -->
                                    <div class="col-md-4 col-sm-6 hidden-sm">
                                        <div class="single-product">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                <a href="product-page.html">
                                                    <img class="primary-img" src="{{ asset('frontend/img/best-seller/9_2.jpg')}}" alt="single-product">
                                                    <img class="secondary-img" src="{{ asset('frontend/img/best-seller/13_1.jpg')}}" alt="single-product">
                                                </a>
                                                <div class="quick-view text-center">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">Quick View</a>
                                                </div>
                                            </div>
                                            <!-- Product Image End -->
                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4><a href="product-page.html">Rival Field Messenger</a></h4>
                                                <p><span>$34.00</span><del>$5.00</del></p>
                                                <div class="rating">
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                    <i class="zmdi zmdi-star"></i>
                                                </div>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </div>
                                                    <div class="actions-secondary">
                                                        <a href="#" data-toggle="tooltip" title="" data-original-title="Add Favourite"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                         
                                                    </div>
                                                </div>
                                                <span class="sticker-new pro-sticker">new</span>
                                            </div>
                                            <!-- Product Content End -->
                                        </div>
                                    </div>
                                    <!-- Single Products End -->
                                </div>
                                <!-- Grid-view Row End -->
                            </div>
                            <!-- #grid view End -->
                            
                        </div>
                        <!-- Grid & List Main Area End -->
                    </div>
                    <!-- Toolbar Pagination Start -->
                    <div class="toolbar-pagination mb-20 mt-40">
                        <ul class="text-center toolbar-pagination-list">
                            <li><a href="#"><i class="zmdi zmdi-chevron-left"></i></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#"><i class="zmdi zmdi-chevron-right"></i></a></li>
                        </ul>
                    </div>
                    <!-- Toolbar Pagination End -->
                </div>
                <!-- product Categorie List End -->


                <!-- Sidebar Shopping Option Start -->
                <div class="col-lg-3 col-lg-pull-9 col-md-3 col-md-pull-9 col-sm-4 col-sm-pull-8">
                    
                    
                    <!-- Sidebar Ads Start -->
                    <div class="sidebar-ads mtb-40 zoom">
                        <a href="#"><img class="full-img" src="{{ asset('frontend/img/accessories/4.jpg')}}" alt="ads-image"></a>
                    </div>
                    <!-- Sidebar Ads End -->
                    
                </div>
                <!-- Sidebar Shopping Option End -->
            </div>
            <!-- Row End -->
        </div>
        <!-- Container End -->
    </div>

@endsection


@section('footertop')

    @include('frontend.layouts.footertop')

@endsection
@extends('frontend/layouts.master')

@section('title', 'Register | Welcome to Honest Technic')
@section('active-login', 'nav-active')

@section ('content')

<div class="sign-up main-signin ptb-50" style="background:#fbfbfb;">
    <div class="container">
        <!-- Section Title Start -->
        <div class="section-title mb-50">
            <h2>customer login</h2>
        </div>
        <!-- Section Title Start End -->
        <div class="row">
            <!-- Sign-in Start -->
            <div class="col-sm-6">
                <div class="create-account riview-field">
                    <!-- Sign-in Information Start -->
                    <div class="sign-in">
                        
                        {!! session('message') !!}
                        
                        <h4 class="mb-15 pb-15">@lang('signin.register_customer')</h4>
                        <p class="mb-30">If you have an account, sign in with your email address.</p>
                        <form autocomplete="off" method="post" action="{{ url('clientlogin')}}">
                            @csrf
                             <div class="form-group">
                                <label class="req" for="email2">@lang('register.email')</label>
                                <input type="email" class="form-control" name="e_mail" id="email2" required="required">
                            </div>
                            <div class="form-group">
                                <label class="req" for="password">@lang('register.password')</label>
                                <input type="password" class="form-control" name="password" id="password" minlength="1" required="required">
                            </div>
                            <button type="submit" id="sign-in">@lang('home.signin')</button>
                            <a class="text-capitalize f-right mt-5" href="#">forgot your password?</a>
                        </form>
                    </div>
                    <!-- Sign-in Information End -->
                </div>
            </div>
            <!-- Sign-in End -->
            <!-- New Customer Start -->
            <div class="col-sm-6">
                <div class="new-customer create-account">
                    <h4 class="mb-15 pb-15">@lang('signin.new_customer')</h4>
                    <p class="mb-30">@lang('signin.new_acc_text')</p>
                    <div class="mt-10">
                        <a class="btn-account" href="{{ url('register')}}">@lang('signin.create_acc')</a>
                    </div>
                </div>
            </div>
            <!-- New Customer End -->
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>

@endsection


@section('footertop')

    @include('frontend.layouts.footertop')

@endsection
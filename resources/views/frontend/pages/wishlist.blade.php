@extends('frontend/layouts.master')

@section('title', 'Wishlist | Welcome to Honest Technic')
@section('active-wishlist', 'nav-active')

@section ('content')

<div class="header-bradcrubm pb-50" style="background:#fbfbfb;">
    <div class="container">
        <div class="row">
            <!-- Product Categorie List Start -->
            <div class="col-md-12">
                <div class="main-categorie">
                    <!-- Breadcrumb Start -->
                    <div class="main-breadcrumb">
                        <ul class="ptb-15 breadcrumb-list">
                            <li><a href="#">home</a></li>
                            <li class="active"><a href="#">wish list</a></li>
                        </ul>
                    </div>
                    <!-- Breadcrumb End -->
                </div>
            </div>
            <!-- product Categorie List End -->
        </div>
        <!-- Row End -->
    </div>
</div>

<div class="cart-main-area wish-list pb-50" style="background:#fbfbfb;">
    <div class="container">
       <!-- Section Title Start -->
        <div class="section-title mb-50">
            <h2>wish list</h2>
        </div>
        <!-- Section Title Start End -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <!-- Form Start -->
                <form action="#">
                    <!-- Table Content Start -->
                    <div class="table-content table-responsive">
                        <table>
                            <thead>
                                <tr>
                                    <th class="product-remove">Remove</th>
                                    <th class="product-thumbnail">Image</th>
                                    <th class="product-name">Product</th>
                                    <th class="product-price">Unit Price</th>
                                    <th class="product-quantity">Stock Status</th>
                                    <th class="product-subtotal">add to cart</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="product-remove"> <a href="#"><i class="zmdi zmdi-close"></i></a></td>
                                    <td class="product-thumbnail">
                                        <a href="#"><img src="{{ asset('frontend/img/menu/5.jpg')}}" alt="cart-image"></a>
                                    </td>
                                    <td class="product-name"><a href="#">Vestibulum suscipit</a></td>
                                    <td class="product-price"><span class="amount">£165.00</span></td>
                                    <td class="product-stock-status"><span>in stock</span></td>
                                    <td class="product-add-to-cart"><a href="#">add to cart</a></td>
                                </tr>
                                <tr>
                                    <td class="product-remove"> <a href="#"><i class="zmdi zmdi-close"></i></a></td>
                                    <td class="product-thumbnail">
                                        <a href="#"><img src="{{ asset('frontend/img/menu/6.jpg')}}" alt="cart-image"></a>
                                    </td>
                                    <td class="product-name"><a href="#">Vestibulum dictum magna</a></td>
                                    <td class="product-price"><span class="amount">£50.00</span></td>
                                    <td class="product-stock-status"><span>in stock</span></td>
                                    <td class="product-add-to-cart"><a href="#">add to cart</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- Table Content Start -->
                </form>
                <!-- Form End -->
            </div>
        </div>
         <!-- Row End -->
    </div>
</div>

@endsection


@section('footertop')

    @include('frontend.layouts.footertop')

@endsection
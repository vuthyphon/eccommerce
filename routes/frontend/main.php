<?php

Route::get('home',[ 'as' => 'home','uses' => 'HomeController@index']);
Route::get('product',[ 'as' => 'product','uses' => 'ProductController@show']);
Route::get('product/detail/{id}','ProductController@detail');
Route::get('product/category/{cate}','ProductController@sub_category');
Route::get('product/cate-{cate}','ProductController@category');
Route::get('product/search/{search}','ProductController@search');

Route::get('contact', function () {
    $mycart = array();
    return view('frontend.pages.contact',['mycart'=>$mycart]);
});
Route::get('register',[ 'as' => 'register','uses' => 'RegisterController@create']);
Route::post('register/add',[ 'as' => 'register','uses' => 'RegisterController@store']);
Route::get('signin',[ 'as' => 'signin','uses' => 'LoginController@index']);
Route::get('signout',[ 'as' => 'signout','uses' => 'LoginController@logout']);
Route::get('about',[ 'as' => 'about','uses' => 'AboutController@index']);
Route::get('wishlist',[ 'as' => 'wishlist','uses' => 'WishlistController@index']);
Route::get('checkout',[ 'as' => 'checkout','uses' => 'CheckoutController@index']);


/* Client Sign in */
Route::post('clientlogin',[ 'as' => 'clientlogin','uses' => 'LoginController@login']);
/* Client Sign in by Ajax */
Route::get('loginajax',[ 'as' => 'loginajax','uses' => 'LoginController@loginbyajax']);

/* Add product to cart */
Route::get('addtocart/{id}','WishlistController@store');
/* Remove item cart */
Route::get('removecart/{id}','WishlistController@remove_itemcart');

Route::get('mycart',[ 'as' => 'mycart','uses' => 'WishlistController@index']);

/* Check out */
Route::get('checkout',[ 'as' => 'checkout','uses' => 'WishlistController@checkout']);
/* Add check out */
Route::post('addcheckout',[ 'as' => 'addcheckout','uses' => 'WishlistController@addCheckOut']);

// activate account
Route::get('register/activate/{us_code}','RegisterController@activate_account');



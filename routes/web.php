<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Route::get('locale/{locale}',function($locale){
    Session::put('locale',$locale);
    return redirect()->back();
});

Auth::routes();

Route::group(['namespace' => 'FrontEnd'], function () {
	require(__DIR__.'/frontend/main.php');
});

Route::group(['prefix' => 'cp','namespace'=>'Cpanel','middleware'=>'auth'], function () {
   

    Route::get('users', 'UserController@index');
    Route::get('users/edit/{id}','UserController@edit');
    Route::get('users/new','UserController@new');
    Route::post('users/add','UserController@store');
    Route::post('users/update','UserController@update');
    Route::get('users/disable/{id}','UserController@disable');
    Route::get('roles','RoleController@index');

    Route::get('category','CategoryController@index');
    Route::get('category/{cate_id}/sub_category','CategoryController@show_sub_category');
   

    Route::get('products/new','ProductController@create');
    Route::post('products/add','ProductController@store');
    Route::get('products','ProductController@index');
    Route::get('products/edit/{id}','ProductController@edit');
    Route::get('products/disable/{id}','ProductController@disable');
    Route::post('products/update/{id}','ProductController@update');
    Route::post('products/new_price/{id}','ProductController@new_item_price');
    Route::get('products/all','ProductController@get_all_item');
    Route::get('products/{search}','ProductController@search');

    Route::get('products/detail/disable/{id}','ProductController@disable_detail');
    
    Route::get('/home', 'HomeController@index');
    Route::get('profile/{id}', 'HomeController@profile')->name('profile');
    Route::post('home/update_setting','HomeController@update');
    Route::post('home/update_profile','HomeController@update_profile');
    
    
    Route::get('orderreport','ReportController@index');
    Route::get('orderreport/{frdt}/{todt}','ReportController@searchOrderReport');
    Route::get('download/{frdt}/{todt}', 'ReportController@download');
    
});

/*
Route::group(['middleware'=>'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('profile/{id}', 'HomeController@profile')->name('profile');
    Route::post('home/update_setting','HomeController@update');
    Route::post('home/update_profile','HomeController@update_profile');
});
*/

//Route::get('/home', 'HomeController@index')->name('home');

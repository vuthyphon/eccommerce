<?php $__env->startSection('title', 'Check out | Welcome to Honest Technic'); ?>
<?php $__env->startSection('active-checkout', 'nav-active'); ?>

<?php $__env->startSection('content'); ?>

<div class="header-bradcrubm" style="background:#fbfbfb;">
    <div class="container">
        <div class="row">
            <!-- Product Categorie List Start -->
            <div class="col-md-12">
                <div class="main-categorie">
                    <!-- Breadcrumb Start -->
                    <div class="main-breadcrumb">
                        <ul class="ptb-15 breadcrumb-list">
                        <li><a href="<?php echo e(route('home')); ?>">home</a></li>
                            <li class="active"><a href="javascript:void(0)">checkout</a></li>
                        </ul>
                    </div>
                    <!-- Breadcrumb End -->
                </div>
            </div>
            <!-- product Categorie List End -->
        </div>
        <!-- Row End -->
    </div>
</div>





<div class="checkout-area pt-30" style="background:#fbfbfb;">
    <div class="container">
        <div class="row">
            <form method="post" action="<?php echo e(route('addcheckout')); ?>" onsubmit="return confirm('Are you sure want to submit this order? ')" enctype="multipart/form-data">
                <?php echo csrf_field(); ?>
                <div class="col-lg-8 col-md-8">
                    <div class="your-order">
                    <h3>Your order</h3>
                        <div class="your-order-table table-responsive">
                            <table>
                                <thead>
                                    <tr>
                                        <th class="">No</th>
                                        <th class="product-name">Product</th>
                                        <th class="product-total">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $i=0;
                                        $totalPrice=0;
                                    ?>
                                    <?php if(isset($mycart)): ?>
                                        
                                        <?php $__currentLoopData = $mycart; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php 
                                                $i+=1;
                                                $totalPrice+=$item->total;
                                                ?>
                                            <tr class="cart_item">
                                                <td align="center"><?php echo e($i); ?></td>
                                                <td align="left">
                                                    <?php echo e($item->item_name_en); ?> <strong class="product-quantity"> × <?php echo e($item->quantity); ?></strong>
                                                </td>
                                                <td align="right">
                                                    <span class="amount">$ <?php echo e(number_format($item->total,2)); ?></span>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?> 

                                </tbody>
                                <tfoot>
                                    <tr class="cart-subtotal">
                                        <th colspan="2">Cart Subtotal</th>
                                    <td><span class="amount">$ <?php echo e(number_format($totalPrice,2)); ?></span></td>
                                    </tr>
                                    <tr class="order-total">
                                        <th colspan="2">Order Total</th>
                                        <td><strong><span class="amount">$ <?php echo e(number_format($totalPrice,2)); ?></span></strong>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                        <div class="payment-method">
                            <div class="payment-accordion">
                                <!--   -->
                                
                                
                                    <div class="different-address">
                                    <div class="ship-different-title">
                                        <h3>
                                            <label id="ship-box">Ship to a different address?</label>
                                            
                                        </h3>
                                    </div>
                                    <div id="ship-box-info" class="row">
                                        
                                        <div class="col-md-12">
                                            <div class="checkout-form-list mb-30">
                                                <label>Address <span class="required">*</span></label>
                                                <input type="text" name="difaddress" placeholder="Street address">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="order-notes">
                                        <div class="checkout-form-list">
                                            <label>Order Notes</label>
                                            <textarea name="order_note" id="checkout-mess" cols="30" rows="10" placeholder="Notes about your order, e.g. special notes for delivery."></textarea>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="order-button-payment">
                                    <input type="submit" value="Place order">
                                </div>
                                
                                
                                
                                <!--   -->
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </form>
           
        </div>
    </div>

    <br>
    <br>
</div>



<?php $__env->stopSection(); ?>


<?php $__env->startSection('footertop'); ?>

    <?php echo $__env->make('frontend.layouts.footertop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend/layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /home/honesttechnic/public_html/website/resources/views/frontend/pages/checkout.blade.php */ ?>
<!DOCTYPE html>
<html lang="en">
   <head><meta http-equiv="Content-Type" content="text/html; charset=shift_jis">
      <base >
      
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
      <meta name="description" content="CoreUI - Open Source Bootstrap Admin Template">
      <meta name="author" content="Łukasz Holeczek">
      <meta name="keyword" content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">
      <title>Login</title>
      <link href="<?php echo e(asset('backend/vendors/%40coreui/icons/css/coreui-icons.min.css')); ?>" rel="stylesheet">
      <link href="<?php echo e(asset('backend/vendors/flag-icon-css/css/flag-icon.min.css')); ?>" rel="stylesheet">
      <link href="<?php echo e(asset('backend/vendors/simple-line-icons/css/simple-line-icons.css')); ?>" rel="stylesheet">
      <link href="<?php echo e(asset('backend/css/style.css')); ?>" rel="stylesheet">
      <link href="<?php echo e(asset('backend/vendors/pace-progress/css/pace.min.css')); ?>" rel="stylesheet">
      <link href='https://fonts.googleapis.com/css?family=Nokora' rel='stylesheet' type='text/css'/>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
      <style>
         body{
			font-family : Nokora;
		}
      </style>

   </head>
   <body class="app flex-row align-items-center">
      <div class="container">
         <div class="row justify-content-center">
            
            <div class="col-md-8">
               <div class="card-group">
                  <div class="card p-4">
                     <div class="card-body">
                        <form method="POST" action="<?php echo e(route('login')); ?>">
                            <?php echo csrf_field(); ?>
                        <h1>Please Signin</h1>
                        <p class="text-muted">Sign In to your account</p>
                        <?php echo session('message'); ?>

                        <div class="input-group mb-3">
                           <div class="input-group-prepend">
                              <span class="input-group-text"><i class="icon-user"></i></span>
                           </div>
                          <input class="form-control" name="username" value="<?php echo e(old('username')); ?>" type="text" placeholder="Username">
                        </div>
                        <div class="input-group mb-4">
                           <div class="input-group-prepend">
                             <span class="input-group-text"><i class="icon-lock"></i></span>
                           </div>
                           <input class="form-control" name="password" type="password" placeholder="Password">
                        </div>
                        <div class="row">
                           <div class="col-6">
                              <button class="btn btn-primary px-4" type="submit">Signin</button>
                           </div>
                           <div class="col-6 text-right">
                              <button class="btn btn-link px-0" type="button">Forgot password?</button>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-12">
                               <br>
                               Click <a href="http://honesttechnic.com">Home</a>, if you wanna go to Home page.
                           </div>
                        </div>
                        </form>
                     </div>
                  </div>
                  <div class="card text-white bg-info py-5 d-md-down-none" style="width:44%">
                     <div class="card-body text-center">
                        <div>
                            <a href="http://honesttechnic.com"><img src="http://honesttechnic.com/frontend/img/logo/logo.png"></a>
                            
                        </div>
                        
                     </div>
                  </div>
               </div>
            </div>
            </form>
         </div>
      </div>
      <script src="<?php echo e(asset('backend/vendors/jquery/js/jquery.min.js')); ?>"></script>
      <script src="<?php echo e(asset('backend/vendors/popper.js/js/popper.min.js')); ?>"></script>
      <script src="<?php echo e(asset('backend/vendors/bootstrap/js/bootstrap.min.js')); ?>"></script>
      <script src="<?php echo e(asset('backend/vendors/pace-progress/js/pace.min.js')); ?>"></script>
      <script src="<?php echo e(asset('backend/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js')); ?>"></script>
      <script src="<?php echo e(asset('backend/vendors/%40coreui/coreui-pro/js/coreui.min.js')); ?>"></script>
      
   </body>
</html>


<?php /* /home/honesttechnic/public_html/website/resources/views/backend/login/login.blade.php */ ?>
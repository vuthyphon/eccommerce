<?php /* /home/honesttechnic/public_html/website/resources/views/backend/products/index.blade.php */ ?>
<?php $__env->startSection('title','Honest Technic | Products'); ?>
<?php $__env->startSection('user','li_active'); ?>

<?php $__env->startSection('main-content'); ?>
    <main class="main">
        <ol class="breadcrumb">
           <li class="breadcrumb-item">
              <a href="#"><?php echo e(Auth::user()->username); ?></a>
           </li>
           <li class="breadcrumb-item active">Products</li>
           <li class="breadcrumb-menu d-md-down-none">
              <div class="btn-group" role="group" aria-label="Button group">
                 <a class="btn" href="#">
                 <i class="icon-speech"></i>
                 </a>
                 <a class="btn" href="index.html">
                 <i class="icon-graph"></i> &nbsp;Dashboard</a>
                 <a class="btn" href="#">
                 <i class="icon-settings"></i> &nbsp;Settings</a>
              </div>
           </li>
        </ol>
        <div class="container-fluid">
                <div class="card row">
                        <div class="card-header">
                           <i class="fa fa-edit"></i> Product Information
                           <div class="card-header-actions">
                           <a href="<?php echo e(url('cp/products/new')); ?>" class="btn btn-primary">New</a>
                           </div>
                        </div>
                        <div class="card-body">
                        <?php echo session('message'); ?>

                            
                        <table class="table table-striped table-bordered" id="myTable" width="100%">
                            <thead>
                                <tr role="row">
                                    <th>No</th>
                                    <th>Product Code</th>
                                    <th>Product Name</th>
                                    <th>Brand</th>
                                    <th>UnitPrice($)</th>
                                    <th>Cateogory</th>
                                    <th>Sub Category</th>
                                    <th>Created Date</th>
                                    <th>By</th>
                                    <!--<th>Status</th>-->
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $i=1;
                                ?>
                                <?php if(isset($products)): ?>
                                   <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr role="row">
                                        <td><?php echo e($i++); ?></td>
                                        <td><?php echo e($item->item_code); ?></td>
                                        <td><?php echo e($item->item_name_en); ?></td>
                                        <td><?php echo e($item->brand); ?></td>
                                        <td><?php echo $item->item_price!=null ? number_format($item->item_price->unit_price,2):'0.00'; ?></td>
                                        <td><?php echo e($item->category->cate_name); ?></td>
                                        <td><?php echo e($item->sub_category->sub_cate_name); ?></td>
                                        <td><?php echo e($item->created_at); ?></td>
                                        <td><?php echo e($item->user->username); ?></td>
                                        <!--<td><?php echo $item->d_status==1 ? "<i class='fa fa-check-circle'></i>" : '<i class="fa fa-ban"></i>'; ?></td>-->
                                        <td>
                                            <a class="btn btn-primary btn-sm btn-edit" href="<?php echo e(url('cp/products/edit/'.$item->item_id)); ?>"><i class="fa fa-edit"></i></a>
                                            <a class="btn btn-danger btn-sm" onclick="return confirm('Are you sure to disabled this product?')" href="<?php echo e(url('cp/products/disable/'.$item->item_id)); ?>"><i class="fas fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
</main>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('customJs'); ?>

<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo e(asset('backend/vendors/dataTable/datatables.js')); ?>"></script>

<script>
    $(document).ready( function () {
        $('#myTable').DataTable({
            "autoWidth":false
        });
});
</script>

<?php $__env->stopSection(); ?>


     
     
<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
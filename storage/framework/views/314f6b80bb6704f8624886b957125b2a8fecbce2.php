<?php $__env->startSection('mainCss'); ?>
    <link href="<?php echo e(asset('backend/vendors/%40coreui/icons/css/coreui-icons.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('backend/vendors/flag-icon-css/css/flag-icon.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('backend/vendors/simple-line-icons/css/simple-line-icons.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('backend/css/style.css')); ?>" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link href="<?php echo e(asset('backend/vendors/pace-progress/css/pace.min.css')); ?>" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Nokora' rel='stylesheet' type='text/css'/>
    <style>
            body{
              /* font-family : Nokora;*/
           }
    </style>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('customCss'); ?>
   <style>
      .li_active{
            background:rgba(245, 245, 240,0.1);
      }
   </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('header'); ?>
<header class="app-header navbar">
        <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
           <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="#">
            <p style="font-size: 16px;">HonestTechnic</p>
        </a>
        <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a href="#" style="align:center;">dashboard</a>
        <ul class="nav navbar-nav ml-auto">
        
           <li class="nav-item dropdown">
              <a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    <?php echo e(Auth::user()->username); ?> <img class="img-avatar" src="<?php echo e(asset('backend/img/avatars/6.jpg')); ?>">
              </a>
              <div class="dropdown-menu dropdown-menu-right">
                  
                 <a class="dropdown-item" href="<?php echo e(route('logout')); ?>" onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();"><i class="fa fa-lock"></i> Logout</a>
                 <a class="dropdown-item" href="<?php echo e(url('home/profile/'.Auth::user()->id)); ?>"><i class="fas fa-user"></i>Profile</a>
              </div>
              <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                    <?php echo csrf_field(); ?>
            </form>
           </li>
       
        </ul>
</header>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('left-side'); ?>
    <div class="sidebar">
        <nav class="sidebar-nav">
           <ul class="nav">
              <li class="nav-item <?php echo $__env->yieldContent('dashboard'); ?>">
                  <a class="nav-link" href="<?php echo e(url('cp/home')); ?>"><i class="nav-icon icon-speedometer"></i> Dashboard</a>
              </li>
              <li class="nav-title">Products</li>
              <li class="nav-item <?php echo $__env->yieldContent('post_product'); ?>">
                  <a class="nav-link" href="<?php echo e(url('cp/products/new')); ?>"><i class="nav-icon icon-drop"></i> Post Product</a>
              </li>
              <li class="nav-item <?php echo $__env->yieldContent('product'); ?>">
                  <a class="nav-link" href="<?php echo e(url('cp/products')); ?>"><i class="nav-icon icon-pencil"></i> Products</a>
             </li>
            <!-- <li class="nav-item <?php echo $__env->yieldContent('category'); ?>">
               <a class="nav-link" href="<?php echo e(url('cp/category')); ?>"><i class="nav-icon icon-pencil"></i> Category</a>
             </li>
            
             <li class="nav-item <?php echo $__env->yieldContent('client'); ?>">
               <a class="nav-link" href="<?php echo e(url('cp/cateogry')); ?>"><i class="nav-icon icon-pencil"></i> Client</a>
             </li>
             <li class="nav-item <?php echo $__env->yieldContent('invoices'); ?>">
               <a class="nav-link" href="<?php echo e(url('cp/cateogry')); ?>"><i class="nav-icon icon-pencil"></i> Client Order</a>
             </li>-->
             
              <li class="nav-title">Report</li>
              
              <li class="nav-item <?php echo $__env->yieldContent('shipment_report'); ?>">
               <a class="nav-link" href="<?php echo e(url('cp/orderreport')); ?>"><i class="nav-icon icon-calculator"></i> Order Report</a>
             </li>
              
             <!-- <li class="nav-item <?php echo $__env->yieldContent('monthly_report'); ?>">
                <a class="nav-link" href="<?php echo e(url('cp/report/monthly')); ?>"><i class="nav-icon icon-calculator"></i>Monlthy Report</a>
             </li>
            
             <li class="nav-item <?php echo $__env->yieldContent('shipment_report'); ?>">
               <a class="nav-link" href="<?php echo e(url('cp/report/shipment')); ?>"><i class="nav-icon icon-calculator"></i> Order Report</a>
             </li>
             -->

             <?php if(auth()->guard()->check()): ?>
                <?php if(Auth::user()->role_id==1): ?>
                <li class="nav-title">Users</li>
                <li class="nav-item <?php echo $__env->yieldContent('users'); ?>">
                    <a class="nav-link" href="<?php echo e(url('cp/users')); ?>"><i class="nav-icon icon-list"></i> Users</a>
                </li>
            <?php endif; ?>
               
             <?php endif; ?>

             

           </ul>
        </nav>
     </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('mainJs'); ?>
<script src="<?php echo e(asset('backend/vendors/jquery/js/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('backend/vendors/popper.js/js/popper.min.js')); ?>"></script>
<script src="<?php echo e(asset('backend/vendors/bootstrap/js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('backend/vendors/pace-progress/js/pace.min.js')); ?>"></script>
<script src="<?php echo e(asset('backend/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js')); ?>"></script>
<script src="<?php echo e(asset('backend/vendors/%40coreui/coreui-pro/js/coreui.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /home/honesttechnic/public_html/website/resources/views/backend/layouts/app.blade.php */ ?>
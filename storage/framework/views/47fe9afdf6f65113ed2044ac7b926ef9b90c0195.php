<?php $__env->startSection('title', 'Register | Welcome to Honest Technic'); ?>
<?php $__env->startSection('active-register', 'nav-active'); ?>

<?php $__env->startSection('content'); ?>

<div class="sign-up ptb-50" style="background:#fbfbfb;">
    <div class="container">
        <!-- Section Title Start -->
        <div class="section-title mb-50">

            <?php echo session('message'); ?>


         

    

            <h2>create a new account</h2>
        </div>
        <!-- Section Title Start End -->
        <div class="row">
            <!-- Create Account Start -->
            

            <div class="col-sm-12">

                <form method="post" action="<?php echo e(url('register/add')); ?>" autocomplete="off">
                    <?php echo csrf_field(); ?>
                    <div class="create-account riview-field">
                        <!-- Personal Information Start -->
                        <div class="personal-info fix">
                            <h4 class="mb-30">personal Information</h4>
                            <div class="form-group">
                                <label class="req" for="f-name">First Name</label>
                                <input type="text" name="first_name" class="form-control" id="f-name" required="required">
                            </div>
                            <div class="form-group">
                                <label class="req" for="l-name">Last Name</label>
                                <input type="text" name="last_name" class="form-control" id="l-name" required="required">
                            </div>
                            
                            <div class="form-group">
                                <label class="req" for="l-name">Tel</label>
                                <input type="text" name="tel" class="form-control" id="tel" required="required">
                            </div>
                            
                            <div class="form-group">
                                <label class="req" for="l-name">Address</label>
                                <input type="text" name="address" class="form-control" id="address" required="required">
                            </div>
                            
                        </div>
                        <!-- Personal Information End -->
                        <!-- Sign-in Information Start -->
                        <div class="sign-in">
                            <h4 class="mb-30">Sign-<span class="inherite">in</span> Information</h4>
                            <div class="form-group">
                                <label class="req" for="email">Email</label>
                                <input type="email" name="e_mail" class="form-control" id="email" data-validation="[EMAIL]" required="required">
                            </div>
                            <div class="form-group">
                                <label class="req" for="password">Password</label>
                                <input type="password" name="uspassword" class="form-control" id="password" required="required">
                            </div>
                            <div class="form-group">
                                <label class="req" for="sure-password">Confirm Password</label>
                                <input type="password" name="reuspassword" class="form-control" id="sure-password" required="required">
                            </div>
                            <button class="mt-10" type="submit">create an account</button>
                        </div>
                        <!-- Sign-in Information End -->
                    </div>
                </form>
            </div>
            <!-- Create Account End -->
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('footertop'); ?>

    <?php echo $__env->make('frontend.layouts.footertop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend/layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /home/honesttechnic/public_html/website/resources/views/frontend/pages/register.blade.php */ ?>
<?php $__env->startSection('title', 'Register | Welcome to Honest Technic'); ?>
<?php $__env->startSection('active-about', 'nav-active'); ?>

<?php $__env->startSection('content'); ?>

<div class="sign-up main-signin ptb-50" style="background:#fbfbfb;">
    <div class="container">
        <!-- Section Title Start -->
        <div class="section-title mb-50">
            <h2>About Honest technic</h2>
        </div>
        <!-- Section Title Start End -->
        <div class="row">
            <!-- Sign-in Start -->
            <div class="col-sm-6">
                <div class="create-account riview-field">
                    <!-- Sign-in Information Start -->
                    <div class="sign-in">
                        <h4 class="mb-15 pb-15">Overview</h4>
                        <p class="mb-30">Since its inception in the year 2000, Honest Technic has grown steadily and enhanced its business through inspired innovations. We’re continually transforming into a dynamic, customer-driven, talent-powered company that focuses on enhancing our customers’ enjoyment of technology. Like many companies, we came from humble beginnings. We’ve been challenged significantly from time to time and we’ve learned, changed and grown from each of these challenges.</p>
                    </div>
                    <!-- Sign-in Information End -->
                </div>

                <div class="create-account riview-field">
                    <!-- Sign-in Information Start -->
                    <div class="sign-in">
                        <h4 class="mb-15 pb-15">Our Mission</h4>
                        <p class="mb-30">Honest Technic has been striving to break the barriers of technology reaching people; our long term mission is to educate people to adopt new way of living with technology and computerization.  Everyone can enjoy life with technology and more quality.</p>
                    </div>
                    <!-- Sign-in Information End -->
                </div>

            </div>
            <!-- Sign-in End -->
            <!-- New Customer Start -->
            <div class="col-sm-6">
                <div class="new-customer create-account">
                    <h4 class="mb-15 pb-15">Our Vision</h4>
                    <p class="mb-30">Based on the rapid growth of IT industries in both national and global economies, Honest Technic has set its goal to meet the right need of demanding home users, corporate users and government institutions in order to improve and strengthen the quality of IT sector.                    
                </div>
            </div>
            <!-- New Customer End -->
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>

<?php $__env->stopSection(); ?>



<?php $__env->startSection('footertop'); ?>

    <?php echo $__env->make('frontend.layouts.footertop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend/layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /home/honesttechnic/public_html/website/resources/views/frontend/pages/about.blade.php */ ?>
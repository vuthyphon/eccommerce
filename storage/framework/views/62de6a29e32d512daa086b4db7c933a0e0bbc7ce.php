
<?php $__env->startSection('title','Invoice | Users'); ?>
<?php $__env->startSection('user','li_active'); ?>

<?php $__env->startSection('main-content'); ?>
    <main class="main">
        <ol class="breadcrumb">
           <li class="breadcrumb-item">Home</li>
           <li class="breadcrumb-item">
              <a href="#">Users</a>
           </li>
           <li class="breadcrumb-item active">Edit</li>
        </ol>
        <div class="container-fluid">
                <div class="card row">
                        <div class="card-header">
                           <i class="fa fa-edit"></i> Add Users Information
                           <div class="card-header-actions">
                          
                           </div>
                        </div>
                        <div class="card-body">

                            <?php if($errors->any()): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>    
                              
                        <form action="<?php echo e(url('cp/users/add')); ?>" method="post">
                                <?php echo csrf_field(); ?>
                                
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nf-email">Username</label>
                                    <input class="form-control" value="<?php echo e(old('username')); ?>"  required name="username" placeholder="ឈ្មោះអ្នកប្រើប្រាស់" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-password">Email</label>
                                        <input class="form-control" value="<?php echo e(old('email')); ?>" name="email" placeholder="អ៊ីមែល" type="text" data-validation="[EMAIL]">
                                    </div>
                                    <div class="form-group">
                                            <label for="nf-password">Password</label>
                                            <input class="form-control" value="" name="password" placeholder="មិនបំពេញមានន័យថាមិនប្តូរលេខសំងាត់" type="password">
                                    </div>
                                   
                                    
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                            <label for="nf-email">Role</label>
                                            <select class="form-control" name="role">
                                                <?php $__currentLoopData = $role; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($item->role_id); ?>" ><?php echo e($item->role_name); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="nf-password">Active</label>
                                        <select name="active" class="form-control">
                                            <option value="0">Inactive</option>
                                            <option value="1">Active</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary" style="margin-top:30px">Save</button>
                                    </div>
                                </div>
                            </div>
                            
                           
                        </form>
                           
                        
                    </div>
                </div>
        </div>
</main>
<?php $__env->stopSection(); ?>





     
     
<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /home/honesttechnic/public_html/website/resources/views/backend/users/new.blade.php */ ?>
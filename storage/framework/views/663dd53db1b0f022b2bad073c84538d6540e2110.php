
        
        <div class="modal fade" id="loginModal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Sign in</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -25px;">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" style="padding: 15px 0px;">
                  
                
                    <div class="container">
                        
                            <div class="col-sm-6">
                                <p style="color:red">Sorry!!! You cannot add products to your cart. Please login to get your session.</p>
                                <form autocomplete="off" method="post" action="#" onsubmit="return false();">
                                    <input type="hidden" name="_token" value="cjCKDypl3DRFXaTqNFx1ajtBCLlih7wCYabBEzE0">                             
                                    <div class="form-group">
                                        <label class="req" for="email2">Email</label>
                                        <input type="email" class="form-control" name="e_mail" id="email" required="required">
                                    </div>
                                    <div class="form-group">
                                        <label class="req" for="password">Password</label>
                                        <input type="password" class="form-control" name="password" id="paswd" minlength="3" required="required">
                                    </div>
                                    <a href="javascript:void(0)" class="btn btn-success" onclick="loginAjax()" id="sign-in">Sign in</a>&nbsp;
                                    <a href="<?php echo e(url('register')); ?>" class="btn btn-primary" id="sign-up">Register</a>
                                    <a class="text-capitalize f-right mt-5" href="#">forgot your password?</a>
                                </form>
                                    
                            </div>
            
                    </div>
                    <!-- Container End -->
                
                
              </div>
              <div class="modal-footer" id="loginmsg" style="padding: 15px 15px 0 15px;">
              </div>
            </div>
          </div>
        </div>
<?php /* /home/honesttechnic/public_html/website/resources/views/frontend/layouts/loginmodal.blade.php */ ?>
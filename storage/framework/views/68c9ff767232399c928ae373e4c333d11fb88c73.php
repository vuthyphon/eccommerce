<?php $__env->startSection('title', 'Product | Welcome to Honest Technic'); ?>
<?php $__env->startSection('active-product', 'nav-active'); ?>
<?php $__env->startSection('content'); ?>


<div class="accessories ptb-50" style="background:#fbfbfb;">
        <div class="container">
            <!-- Group Title Start -->
            <div class="section-title">
                <h2>Product</h2>
            </div>
            <!-- Group Title End -->
            <div class="row">
                <!-- Product Categorie List Start -->
                <div class="col-lg-9 col-lg-push-3 col-md-9 col-md-push-3 col-sm-8 col-sm-push-4">
                    <div class="main-categorie">
                        <!-- Breadcrumb Start -->
                        <div class="main-breadcrumb mb-30">
                            <ul class="ptb-15 breadcrumb-list">
                                <li><a href="<?php echo e(route('home')); ?>">home</a></li>
                            <li class="active"><a href="<?php echo e(route('product')); ?>">Product</a></li>
                                <?php if(isset($category)): ?>
                                    <li class="active"><a href="<?php echo e(url('product/cate-'.$cate_id)); ?>"><?php echo e($category); ?></a></li>
                                <?php endif; ?>

                                <?php if(isset($sub_category)): ?>
                                    <li class="active"><a href="javascript:void(0)"><?php echo e($sub_category); ?></a></li>
                                <?php endif; ?>
                                
                            </ul>
                        </div>
                        <!-- Breadcrumb End -->
                  
                        <div class="tab-content">
                            <div id="grid-view" class="tab-pane fade in active">
                                  <div class="row">
                                      <?php $__currentLoopData = $product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="col-md-4 col-sm-6">
                                            <div class="single-product" style="height:349.5px;">
                                                <!-- Product Image Start -->
                                                <div class="pro-img">
                                                    <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>">
                                                        <?php if($item->feature_image): ?>
                                                            <img class="primary-img" src="<?php echo e(asset('storage/photos/'.$item->feature_image->filename)); ?>" alt="single-product">
                                                        <?php elseif($item->feature_image==false): ?>
                                                            <img class="primary-img" src="<?php echo e(asset('frontend/img/no-image.png')); ?>" alt="single-product">
                                                        <?php endif; ?>
                                                    </a>
                                                    <div class="quick-view text-center">
                                                        <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>">Quick View</a>
                                                    </div>
                                                </div>
                                                <!-- Product Image End -->
                                                <!-- Product Content Start -->
                                                <div class="pro-content">
                                                    <h4><a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><?php echo e($item->item_name_en); ?></a></h4>
                                                    <p><span>$<?php echo $item->item_price ? $item->item_price->unit_price : '0.00'; ?></span></p>
                                                    <div class="rating">
                                                        <i class="zmdi zmdi-star"></i>
                                                        <i class="zmdi zmdi-star"></i>
                                                        <i class="zmdi zmdi-star"></i>
                                                        <i class="zmdi zmdi-star"></i>
                                                        <i class="zmdi zmdi-star"></i>
                                                    </div>
                                                    <div class="pro-actions">
                                                        <div class="actions-primary">
                                                            <a href="javascript:void(0)" onclick="addtocart(<?php echo e($item->item_id); ?>);" data-toggle="tooltip" title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                        </div>
                                                    </div>
                                                    <?php if($item->tag): ?>
                                                        <span class="sticker-sale pro-sticker"><?php echo e($item->tag); ?></span>
                                                    <?php endif; ?>
                                                </div>
                                                <!-- Product Content End -->
                                            </div>
                                        </div>
                                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      
                                       <?php if(count($product)<=0): ?>
                                          <h3>Opp!! No product in this category...<i class="fa fa-search fa-4x"></i></h3>
                                      <?php endif; ?>
                                
                                </div>
                                <!-- Grid-view Row End -->
                            </div>
                            <!-- #grid view End -->
                            
                        </div>
                        <!-- Grid & List Main Area End -->
                    </div>
                    <!-- Toolbar Pagination Start -->
                    <div class="toolbar-pagination mb-20 mt-40">
                            <?php echo e($product->links()); ?>

                    </div>
                    <!-- Toolbar Pagination End -->
                </div>
                <!-- product Categorie List End -->


                <!-- Sidebar Shopping Option Start -->
                <div class="col-lg-3 col-lg-pull-9 col-md-3 col-md-pull-9 col-sm-4 col-sm-pull-8">
                    
                    
                    <!-- Sidebar Ads Start -->
                    <div class="sidebar-ads mtb-40 zoom">
                        <a href="#"><img class="full-img" src="<?php echo e(asset('frontend/img/accessories/4.jpg')); ?>" alt="ads-image"></a>
                    </div>
                    <!-- Sidebar Ads End -->
                    
                </div>
                <!-- Sidebar Shopping Option End -->
            </div>
            <!-- Row End -->
        </div>
        <!-- Container End -->
    </div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('footertop'); ?>

    <?php echo $__env->make('frontend.layouts.footertop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('loginmodal'); ?>
    <?php echo $__env->make('frontend.layouts.loginmodal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customJs'); ?>
    <?php $__env->startSection('customJs'); ?>
    <script>

        function addtocart(id)
        {
            
            $.ajax({
                type: "get",
                url: "<?php echo e(url('addtocart')); ?>/"+id,
                //data:'_token = <?php echo csrf_token() ?>',
                success: function(msg) {
                    if(msg=='login')
                    {
                        $('#loginModal').modal(); 
                    }
                    else
                    {
                        $('#toast').html(msg);
                        $('#toast').show(1000);
                    }
                }
            });
        }
    
        function loginAjax()
        {
            $.ajax({
                type: "get",
                url: "<?php echo e(url('loginajax')); ?>",
                data:{
                    e_mail: $('#email').val(),
                    password: $('#paswd').val()
                },
                success: function(msg) {
                    if(msg=='LoginOk'){
                        $('#loginModal').modal('hide');
                    }
                    $('#loginmsg').html(msg);
                }
            });
        }
    
</script>
<?php $__env->stopSection(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend/layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /home/honesttechnic/public_html/website/resources/views/frontend/pages/product.blade.php */ ?>
<?php $__env->startSection('title','Honest Technic | New Products'); ?>
<?php $__env->startSection('user','li_active'); ?>
<?php $__env->startSection('customCss'); ?>
   
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
    <main class="main">
        <ol class="breadcrumb">
           <li class="breadcrumb-item">Home</li>
           <li class="breadcrumb-item">
              <a href="#">Product</a>
           </li>
           <li class="breadcrumb-item active">Add</li>
        </ol>
        <div class="container-fluid">
                <div class="card row">
                        <div class="card-header">
                           <i class="fa fa-edit"></i> Add Product Information
                           <div class="card-header-actions">
                          
                           </div>
                        </div>
                        <div class="card-body">

                            <?php if($errors->any()): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                            
                            <?php if(session('success')): ?>
                                <div class="alert alert-success">
                                <?php echo e(session('success')); ?>

                                </div> 
                            <?php endif; ?>
                              
                        <form action="<?php echo e(url('cp/products/add')); ?>" method="post" enctype="multipart/form-data">
                                <?php echo csrf_field(); ?>
                                
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nf-email">Product Code</label>
                                        <input class="form-control" value="<?php echo e(old('item_code')); ?>"  required name="item_code" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">Product Name</label>
                                        <input class="form-control" value="<?php echo e(old('item_name_en')); ?>"  required name="item_name_en" required type="text">
                                    </div>
                                    <div class="form-group">
                                            <label for="nf-email">#Tag</label>
                                            <input class="form-control" value="<?php echo e(old('tag')); ?>" placeholder="New,Arrival,Promtion" required name="tag" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">#Brands</label>
                                        <input class="form-control" value="<?php echo e(old('brand')); ?>" placeholder="Dell,Acer,Lenovo....."  required name="brand" type="text">
                                    </div>
                                    
                                </div>
                                <div class="col-sm-6">
                                   
                                    <div class="form-group">
                                            <label for="nf-email">Unit Prices</label>
                                            <input class="form-control" value="<?php echo e(old('unit_price')); ?>"  required name="unit_price" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">Category</label>
                                        <select name="category" class="form-control" id="category">
                                            <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($item->cate_id); ?>"><?php echo e($item->cate_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">SubCategory</label>
                                        <select name="sub_category" class="form-control" id="sub_cate">
                                            <?php $__currentLoopData = $sub_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($item->sub_cateid); ?>"><?php echo e($item->sub_cate_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                        <div class="card">
                                            <div class="card-header"><i class="fa fa-edit"></i>Description(Khmer)
                                                <div class="card-header-actions">
                                                    <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#kh" aria-expanded="true"><i class="icon-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="card-body collapse hide" id="kh" style="">
                                                <div class="form-group">
                                                    <textarea class="summernote" name="descriptions_kh"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            <div class="card-header"><i class="fa fa-edit"></i>Description(English)
                                                <div class="card-header-actions">
                                                    <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#collapseExample" aria-expanded="true"><i class="icon-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="card-body collapse hide" id="collapseExample" style="">
                                                <div class="form-group">
                                                    <textarea class="summernote" name="descriptions_en"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card">
                                                <div class="card-header"><i class="fa fa-edit"></i>Description(Vietnam)
                                                    <div class="card-header-actions">
                                                        <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#vn" aria-expanded="true"><i class="icon-arrow-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="card-body collapse hide" id="vn" style="">
                                                    <div class="form-group">
                                                        <textarea class="summernote" name="descriptions_vn"></textarea>
                                                    </div>
                                                </div>
                                         </div>

                                         <div class="card">
                                                <div class="card-header"><i class="fa fa-edit"></i>Description(Chinese)
                                                    <div class="card-header-actions">
                                                        <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#cn" aria-expanded="true"><i class="icon-arrow-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="card-body collapse hide" id="cn" style="">
                                                    <div class="form-group">
                                                        <textarea class="summernote" name="descriptions_cn"></textarea>
                                                    </div>
                                                </div>
                                         </div>

                                         <div class="form-group">
                                            <label for="nf-email">photos</label>
                                            <input type="file" class="form-control" name="photos[]" multiple />
                                        </div>
                                        <div class="form-group">
                                                <button type="submit" class="btn btn-primary" style="margin-top:30px">Save</button>
                                        </div>
                                        
                                    </div>

                                    
                                
                            </div>
                            
                           
                        </form>
                           
                        
                    </div>
                </div>
        </div>
</main>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customJs'); ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.js"></script>
    <script>
        $('.summernote').summernote({
        placeholder: 'Description in english',
        tabsize: 2,
        height: 100
      });
    </script>

    <script>
        $("#category").change(function(){
            var cateId=$(this).val();
            $.ajax({
                url:"<?php echo url('cp/category/')?>/"+cateId+"/sub_category",
                method:'GET',
                dataType:'json',
                success:function(result){
                    var opt="";
                    $.each(result,function(k,v){
                        opt+="<option value='"+v.sub_cateid+"'>"+v.sub_cate_name+"</option>";
                    });
                    $("#sub_cate").html(opt);
                }

            });
        });
        
    </script>
<?php $__env->stopSection(); ?>





     
     
<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /home/honesttechnic/public_html/website/resources/views/backend/products/new.blade.php */ ?>
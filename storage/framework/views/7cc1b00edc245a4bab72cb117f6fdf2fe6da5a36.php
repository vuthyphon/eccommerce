<?php $__env->startSection('title', 'Home | Welcome to Honest Technic'); ?>

<?php $__env->startSection('functionalscript'); ?>

    <script>

        function addtocart(id)
        {
            
            $.ajax({
                type: "get",
                url: "<?php echo e(url('addtocart')); ?>/"+id,
                //data:'_token = <?php echo csrf_token() ?>',
                success: function(msg) {
                    if(msg=='login')
                    {
                        $('#loginModal').modal(); 
                    }
                    else
                    {
                        $('#toast').html(msg);
                        $('#toast').show(1000);
                    }
                }
            });
        }
        
        function loginAjax()
        {
            $.ajax({
                type: "get",
                url: "<?php echo e(url('loginajax')); ?>",
                data:{
                    e_mail: $('#email').val(),
                    password: $('#paswd').val()
                },
                success: function(msg) {
                    if(msg=='LoginOk'){
                        $('#loginModal').modal('hide');
                    }
                    $('#loginmsg').html(msg);
                }
            });
        }
        
    </script>  

<?php $__env->stopSection(); ?>

<?php $__env->startSection('active-home', 'nav-active'); ?>
<?php $__env->startSection('home'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>

<?php echo $__env->make('frontend.layouts.slider', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

        <!-- New Products Start -->
        <div class="new-single-products pb-50" style="background:#fbfbfb;">
                <br><br>
            <div class="container">
                <!-- Group Title Start -->
                <div class="group-title">
                    <h2><?php echo app('translator')->getFromJson('home.new_product'); ?></h2>
                </div>
                <!-- Group Title End -->
                <div class="row">
                    <div class="col-md-12">
                        <!-- New Products Activaion Start -->
                        <div class="new-products second-featured-pro owl-carousel">
                            
                            <?php $__currentLoopData = $newproduct; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <!-- Single Products Start -->
                            <div class="single-product" style="height:229.118px">
                                    <!-- Product Image Start -->
                                    <div class="pro-img">
                                        <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>">
                                            <?php if($item->feature_image): ?>
                                                <img class="primary-img" src="<?php echo e(asset('storage/photos/'.$item->feature_image->filename)); ?>" alt="single-product">
                                            <?php else: ?>
                                               <img class="primary-img" src="<?php echo e(asset('frontend/img/no-image.png')); ?>" alt="single-product">
                                            <?php endif; ?>                                           
                                        </a>
                                        <div class="quick-view">
                                        <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>" alt="">Quick View</a>
    
                                        </div>
                                    </div>
                                    <!-- Product Image End -->
                                    <!-- Product Content Start -->
                                    <div class="pro-content">
                                        <h4><a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><?php echo e($item->item_name_en); ?></a></h4>
                                            <p><span>$<?php echo $item->item_price? number_format($item->item_price->unit_price,2) : 0.00; ?></span></p>
                                        <div class="pro-actions">
                                            <div class="actions-primary">
                                                    <a href="javascript:void(0)" onclick="addtocart(<?php echo e($item->item_id); ?>);" data-toggle="tooltip" title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            </div>
                                        </div>

                                        <?php
                                            if($item->tag!='')
                                                echo "<span class='sticker-new pro-sticker'>$item->tag</span>";
                                                ?>
                                        
                                    </div>
                                    <!-- Product Content End -->
                                </div>
                                <!-- Single Products End -->
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                        </div>
                        <!-- New Products Carousel Activaion End -->
                    </div>
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>
        <!-- New Products End -->
        <!-- Best Seller Product Start -->
        <div class="best-seller-product pb-50" style="background:#fbfbfb;">
            <div class="container">
                <div class="row">
			
                    <!-- Hot Deal Start -->
                    <div class="col-md-4">
                        <div class="sidebar-ads" style="height:417px; background: #fff;">
                            <?php if($item->feature_image): ?>
                                <a href="javascript:void()"><img class="full-img" src="<?php echo e(asset('storage/photos/'.$item->feature_image->filename)); ?>" alt="ads-image"></a>
                            <?php elseif($item->item_price==false): ?>
                                <img class="primary-img" src="<?php echo e(asset('frontend/img/no-image.png')); ?>" alt="single-product">
                            <?php endif; ?>
                        </div>
                    </div>
                    <!-- Hot Deal End -->
					
                    <!-- Best Seller Start -->
                    <div class="col-md-8">
                        <!-- Group Title Start -->
                        <div class="group-title mts">
                            <h2><?php echo app('translator')->getFromJson('home.popular_product'); ?></h2>
                        </div>
                        <!-- Group Title End -->
                        <div class="best-seller owl-carousel">

                            <div class="double-product">

                            <?php $i=0;?>

                            <?php $__currentLoopData = $bestseller; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                
                            <?php $i+=1;?>
                            <?php if($i%2==0): ?>
                            
                                <!-- Single Products Start -->
                                <div class="single-product" style="height:183.61px;">
                                        <!-- Product Image Start -->
                                        <div class="pro-img">
                                            <?php if($item->feature_image): ?>
                                                <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><img class="full-img" src="<?php echo e(asset('storage/photos/'.$item->feature_image->filename)); ?>" alt="ads-image"></a>
                                            <?php elseif($item->item_price==false): ?>
                                                <img class="primary-img" src="<?php echo e(asset('frontend/img/no-image.png')); ?>" alt="single-product">
                                            <?php endif; ?>
                                            <div class="quick-view">
                                                <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>" data-toggle="modal" data-target="#myModal">Quick View</a>
                                            </div>
                                        </div>
                                        <!-- Product Image End -->
                                        <!-- Product Content Start -->
                                        <div class="pro-content">
                                            <h4><a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><?php echo e($item->item_name_en); ?></a></h4>
                                            <p><span>$<?php echo $item->item_price ? number_format($item->item_price->unit_price,2) : '0.00'; ?></span></p>
                                            
                                            <div class="pro-actions">
                                                <div class="actions-primary">
                                                    <a href="javascript:void()" data-toggle="tooltip" title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                </div>
                                                <div class="actions-secondary">
                                                    <a href="javascript:void(0)" onclick="addwishlist(<?php echo e($item->item_id); ?>);" data-toggle="tooltip" title="Add Favourite"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                </div>
                                            </div>
                                            <span class="sticker-new pro-sticker">new</span>
                                        </div>
                                        <!-- Product Content End -->
                                    </div>
                                    <!-- Single Products End -->
                                </div> 

                                <!-- End Double Product -->

                                <div class="double-product">
                            
                            <?php else: ?>
                            
                                <!-- Single Products Start -->
                                <div class="single-product" style="height:183.61px;">
                                        <!-- Product Image Start -->
                                        <div class="pro-img">
                                            <?php if($item->feature_image): ?>
                                                <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><img class="full-img" src="<?php echo e(asset('storage/photos/'.$item->feature_image->filename)); ?>" alt="ads-image"></a>
                                            <?php elseif($item->item_price==false): ?>
                                                <img class="primary-img" src="<?php echo e(asset('frontend/img/no-image.png')); ?>" alt="single-product">
                                            <?php endif; ?>
                                            <div class="quick-view">
                                                <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>" data-toggle="modal" data-target="#myModal">Quick View</a>
                                            </div>
                                        </div>
                                        <!-- Product Image End -->
                                        <!-- Product Content Start -->
                                        <div class="pro-content">
                                        <h4><a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><?php echo e($item->item_name_en); ?></a></h4>
                                            <p><span>$<?php echo $item->item_price ? number_format($item->item_price->unit_price,2) : '0.00'; ?></span></p>
                                            
                                            <div class="pro-actions">
                                                <div class="actions-primary">
                                                    <a href="javascript:void()" data-toggle="tooltip" title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                </div>
                                                <div class="actions-secondary">
                                                    <a href="javascript:void(0)" onclick="addwishlist(<?php echo e($item->item_id); ?>);" data-toggle="tooltip" title="Add Favourite"><i class="zmdi zmdi-favorite-outline"></i></a>
                                                </div>
                                            </div>
                                            <span class="sticker-new pro-sticker">new</span>
                                        </div>
                                        <!-- Product Content End -->
                                    </div>
                                    <!-- Single Products End -->
                            
                            <?php endif; ?>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            
                            </div>
                            
                            
                    </div>
                    <!-- Best Seller End -->
                </div>
            </div>
        </div>
        <!-- Best Seller Product End -->

        <br><br>
        <!-- Featured Products Start -->
        <div class="first-featured-products pb-50" style="background:#fbfbfb;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Featured Product List Item Start -->
                        <ul class="product-list mb-30">
                            <li class="active"><a data-toggle="tab" href="#computer">Computer</a></li>
                            <li><a data-toggle="tab" href="#monitor">Monitor</a></li>
                            <li><a data-toggle="tab" href="#network">Networking</a></li>
                            <li><a data-toggle="tab" href="#camera">Camera</a></li>
                            <li><a data-toggle="tab" href="#electronic">Office Electronics</a></li>
                        </ul>
                        <!-- Featured Product List Item End -->
                    </div>
                </div>
                <div class="tab-content">
                    <div id="computer" class="tab-pane fade in active" style="background:#fbfbfb;">
                        <div class="row">
                            <?php $i=0?>
                            <?php $__currentLoopData = $computer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <?php $i++;?>
                                    <div class="col-sm-3">
                                        <div class="single-product" style="height:349.5px;">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                <?php if($item->feature_image): ?>
                                                    <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><img class="primary-image" src="<?php echo e(asset('storage/photos/'.$item->feature_image->filename)); ?>" alt="ads-image"></a>
                                                <?php else: ?>
                                                <a href="javascript:void()">
                                                   <img class="primary-image" src="<?php echo e(asset('frontend/img/no-image.png')); ?>" alt="single-product">
                                                <?php endif; ?>
                                                </a>
                                                <div class="quick-view text-center">
                                                <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>">Quick View</a>
                                                </div>
                                            </div>
                                            <!-- Product Image End -->

                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4><a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><?php echo e($item->item_name_en); ?></a></h4>
                                                <p><span>$<?php echo $item->item_price ? number_format($item->item_price->unit_price,2) : '0.00'; ?></span></p>
                                                <div class="rating">
                                                    <i class="">&nbsp;</i>
                    
                                                </div>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                    </div>
                                                    <div class="actions-secondary">
                                                        <a href="javascript:void(0)" onclick="addtocart(<?php echo e($item->item_id); ?>);" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </div>
                                                </div>
                                                <span class="sticker-new pro-sticker">new</span>
                                            </div>
                                            <!-- Product Content End -->
                                        </div>
                                    </div>
                                    <?php if($i%4==0 && $i<8) 
                                        echo "</div><div class='row'>";
                                    ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <!-- Single Products End -->
                            </div>
                        </div>
                                        <!-- Row End --> 
                        <!-- #computer End -->
                        <div id="monitor" class="tab-pane fade">
                            <div class="row">
                                    <?php $i=0?>
                                <?php $__currentLoopData = $monitor; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php $i++;?>
                                        <div class="col-md-3 col-sm-6">
                                            <div class="single-product" style="height:349.5px;">
                                                <!-- Product Image Start -->
                                                <div class="pro-img">
                                                    <?php if($item->feature_image): ?>
                                                        <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><img class="full-img" src="<?php echo e(asset('storage/photos/'.$item->feature_image->filename)); ?>" alt="ads-image"></a>
                                                    <?php else: ?>
                                                    <a href="javascript:void()">
                                                        <img class="full-img" src="<?php echo e(asset('frontend/img/no-image.png')); ?>" alt="single-product">
                                                    <?php endif; ?>
                                                    </a>
                                                    <div class="quick-view text-center">
                                                    <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>">Quick View</a>
                                                    </div>
                                                </div>
                                                <!-- Product Image End -->
    
                                                <!-- Product Content Start -->
                                                <div class="pro-content">
                                                    <h4><a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><?php echo e($item->item_name_en); ?></a></h4>
                                                    <p><span>$<?php echo $item->item_price ? number_format($item->item_price->unit_price,2) : '0.00'; ?></span></p>
                                                    <div class="rating">
                                                        <i class="">&nbsp;</i>
                        
                                                    </div>
                                                    <div class="pro-actions">
                                                        <div class="actions-primary">
                                                        </div>
                                                        <div class="actions-secondary">
                                                            <a href="javascript:void(0)" onclick="addtocart(<?php echo e($item->item_id); ?>);" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                        </div>
                                                    </div>
                                                    <span class="sticker-new pro-sticker">new</span>
                                                </div>
                                                <!-- Product Content End -->
                                            </div>
                                        </div>
                                        <?php if($i%4==0 && $i<8) 
                                            echo "</div><div class='row'>";
                                        ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <!-- Single Products End -->
                            </div>
                        </div>
                            <!-- #monitor End -->
                        <div id="network" class="tab-pane fade">
                                <div class="row">
                                    <?php $i=0?>
                                    <?php $__currentLoopData = $networking; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php $i++;?>
                                    <div class="col-md-3 col-sm-6">
                                        <div class="single-product" style="height:349.5px;">
                                            <!-- Product Image Start -->
                                            <div class="pro-img">
                                                <?php if($item->feature_image): ?>
                                                    <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><img class="full-img" src="<?php echo e(asset('storage/photos/'.$item->feature_image->filename)); ?>" alt="ads-image"></a>
                                                <?php else: ?>
                                                <a href="javascript:void()">
                                                    <img class="full-img" src="<?php echo e(asset('frontend/img/no-image.png')); ?>" alt="single-product">
                                                <?php endif; ?>
                                                </a>
                                                <div class="quick-view text-center">
                                                <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>">Quick View</a>
                                                </div>
                                            </div>
                                            <!-- Product Image End -->

                                            <!-- Product Content Start -->
                                            <div class="pro-content">
                                                <h4><a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><?php echo e($item->item_name_en); ?></a></h4>
                                                <p><span>$<?php echo $item->item_price ? number_format($item->item_price->unit_price,2) : '0.00'; ?></span></p>
                                                <div class="rating">
                                                    <i class="">&nbsp;</i>
                    
                                                </div>
                                                <div class="pro-actions">
                                                    <div class="actions-primary">
                                                    </div>
                                                    <div class="actions-secondary">
                                                        <a href="javascript:void(0)" onclick="addtocart(<?php echo e($item->item_id); ?>);" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                    </div>
                                                </div>
                                                <span class="sticker-new pro-sticker">new</span>
                                            </div>
                                        <!-- Product Content End -->
                                    </div>
                                </div>
                                <?php if($i%4==0 && $i<8) 
                                            echo "</div><div class='row'>";
                                        ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <!-- Single Products End -->
                            </div>
                        </div>
                            <!-- #network End -->
                            <div id="camera" class="tab-pane fade">
                                    <div class="row">
                                            <?php $i=0?>
                                    <?php $__currentLoopData = $camera; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php $i++;?>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="single-product" style="height:349.5px;">
                                                    <!-- Product Image Start -->
                                                    <div class="pro-img">
                                                        <?php if($item->feature_image): ?>
                                                            <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><img class="full-img" src="<?php echo e(asset('storage/photos/'.$item->feature_image->filename)); ?>" alt="ads-image"></a>
                                                        <?php elseif($item->feature_image==false): ?>
                                                        <a href="javascript:void()">
                                                            <img class="full-img" src="<?php echo e(asset('frontend/img/no-image.png')); ?>" alt="single-product">
                                                        <?php endif; ?>
                                                        </a>
                                                        <div class="quick-view text-center">
                                                        <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>">Quick View</a>
                                                        </div>
                                                    </div>
                                                    <!-- Product Image End -->
        
                                                    <!-- Product Content Start -->
                                                    <div class="pro-content">
                                                        <h4><a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><?php echo e($item->item_name_en); ?></a></h4>
                                                        <p><span>$<?php echo $item->item_price ? number_format($item->item_price->unit_price,2) : '0.00'; ?></span></p>
                                                        <div class="rating">
                                                            <i class="">&nbsp;</i>
                            
                                                        </div>
                                                        <div class="pro-actions">
                                                            <div class="actions-primary">
                                                            </div>
                                                            <div class="actions-secondary">
                                                                <a href="javascript:void(0)" onclick="addtocart(<?php echo e($item->item_id); ?>);" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                            </div>
                                                        </div>
                                                        <span class="sticker-new pro-sticker">new</span>
                                                    </div>
                                                    <!-- Product Content End -->
                                                </div>
                                            </div>
                                            <?php if($i%4==0 && $i<8) 
                                                echo "</div><div class='row'>";
                                            ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <!-- Single Products End -->
                                </div>
                            </div>
                            <!-- #camera End -->
                            <div id="electronic" class="tab-pane fade">

                                    <div class="row">
                                            <?php $i=0?>
                                            <?php $__currentLoopData = $electronics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php $i++;?>
                                                    <div class="col-md-3 col-sm-6">
                                                        <div class="single-product" style="height:349.5px;">
                                                            <!-- Product Image Start -->
                                                            <div class="pro-img">
                                                                <?php if($item->feature_image): ?>
                                                                    <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><img class="full-img" src="<?php echo e(asset('storage/photos/'.$item->feature_image->filename)); ?>" alt="ads-image"></a>
                                                                <?php elseif($item->feature_image==false): ?>
                                                                <a href="javascript:void()">
                                                                    <img class="full-img" src="<?php echo e(asset('frontend/img/no-image.png')); ?>" alt="single-product">
                                                                <?php endif; ?>
                                                                </a>
                                                                <div class="quick-view text-center">
                                                                <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>">Quick View</a>
                                                                </div>
                                                            </div>
                                                            <!-- Product Image End -->
                
                                                            <!-- Product Content Start -->
                                                            <div class="pro-content">
                                                                <h4><a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><?php echo e($item->item_name_en); ?></a></h4>
                                                                <p><span>$<?php echo $item->item_price ? number_format($item->item_price->unit_price,2) : '0.00'; ?></span></p>
                                                                <div class="rating">
                                                                    <i class="">&nbsp;</i>
                                    
                                                                </div>
                                                                <div class="pro-actions">
                                                                    <div class="actions-primary">
                                                                    </div>
                                                                    <div class="actions-secondary">
                                                                        <a href="javascript:void(0)" onclick="addtocart(<?php echo e($item->item_id); ?>);" data-toggle="tooltip" title="" data-original-title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                                                    </div>
                                                                </div>
                                                                <span class="sticker-new pro-sticker">new</span>
                                                            </div>
                                                            <!-- Product Content End -->
                                                        </div>
                                                    </div>
                                                    <?php if($i%4==0 && $i<8) 
                                                        echo "</div><div class='row'>";
                                                    ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            <!-- Single Products End -->
                                        </div>
                            </div>
                                   
                            <!-- #electronic End -->
                        </div>
                        <!-- Tab-Content End -->
                    </div>
            </div>
            <!-- Container End -->
        </div>
        <!-- Featured Products End -->
        <!-- Ads Banner Start -->
	

<?php $__env->stopSection(); ?>   

<?php $__env->startSection('footertop'); ?>

    <?php echo $__env->make('frontend.layouts.footertop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('loginmodal'); ?>

    <?php echo $__env->make('frontend.layouts.loginmodal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend/layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /home/honesttechnic/public_html/website/resources/views/frontend/pages/home.blade.php */ ?>
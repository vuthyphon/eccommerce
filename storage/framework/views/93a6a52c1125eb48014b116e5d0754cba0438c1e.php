<?php /* D:\laragon\www\honesttechnic\resources\views/frontend/layouts/category_menu.blade.php */ ?>
  <!-- Header Middle Start -->
  <div class="header-middle ptb-20 white-bg">
    <div class="container">
        <div class="row">
            <!-- Logo Start -->
            <div class="col-lg-3 col-md-4">
                <div class="logo">
                    <a href="<?php echo e(route('home')); ?>"><img src="<?php echo e(asset ('frontend/img/logo/logo.png')); ?>" alt="Honest Technic" title="Honest Technic"></a>
                </div>
            </div>
            <!-- Logo End -->
            <!-- Header Middle Menu Start -->
            <div class="col-lg-9 col-md-8">
                <div class="middle-menu hidden-sm hidden-xs">
                    <nav>
                        <ul class="middle-menu-list">
                            <li><a href="<?php echo e(route('home')); ?>" class="<?php echo $__env->yieldContent('active-home'); ?>" alt="Home" title="Home"><?php echo app('translator')->getFromJson('home.home_menu'); ?></a></li>
                            <li><a href="<?php echo e(route('product')); ?>" class="<?php echo $__env->yieldContent('active-product'); ?>" alt="Product" title="Product"><?php echo app('translator')->getFromJson('home.product_menu'); ?></a></li>
                            <li><a href="<?php echo e(route('about')); ?>" class="<?php echo $__env->yieldContent('active-about'); ?>" alt="About Honest Technic" title="About Honest Technic"><?php echo app('translator')->getFromJson('home.about_menu'); ?></a></li>
                            <li><a href="<?php echo e(url('/contact')); ?>" class="<?php echo $__env->yieldContent('active-contact'); ?>" alt="Contact" title="Contact"><?php echo app('translator')->getFromJson('home.contact_menu'); ?></a></li>
                            <li><a href="<?php echo e(url('register')); ?>" class="<?php echo $__env->yieldContent('active-register'); ?>" alt="Register" title="Register"><?php echo app('translator')->getFromJson('home.register_menu'); ?></a></li>
                            <li><a href="<?php echo e(route('signin')); ?>" class="<?php echo $__env->yieldContent('active-login'); ?>" alt="Signin" title="Signin"><?php echo app('translator')->getFromJson('home.sign_in_menu'); ?></a></li>
                            
                        </ul>
                    </nav>
                </div>

               <!-- Main Cart Box Start 
                Mobile view for check out
                -->
                <div class="cart-box visible-xs">
                    <ul>
                        <li>
                           
                            
                            <a href="<?php echo e(route('mycart')); ?>">
                            
                                    <?php if(count($mycart)>0): ?>
                                        <span style="position: absolute;
                                        right: -3px;
                                        top: -7px;
                                        background: red;
                                        color: #fff;
                                        width: 16px;
                                        height: 16px;
                                        border-radius: 50%;
                                        text-align: center;
                                        font-size: 10px;
                                        line-height: 16px;"> <?php echo e(count($mycart)); ?></span>
                                    <?php endif; ?>    
                            </a> 
                            
                        </li>
                    </ul>
                </div>
                <!-- Main Cart Box End -->

                
                <!-- Mobile Menu  Start -->
                <div class="mobile-menu visible-sm visible-xs">
                        <nav>
                            <ul>
                                <li><a href="<?php echo e(route('home')); ?>"><?php echo app('translator')->getFromJson('home.home_menu'); ?></a></li>
                                <li><a href="<?php echo e(url('product/cate-10')); ?>"><?php echo app('translator')->getFromJson('category.paper'); ?></a><!--Paper!-->
                                   
                                    <ul>
                                        <li><a href="<?php echo e(url('product/category/53')); ?>"><?php echo app('translator')->getFromJson('category.printer_paper'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/54')); ?>"><?php echo app('translator')->getFromJson('category.color_paper'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/55')); ?>"><?php echo app('translator')->getFromJson('category.notebook'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/56')); ?>"><?php echo app('translator')->getFromJson('category.post_it_related'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/57')); ?>"><?php echo app('translator')->getFromJson('category.subpoena'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/58')); ?>"><?php echo app('translator')->getFromJson('category.envelope'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/59')); ?>"><?php echo app('translator')->getFromJson('category.cardboard'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/60')); ?>"><?php echo app('translator')->getFromJson('category.label_sticker'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/61')); ?>"><?php echo app('translator')->getFromJson('category.inspection_paper'); ?></a></li>
                                    </ul>
                                  
                                <li><a href="<?php echo e(url('product/cate-11')); ?>"><?php echo app('translator')->getFromJson('category.office_supplies'); ?></a>
                                    <!-- Office Suplies-->
                                    <ul>
                                        <li><a href="<?php echo e(url('product/category/62')); ?>"><?php echo app('translator')->getFromJson('category.hole_puncher'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/63')); ?>"><?php echo app('translator')->getFromJson('category.stapler'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/64')); ?>"><?php echo app('translator')->getFromJson('category.scissor_paper_cutter'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/65')); ?>"><?php echo app('translator')->getFromJson('category.utility_knife'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/66')); ?>"><?php echo app('translator')->getFromJson('category.correction_tap'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/67')); ?>"><?php echo app('translator')->getFromJson('category.glue'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/68')); ?>"><?php echo app('translator')->getFromJson('category.various_tap'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/69')); ?>"><?php echo app('translator')->getFromJson('category.measuring'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/70')); ?>"><?php echo app('translator')->getFromJson('category.clip_item'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/71')); ?>"><?php echo app('translator')->getFromJson('category.stamps'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/72')); ?>"><?php echo app('translator')->getFromJson('category.guard_film'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/73')); ?>"><?php echo app('translator')->getFromJson('category.stationeries'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/74')); ?>"><?php echo app('translator')->getFromJson('category.package_items'); ?></a></li>
                                    </ul>
                                    
                                </li>
                                <li><a href="<?php echo e(url('product/cate-12')); ?>"><?php echo app('translator')->getFromJson('category.business_supplies'); ?></a>
                                    <!-- Business Supplies -->
                                    <ul>
                                        <li><a href="<?php echo e(url('product/category/75')); ?>"><?php echo app('translator')->getFromJson('category.calculator'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/76')); ?>"><?php echo app('translator')->getFromJson('category.battery'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/77')); ?>"><?php echo app('translator')->getFromJson('category.usb_mouse_'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/78')); ?>"><?php echo app('translator')->getFromJson('category.flash_disc'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/79')); ?>"><?php echo app('translator')->getFromJson('category.printing_devices'); ?></a></li>
                                    </ul>
                                    
                                </li>
                                <li><a href="<?php echo e(url('product/cate-13')); ?>"><?php echo app('translator')->getFromJson('category.pen'); ?></a>
                                    <!--Pen -->
                                    <ul>
                                        <li><a href="<?php echo e(url('product/category/80')); ?>"><?php echo app('translator')->getFromJson('category.gel_pen'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/81')); ?>"><?php echo app('translator')->getFromJson('category.ball_pen'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/82')); ?>"><?php echo app('translator')->getFromJson('category.mike_oil'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/83')); ?>"><?php echo app('translator')->getFromJson('category.pencil_'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/84')); ?>"><?php echo app('translator')->getFromJson('category.automatic_'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/85')); ?>"><?php echo app('translator')->getFromJson('category.fountain_pen'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/86')); ?>"><?php echo app('translator')->getFromJson('category.paint_'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/87')); ?>"><?php echo app('translator')->getFromJson('category.other_pens'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/88')); ?>"><?php echo app('translator')->getFromJson('category.pencil_case'); ?></a></li>
                                    </ul>
                                    
                                </li>
                                <li><a href="<?php echo e(url('product/cate-14')); ?>"><?php echo app('translator')->getFromJson('category.conference_supplies'); ?></a>
                                    <!-- conference_supplies 	 -->
                                    <ul>
                                        <li><a href="<?php echo e(url('product/category/89')); ?>"><?php echo app('translator')->getFromJson('category.various_board_'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/90')); ?>"><?php echo app('translator')->getFromJson('category.bulletin_'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/91')); ?>"><?php echo app('translator')->getFromJson('category.magnet'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/92')); ?>"><?php echo app('translator')->getFromJson('category.office_appliance'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/93')); ?>"><?php echo app('translator')->getFromJson('category.hole_'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/94')); ?>"><?php echo app('translator')->getFromJson('category.data_class_'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/95')); ?>"><?php echo app('translator')->getFromJson('category.information_bag'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/96')); ?>"><?php echo app('translator')->getFromJson('category.file_box'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/97')); ?>"><?php echo app('translator')->getFromJson('category.business_card'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/98')); ?>"><?php echo app('translator')->getFromJson('category.hole_guard'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/99')); ?>"><?php echo app('translator')->getFromJson('category.seperate_'); ?></a></li>
                                    </ul>
                                    
                                </li>
                                <li><a href="<?php echo e(url('product/cate-15')); ?>"><?php echo app('translator')->getFromJson('category.tool'); ?></a>
                                    <!-- Tool -->
                                    <ul>
                                        <li><a href="<?php echo e(url('product/category/100')); ?>"><?php echo app('translator')->getFromJson('category.tool_set'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/101')); ?>"><?php echo app('translator')->getFromJson('category.hand_tool'); ?></a></li>
                                    </ul>
                                    
                                </li>
                                <li><a href="<?php echo e(url('product/cate-16')); ?>"><?php echo app('translator')->getFromJson('category.cleaning_supplies'); ?></a>
                                    <!-- Cleaning Supplies -->
                                    <ul>
                                        <li><a href="<?php echo e(url('product/category/102')); ?>"><?php echo app('translator')->getFromJson('category.labor_'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/103')); ?>"><?php echo app('translator')->getFromJson('category.detergents_'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/104')); ?>"><?php echo app('translator')->getFromJson('category.toilet_paper'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/105')); ?>"><?php echo app('translator')->getFromJson('category.mop_broom_'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/106')); ?>"><?php echo app('translator')->getFromJson('category.rag'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/107')); ?>"><?php echo app('translator')->getFromJson('category.cotton_gloves_'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/108')); ?>"><?php echo app('translator')->getFromJson('category.brush_melon_'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/109')); ?>"><?php echo app('translator')->getFromJson('category.other_'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/110')); ?>"><?php echo app('translator')->getFromJson('category.air_purifier'); ?></a></li>
                                    </ul>
                                    
                                </li>
                                <li><a href="<?php echo e(url('product/cate-17')); ?>"><?php echo app('translator')->getFromJson('category.light_'); ?></a>
                                    <!-- Light -->
                                    <ul>
                                        <li><a href="<?php echo e(url('product/category/111')); ?>"><?php echo app('translator')->getFromJson('category.led'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/112')); ?>"><?php echo app('translator')->getFromJson('category.extenstion_cord'); ?></a></li>
                                        <li><a href="<?php echo e(url('product/category/113')); ?>"><?php echo app('translator')->getFromJson('category.other_electronics'); ?></a></li>
                                    </ul>
                                    
                                </li>
                                <li><a href="<?php echo e(route('about')); ?>"><?php echo app('translator')->getFromJson('home.about_menu'); ?></a></li>
                                <li><a href="<?php echo e(url('/contact')); ?>"><?php echo app('translator')->getFromJson('home.contact_menu'); ?></a></li>
                                <li><a href="<?php echo e(route('signout')); ?>"><?php echo app('translator')->getFromJson('home.signout'); ?></a></li>
                            </ul>
                        </nav>
                    </div>
                <!-- Mobile Menu  End -->
                
                
                
            </div>
            <!-- Header Middle Menu End -->
            
            
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- Header Middle End -->



            <!-- Header Bottom Start -->
            <div class="header-bottom ptb-10 blue-bg">
                <div class="container">
                    <div class="row">

                    
                        <!-- Primary Vertical-Menu Start -->
                        <div class="col-lg-4 col-md-4 col-sm-4 hidden-sm hidden-xs">
                                <div class="vertical-menu">
                                    <span class="categorie-title"><?php echo app('translator')->getFromJson('category.cate_menu'); ?></span>
                                    <nav>
                                        <?php if(View::hasSection('home')): ?>
                                            <ul class="vertical-menu-list">
                                        
                                        <?php else: ?>
                                            <ul class="vertical-menu-list menu-hideen">
                                        <?php endif; ?>
                                        
                                        
                                            <li><a href="<?php echo e(url('product/cate-10')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/computer.png')); ?>" ></span><?php echo app('translator')->getFromJson('category.paper'); ?></a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column pb-20 fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="<?php echo e(url('product/category/53')); ?>"><?php echo app('translator')->getFromJson('category.printer_paper'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/54')); ?>"><?php echo app('translator')->getFromJson('category.color_paper'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/55')); ?>"><?php echo app('translator')->getFromJson('category.notebook'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/56')); ?>"><?php echo app('translator')->getFromJson('category.post_it_related'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/57')); ?>"><?php echo app('translator')->getFromJson('category.subpoena'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/58')); ?>"><?php echo app('translator')->getFromJson('category.envelope'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/59')); ?>"><?php echo app('translator')->getFromJson('category.cardboard'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/60')); ?>"><?php echo app('translator')->getFromJson('category.label_sticker'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/61')); ?>"><?php echo app('translator')->getFromJson('category.inspection_paper'); ?></a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                        </ul>
                                                    </li>
                                                
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-11')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/monitors.png')); ?>" ></span><?php echo app('translator')->getFromJson('category.office_supplies'); ?></a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                            
                                                                <ul>
                                                                    <li><a href="<?php echo e(url('product/category/62')); ?>"><?php echo app('translator')->getFromJson('category.hole_puncher'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/63')); ?>"><?php echo app('translator')->getFromJson('category.stapler'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/64')); ?>"><?php echo app('translator')->getFromJson('category.scissor_paper_cutter'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/65')); ?>"><?php echo app('translator')->getFromJson('category.utility_knife'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/66')); ?>"><?php echo app('translator')->getFromJson('category.correction_tap'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/67')); ?>"><?php echo app('translator')->getFromJson('category.glue'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/68')); ?>"><?php echo app('translator')->getFromJson('category.various_tap'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/69')); ?>"><?php echo app('translator')->getFromJson('category.measuring'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/70')); ?>"><?php echo app('translator')->getFromJson('category.clip_item'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/71')); ?>"><?php echo app('translator')->getFromJson('category.stamps'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/72')); ?>"><?php echo app('translator')->getFromJson('category.guard_film'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/73')); ?>"><?php echo app('translator')->getFromJson('category.stationeries'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/74')); ?>"><?php echo app('translator')->getFromJson('category.package_items'); ?></a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                                                                    
                                                        </ul>
                                                    </li>
                                                    <!-- Mega-Menu Three Column End -->
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-12')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/component.png')); ?>" ></span><?php echo app('translator')->getFromJson('category.business_supplies'); ?></a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Two Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                   
                                                                    <li><a href="<?php echo e(url('product/category/75')); ?>"><?php echo app('translator')->getFromJson('category.calculator'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/76')); ?>"><?php echo app('translator')->getFromJson('category.battery'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/77')); ?>"><?php echo app('translator')->getFromJson('category.usb_mouse_'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/78')); ?>"><?php echo app('translator')->getFromJson('category.flash_disc'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/79')); ?>"><?php echo app('translator')->getFromJson('category.printing_devices'); ?></a></li>
                                                                    
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                            
                                                        </ul>
                                                    </li>
                                                    
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-13')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/networking.png')); ?>" ></span><?php echo app('translator')->getFromJson('category.pen'); ?></a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="<?php echo e(url('product/category/80')); ?>"><?php echo app('translator')->getFromJson('category.gel_pen'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/81')); ?>"><?php echo app('translator')->getFromJson('category.ball_pen'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/82')); ?>"><?php echo app('translator')->getFromJson('category.mike_oil'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/83')); ?>"><?php echo app('translator')->getFromJson('category.pencil_'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/84')); ?>"><?php echo app('translator')->getFromJson('category.automatic_'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/85')); ?>"><?php echo app('translator')->getFromJson('category.fountain_pen'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/86')); ?>"><?php echo app('translator')->getFromJson('category.paint_'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/87')); ?>"><?php echo app('translator')->getFromJson('category.other_pens'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/88')); ?>"><?php echo app('translator')->getFromJson('category.pencil_case'); ?></a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                            
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-14')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/camera.png')); ?>" ></span><?php echo app('translator')->getFromJson('category.conference_supplies'); ?></a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                            
                                                                <ul>
                                                                    <li><a href="<?php echo e(url('product/category/89')); ?>"><?php echo app('translator')->getFromJson('category.various_board_'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/90')); ?>"><?php echo app('translator')->getFromJson('category.bulletin_'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/91')); ?>"><?php echo app('translator')->getFromJson('category.magnet'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/92')); ?>"><?php echo app('translator')->getFromJson('category.office_appliance'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/93')); ?>"><?php echo app('translator')->getFromJson('category.hole_'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/94')); ?>"><?php echo app('translator')->getFromJson('category.data_class_'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/95')); ?>"><?php echo app('translator')->getFromJson('category.information_bag'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/96')); ?>"><?php echo app('translator')->getFromJson('category.file_box'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/97')); ?>"><?php echo app('translator')->getFromJson('category.business_card'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/98')); ?>"><?php echo app('translator')->getFromJson('category.hole_guard'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/99')); ?>"><?php echo app('translator')->getFromJson('category.seperate_'); ?></a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                            
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-15')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/printer.png')); ?>" ></span><?php echo app('translator')->getFromJson('category.tool'); ?></a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="<?php echo e(url('product/category/100')); ?>"><?php echo app('translator')->getFromJson('category.tool_set'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/101')); ?>"><?php echo app('translator')->getFromJson('category.hand_tool'); ?></a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-16')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/storage.png')); ?>" ></span><?php echo app('translator')->getFromJson('category.cleaning_supplies'); ?></a>
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="<?php echo e(url('product/category/102')); ?>"><?php echo app('translator')->getFromJson('category.labor_'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/103')); ?>"><?php echo app('translator')->getFromJson('category.detergents_'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/104')); ?>"><?php echo app('translator')->getFromJson('category.toilet_paper'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/105')); ?>"><?php echo app('translator')->getFromJson('category.mop_broom_'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/106')); ?>"><?php echo app('translator')->getFromJson('category.rag'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/107')); ?>"><?php echo app('translator')->getFromJson('category.cotton_gloves_'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/108')); ?>"><?php echo app('translator')->getFromJson('category.brush_melon_'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/109')); ?>"><?php echo app('translator')->getFromJson('category.other_'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/110')); ?>"><?php echo app('translator')->getFromJson('category.air_purifier'); ?></a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-17')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/storage.png')); ?>" ></span><?php echo app('translator')->getFromJson('category.light_'); ?></a>
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="<?php echo e(url('product/category/111')); ?>"><?php echo app('translator')->getFromJson('category.led'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/112')); ?>"><?php echo app('translator')->getFromJson('category.extenstion_cord'); ?></a></li>
                                                                    <li><a href="<?php echo e(url('product/category/113')); ?>"><?php echo app('translator')->getFromJson('category.other_electronics'); ?></a></li>
                                                                 </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            
                                            <!-- More Categoies Start -->
                                            <li id="cate-toggle" class="category-menu">
                                                <ul>
                                                    <li class="has-sub"> <a href="#">More Categories</a>
                                                        <ul class="category-sub">
                                                            <li><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/5.png')); ?>" ></span> <a href="javascript:void(0)">No more categories</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <!-- More Categoies End -->
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        <!-- Primary Vertical-Menu End -->
                        <!-- Search Box Start -->
                        <div class="col-lg-6 col-md-5 col-sm-8">
                            <div class="search-box-view fix">
                            <form action="<?php echo e(url('product/search')); ?>" method="GET">
                                    <input class="email" type="text" placeholder="<?php echo app('translator')->getFromJson('home.search_in_store'); ?>" name="filter" id="search">
                                    <button type="submit" class="submit"></button>
                                </form>
                            </div>
                        </div>
                        <!-- Search Box End -->
                        <!-- Cartt Box Start -->
                        <div class="col-lg-2 col-md-3 col-sm-4">
                            <div class="cart-box hidden-xs">
                                <ul>
                                    <li>
                                    <a href="<?php echo e(route('mycart')); ?>"><span class="cart-text"><?php echo app('translator')->getFromJson('home.my_cart'); ?></span>
                                            <span class="cart-counter">
                                                    <?php if(isset($mycart)): ?> <?php echo e(count($mycart)); ?> <?php endif; ?> item(s)
                                            </span>
                                        </a>
                                        
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Cartt Box End -->
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Container End -->
            </div>
            <!-- Header Bottom End -->
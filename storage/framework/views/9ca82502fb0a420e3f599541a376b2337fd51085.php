
<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
      <meta name="description" content="Honest Technic,Computer,Printer,Accessary,">
      <meta name="author" content="Phon Vuthy">
      <meta name="keyword" content="Honest Technic">
      <title><?php echo $__env->yieldContent('title'); ?></title>
      <?php echo $__env->yieldContent('mainCss'); ?>

      <?php echo $__env->yieldContent('customCss'); ?>

     

   </head>
   <body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
       <?php echo $__env->yieldContent('header'); ?>

      <div class="app-body">
        <!-- left side bar -->
        <?php echo $__env->yieldContent('left-side'); ?>
                 <!-- left side bar -->

        <?php echo $__env->yieldContent('main-content'); ?>
      </div>
      <footer class="app-footer">
         <div>
            <span>© 2019 HonestTechnic</span>
         </div>
         <div class="ml-auto">
            <span></span>
         </div>
      </footer>
      <?php echo $__env->yieldContent('customModal'); ?>;
      <?php echo $__env->yieldContent('mainJs'); ?>
      <?php echo $__env->yieldContent('customJs'); ?>
   </body>
</html>


<?php /* D:\laragon\www\website\resources\views/backend/layouts/master.blade.php */ ?>
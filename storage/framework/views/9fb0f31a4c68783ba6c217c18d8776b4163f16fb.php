<?php /* D:\laragon\www\honesttechnic\resources\views/backend/users/index.blade.php */ ?>
<?php $__env->startSection('title','ប្រព័ន្ធគ្រប់គ្រងបេសកកម្ម | អ្នកប្រើប្រាស់'); ?>
<?php $__env->startSection('user','li_active'); ?>

<?php $__env->startSection('main-content'); ?>
    <main class="main">
        <ol class="breadcrumb">
           <li class="breadcrumb-item">
              <a href="#"><?php echo e(Auth::user()->username); ?></a>
           </li>
           <li class="breadcrumb-item active">អ្នកប្រើប្រាស់</li>
           <li class="breadcrumb-menu d-md-down-none">
              <div class="btn-group" role="group" aria-label="Button group">
                 <a class="btn" href="#">
                 <i class="icon-speech"></i>
                 </a>
                 <a class="btn" href="index.html">
                 <i class="icon-graph"></i> &nbsp;Dashboard</a>
                 <a class="btn" href="#">
                 <i class="icon-settings"></i> &nbsp;Settings</a>
              </div>
           </li>
        </ol>
        <div class="container-fluid">
                <div class="card row">
                        <div class="card-header">
                           <i class="fa fa-edit"></i> ព័ត៌មានអ្នកប្រើប្រាស់
                           <div class="card-header-actions">
                           <a href="<?php echo e(url('cp/users/new')); ?>" class="btn btn-primary">បង្កើតថ្មី</a>
                           </div>
                        </div>
                        <div class="card-body">
                        <?php echo session('message'); ?>

                            
                        <table class="table table-striped table-bordered" id="myTable" width="100%">
                            <thead>
                                <tr role="row">
                                    <th>ល.រ</th>
                                    <th>អ្នកប្រើប្រាស់</th>
                                    <th>អ៊ីមែល</th>
                                    <th>សិទ្ធីប្របាស់</th>
                                    <th>បង្កើតថ្ងៃ</th>
                                    <th>កែប្រែថ្ងៃ</th>
                                    <th>ស្ថានភាពប្រើប្រាស់</th>
                                    <th>ជម្រើស</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $i=1;
                                ?>
                                <?php if(isset($users)): ?>
                                    <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr role="row">
                                        <td><?php echo e($i++); ?></td>
                                        <td><?php echo e($item->username); ?></td>
                                        <td><?php echo e($item->email); ?></td>
                                        <td><?php echo e($item->user_role->role_name); ?></td>
                                        <td><?php echo e($item->created_at); ?></td>
                                        <td><?php echo e($item->updated_at); ?></td>
                                        <td><?php echo $item->active==1 ? "<i class='fa fa-check-circle'></i>" : '<i class="fa fa-ban"></i>'; ?></td>
                                        <td>
                                            <a class="btn btn-info btn-sm btn-edit" href="<?php echo e(url('cp/users/edit/'.$item->id)); ?>"><i class="fa fa-edit"></i></a>
                                            <a class="btn btn-danger btn-sm" href="<?php echo e(url('cp/users/disable/'.$item->id)); ?>"><i class="fas fa-user-times"></i></a>
                                        </td>
                                    </tr>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
</main>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('customJs'); ?>

<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo e(asset('backend/vendors/dataTable/datatables.js')); ?>"></script>

<script>
    $(document).ready( function () {
        $('#myTable').DataTable({
            "autoWidth":false
        });
});
</script>

<?php $__env->stopSection(); ?>


     
     
<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
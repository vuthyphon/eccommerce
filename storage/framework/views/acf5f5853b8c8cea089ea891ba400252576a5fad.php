<?php /* D:\laragon\www\honesttechnic\resources\views/backend/reports/index.blade.php */ ?>
<?php $__env->startSection('title','Honest Technic | Products'); ?>
<?php $__env->startSection('user','li_active'); ?>

<?php $__env->startSection('main-content'); ?>
    <main class="main">
        <ol class="breadcrumb">
           <li class="breadcrumb-item">
              <a href="#"><?php echo e(Auth::user()->username); ?></a>
           </li>
           <li class="breadcrumb-item active">Client Order Report</li>
      
           <li class="breadcrumb-menu d-md-down-none">
              <div class="btn-group" role="group" aria-label="Button group">
                 <a class="btn" href="#">
                 <i class="icon-speech"></i>
                 </a>
                 <a class="btn" href="index.html">
                 <i class="icon-graph"></i> &nbsp;Dashboard</a>
                 <a class="btn" href="#">
                 <i class="icon-settings"></i> &nbsp;Settings</a>
              </div>
           </li>
        </ol>
        
        <div class="container-fluid">
                <div class="card row">
                        <div class="card-header">
                           <i class="fa fa-edit"> Client Order Report</i>
                           <div class="card-header-actions">
                               <b>From:</b>
                           <input type="date" value="<?php if(Request::segment(3)==''): ?><?php echo e(date('Y-m-d')); ?><?php else: ?><?php echo e(Request::segment(3)); ?><?php endif; ?>" id="frdt" >
                               <b>&nbsp;&nbsp;&nbsp;To:</b>
                               <input type="date" value="<?php if(Request::segment(4)==''): ?><?php echo e(date('Y-m-d')); ?><?php else: ?><?php echo e(Request::segment(4)); ?><?php endif; ?>"  id="todt" >

                               <button class="btn-primary btn-sm" id="btn_search"><b><i class="fa fa-search"></i> Search</b></button>
                                &nbsp;&nbsp;
                                <a href="javascript:void(0);" id="btn_download" class=" btn btn-success btn-sm"><b><i class="fa fa-download"></i> Download</b></a>
                               
                           </div>
                           
                        </div>
                        <div class="card-body">
                        
                            
                        <table class="table table-bordered" width="100%">
                            <thead>
                                <tr role="row">
                                    <th>No</th>
                                    <th>Customer</th>
                                    <th>Tel</th>
                                    <th>Order Date</th>
                                    <th>Product Code</th>
                                    <th>Product Name</th>
                                    <th>Qty</th>
                                    <th>Unit Price($)</th>
                                    <th>Total</th>
                                    <th>Delivery to</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $i=1;
                                    $key='';
                                    $total=0;
                                ?>
                                <?php if(isset($clientorder)): ?>
                                   <?php $__currentLoopData = $clientorder; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                   <?php if($item->order_id!==$key): ?>
                                   <?php $key=$item->order_id; ?>
                                    <tr role="row">
                                        <td><?php echo e($i); ?></td>
                                        <td><?php echo e($item->first_name.' '.$item->last_name); ?></td>
                                        <td><?php echo e($item->tel); ?></td>
                                        <td><?php echo e(date_format(date_create($item->order_date),"d-m-Y")); ?></td>
                                        <td><?php echo e($item->item_code); ?></td>
                                        <td><?php echo e($item->item_name_en); ?></td>
                                        <td><?php echo e($item->qty); ?></td>
                                        <td><?php echo e($item->unit_price); ?></td>
                                        <td><?php echo e($item->qty * $item->unit_price); ?></td>
                                        <th><?php if($item->ref!==''): ?> <?php echo e($item->ref); ?> <?php else: ?> <?php echo e($item->address); ?> <?php endif; ?></th>
                                    </tr>
                                    <?php else: ?> 
                                    <tr role="row">
                                            <td><?php echo e($i); ?></td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td>-</td>
                                            <td><?php echo e($item->item_code); ?></td>
                                            <td><?php echo e($item->item_name_en); ?></td>
                                            <td><?php echo e($item->qty); ?></td>
                                            <td><?php echo e($item->unit_price); ?></td>
                                            <td><?php echo e($item->qty * $item->unit_price); ?></td>
                                            <td>-</td>
                                        </tr>
                                    <?php endif; ?>

                                    <?php
                                        $total+=($item->qty * $item->unit_price);
                                        $i++;
                                    ?>
                                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                   <tr role="row">
                                        <th colspan="8">Total</th>
                                        <th><?php echo e(number_format($total,2)); ?></th>
                                        <th></th>
                                    </tr>

                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
</main>
<?php $__env->stopSection(); ?>


<?php $__env->startSection('customJs'); ?>
    
<script>

    //$('#frdt').datePicker('yy-mm-d');

    $('#btn_search').click(function(){
        window.open("<?php echo e(url('cp/orderreport')); ?>/"+$('#frdt').val()+"/"+$('#todt').val(),'_parent');
    });

    $('#btn_download').click(function(){
        window.open("<?php echo e(url('cp/download')); ?>/"+$('#frdt').val()+"/"+$('#todt').val(),'_blank');
    });

</script>

<?php $__env->stopSection(); ?>


     
     
<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
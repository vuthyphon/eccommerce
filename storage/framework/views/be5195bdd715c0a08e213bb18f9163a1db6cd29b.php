

<!-- Footer Start -->
<footer>
    <!-- Footer Top Start -->
    <div class="footer-top ptb-40 white-bg">
        <div class="container">
            <div class="row">
                <!-- Single Footer Start -->
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="single-footer">
                        <div class="footer-logo mb-20">
                            <a href="<?php echo e(route('home')); ?>"><img class="img" src="<?php echo e(asset('frontend/img/logo/logo.png')); ?>" alt="logo-img"></a>
                        </div>
                        <div class="footer-content">
                            <!--<p class="f-left">Copyright © 2019 <a href="https://honesttechnic.com/">Honesttechnic.</a><br>All Rights Reserved.</p>-->
                        </div>
                    </div>
                </div>
                <!-- Single Footer Start -->
                <!-- Single Footer Start -->
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="single-footer">
                        <h3 class="footer-title">Cambodia</h3>
                        <div class="footer-content">
                            <ul class="footer-list first-content">
                                <li><i class="zmdi zmdi-phone-in-talk"></i> +(855)11 903 909</li>
                                <li><i class="zmdi zmdi-print"></i> +(855)23 636 8882</li>
                                <li>
                                    <i class="zmdi zmdi-pin-drop"></i> Address : No 6 E0E1, Str 5, Phumi Tropaingthleoung, Sangkat Choam Choav, Khan Pursenchey, Phnom Penh, Cambodia.
                                </li>
                                <li></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Single Footer Start -->
                <!-- Single Footer Start -->
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="single-footer">
                        <h3 class="footer-title">Vietnam</h3>
                        <div class="footer-content">
                            <ul class="footer-list first-content">
                                <li><i class="zmdi zmdi-phone-in-talk"></i> +(1234) 567 890</li>
                                <li><i class="zmdi zmdi-email"></i><a href="#">mailto:info@roadthemes.com</a></li>
                                <li>
                                    <i class="zmdi zmdi-pin-drop"></i> Address : No 40 Baria Sreet 133/2 <br> NewYork City, NY, United States.
                                </li>
                                <li></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Single Footer Start -->
                <!-- Single Footer Start -->
                <div class="col-lg-3 col-md-3 col-sm-6">
                    <div class="single-footer">
                        <h3 class="footer-title">Support</h3>
                        <div class="footer-content">
                            <ul class="footer-list">
                                <li><a href="<?php echo e(route('about')); ?>">About Us</a></li>
                                <li><a href="<?php echo e(url('register')); ?>">Register</a></li>
                                <li><a href="<?php echo e(url('/contact')); ?>">contact us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- Single Footer Start -->
            </div>
            <!-- Row End -->
        </div>
        <!-- Container End -->
    </div>
    <!-- Footer Top End -->
    
    
    <div id="toast" style="position:fixed; height:40px; right:0; top:2px; z-index:1000; display:none;">
        <!-- toast alert show status of adding item to cart --> 
    </div>
    
    
</footer>
<!-- Footer End -->
<?php /* /home/honesttechnic/public_html/website/resources/views/frontend/layouts/footertop.blade.php */ ?>
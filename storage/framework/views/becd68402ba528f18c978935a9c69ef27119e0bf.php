<?php /* /home/honesttechnic/public_html/website/resources/views/excel/pswm_daily.blade.php */ ?>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="<?php echo e(asset('css/table.css')); ?>" rel="stylesheet">
    </head>

    
    <body>
            <h3>Customer Order Report</h3>
            <h4>From: <?php echo e($frdt); ?> To: <?php echo e($todt); ?></h4>
            
            <table class="table table-bordered" width="100%">
                <thead>
                    <tr role="row">
                        <th>No</th>
                        <th>Customer</th>
                        <th>Tel</th>
                        <th>Order Date</th>
                        <th>Product Code</th>
                        <th>Product Name</th>
                        <th>Qty</th>
                        <th>Unit Price($)</th>
                        <th>Total</th>
                        <th>Delivery to</th>
                    </tr>
                </thead>
                <tbody>

                    <?php if(isset($order_data)): ?>
                    <?php
                        $i=1;
                        $key='';
                        $total=0;
                    ?>
        
                    <?php $__currentLoopData = $order_data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    
                        <?php if($item->order_id!==$key): ?>
                        <?php $key=$item->order_id; ?>
                        <tr role="row">
                            <td><?php echo e($i); ?></td>
                            <td><?php echo e($item->first_name.' '.$item->last_name); ?></td>
                            <td><?php echo e($item->tel); ?></td>
                            <td><?php echo e(date_format(date_create($item->order_date),"d-m-Y")); ?></td>
                            <td><?php echo e($item->item_code); ?></td>
                            <td><?php echo e($item->item_name_en); ?></td>
                            <td><?php echo e($item->qty); ?></td>
                            <td><?php echo e($item->unit_price); ?></td>
                            <td><?php echo e($item->qty * $item->unit_price); ?></td>
                            <th><?php if($item->ref!==''): ?> <?php echo e($item->ref); ?> <?php else: ?> <?php echo e($item->address); ?> <?php endif; ?></th>
                        </tr>
                        <?php else: ?> 
                        <tr role="row">
                                <td><?php echo e($i); ?></td>
                                <td>-</td>
                                <td>-</td>
                                <td>-</td>
                                <td><?php echo e($item->item_code); ?></td>
                                <td><?php echo e($item->item_name_en); ?></td>
                                <td><?php echo e($item->qty); ?></td>
                                <td><?php echo e($item->unit_price); ?></td>
                                <td><?php echo e($item->qty * $item->unit_price); ?></td>
                                <td>-</td>
                            </tr>
                        <?php endif; ?>
                        <?php
                            $total+=($item->qty * $item->unit_price);
                            $i++;
                        ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>                                                                      
                        
                                                           
                       <tr role="row">
                            <th colspan="8">Total</th>
                            <th><?php echo e($total); ?></th>
                            <td></td>
                        </tr>

                    </tbody>
            </table>
    </body>
</html>


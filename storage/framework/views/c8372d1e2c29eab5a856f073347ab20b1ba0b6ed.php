<?php $__env->startSection('title','Honest Technic | New Products'); ?>
<?php $__env->startSection('user','li_active'); ?>
<?php $__env->startSection('customCss'); ?>
   
<?php $__env->stopSection(); ?>

<?php $__env->startSection('main-content'); ?>
    <main class="main">
        <ol class="breadcrumb">
           <li class="breadcrumb-item">Home</li>
           <li class="breadcrumb-item">
              <a href="#">Product</a>
           </li>
           <li class="breadcrumb-item active">Update Product</li>
        </ol>
        <div class="container-fluid">
                <div class="card row">
                        <div class="card-header">
                           <i class="fa fa-edit"></i> Update Product Information
                           <div class="card-header-actions">
                          
                           </div>
                        </div>
                        <div class="card-body">
                            <?php if($errors->any()): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                            
                            <?php if(session('success')): ?>
                                <div class="alert alert-success">
                                <?php echo e(session('success')); ?>

                                </div> 
                            <?php endif; ?>
                              
                        <form action="<?php echo e(url('cp/products/update/'.$product['item_id'])); ?>" method="post" enctype="multipart/form-data">
                                <?php echo csrf_field(); ?>
                                
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="nf-email">Product Code</label>
                                        <input class="form-control" value="<?php echo e($product['item_code']); ?>"  required name="item_code" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">Product Name</label>
                                        <input class="form-control" value="<?php echo e($product['item_name_en']); ?>"  required name="item_name_en" required type="text">
                                    </div>
                                    <div class="form-group">
                                            <label for="nf-email">#Tag</label>
                                            <input class="form-control" value="<?php echo e($product['tag']); ?>" placeholder="New,Arrival,Promtion" name="tag" type="text">
                                    </div>
                                    
                                    
                                </div>
                                <div class="col-sm-6">
                                   
                                    <div class="form-group">
                                        <label for="nf-email">#Brands</label>
                                        <input class="form-control" value="<?php echo e($product['brand']); ?>" placeholder="Dell,Acer,Lenovo....."  required name="brand" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">Category</label>
                                        <select name="category" class="form-control">
                                            <?php $__currentLoopData = $category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($item->cate_id); ?>" <?php echo $item->cate_id==$product->cate_id ? 'selected':''; ?>><?php echo e($item->cate_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">SubCategory</label>
                                        <select name="sub_category" class="form-control">
                                            <?php $__currentLoopData = $sub_category; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($item->sub_cateid); ?>" <?php echo $item->sub_cateid==$product->sub_cate_id ? 'selected':''; ?>><?php echo e($item->sub_cate_name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                        <div class="card">
                                            
                                            <div class="card-header"><i class="fa fa-edit"></i>Description(Khmer)
                                                <div class="card-header-actions">
                                                    <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#kh" aria-expanded="true"><i class="icon-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="card-body collapse hide" id="kh" style="">
                                                <div class="form-group">
                                                <textarea class="summernote" name="descriptions_kh"><?php echo e($product->descriptions_kh); ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card">
                                            
                                            <div class="card-header"><i class="fa fa-edit"></i>Description(English)
                                                <div class="card-header-actions">
                                                    <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#collapseExample" aria-expanded="true"><i class="icon-arrow-up"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="card-body collapse hide" id="collapseExample" style="">
                                                <div class="form-group">
                                                <textarea class="summernote" name="descriptions_en"><?php echo e($product->descriptions_en); ?></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="card">
                                                <div class="card-header"><i class="fa fa-edit"></i>Description(Vietnam)
                                                    <div class="card-header-actions">
                                                        <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#vn" aria-expanded="true"><i class="icon-arrow-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="card-body collapse hide" id="vn" style="">
                                                    <div class="form-group">
                                                        <textarea class="summernote" name="descriptions_vn"><?php echo e($product->descriptions_vn); ?></textarea>
                                                    </div>
                                                </div>
                                         </div>

                                         <div class="card">
                                                <div class="card-header"><i class="fa fa-edit"></i>Description(Chinese)
                                                    <div class="card-header-actions">
                                                        <a class="btn btn-minimize" href="#" data-toggle="collapse" data-target="#cn" aria-expanded="true"><i class="icon-arrow-up"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="card-body collapse hide" id="cn" style="">
                                                    <div class="form-group">
                                                        <textarea class="summernote" name="descriptions_cn"><?php echo e($product->descriptions_cn); ?></textarea>
                                                    </div>
                                                </div>
                                         </div>

                                         <div class="form-group">
                                            <label for="nf-email">photos</label>
                                            <input type="file" class="form-control" name="photos[]" multiple />
                                        </div>
                                        <div class="form-group">
                                                <button type="submit" class="btn btn-primary" style="margin-top:30px">Save</button>
                                        </div>
                                        
                                    </div>

                                    
                                
                            </div>
                            
                           
                        </form>
                           
                        <div id="accordion" role="tablist">
                                <div class="card">
                                    <div class="card-header" id="headingOne" role="tab">
                                        <h5 class="mb-0"><a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">Product Image</a></h5>
                                    </div>
                                    <div class="collapse" id="collapseOne" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                        <div class="card-body">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <th>No</th>
                                                    <th>Filename</th>
                                                    <th>Thumbnail</th>
                                                    <th>Feature Image</th>
                                                    <th>Created Date</th>
                                                    <th>Delete</th>
                                                </thead>
                                                <tbody>
                                                    <?php if(isset($product->item_detail)): ?>
                                                            <?php
                                                                $i=0;
                                                            ?>
                                                        <?php $__currentLoopData = $product->item_detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <?php
                                                                $i++;
                                                            ?>
                                                            <tr>
                                                                <td><?php echo e($i); ?></td>
                                                                <td><?php echo e($item->filename); ?></td>
                                                                <td><img src="<?php echo e(asset('storage/photos/thumbnail/'.$item->filename)); ?>" /></td>
                                                                <td></td>
                                                                <td><?php echo e($item->created_at); ?></td>
                                                            <td><a href="<?php echo e(url('cp/products/detail/disable/'.$item->id)); ?>" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure to delete this image?')"><i class="fas fa-trash-alt"></i></a></td>
                                                            </tr>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                <tbody>
                                                
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="headingTwo" role="tab">
                                        <h5 class="mb-0"><a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Product Prices</a></h5>
                                    </div>
                                    <div class="collapse" id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion" style="">
                                        <div class="card-body">
                                                    <button type="button" style="margin:10px;" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">
                                                       New Item Price
                                                      </button>
                                                <table class="table table-bordered">
                                                        <thead>
                                                            <th>No</th>
                                                            <th>Unit Price($)</th>
                                                            <th>Created Date</th>
                                                            <th>Status</th>
                                                            <th>Delete</th>
                                                        </thead>
                                                        <tbody>
                                                            <?php if(isset($product->items_price)): ?>
                                                                    <?php
                                                                        $i=0;
                                                                    ?>
                                                                <?php $__currentLoopData = $product->items_price; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php
                                                                    $i++;
                                                                    ?>
                                                                   <tr>
                                                                        <td><?php echo e($i); ?></td>
                                                                        <td><?php echo e(number_format($item->unit_price,2)); ?></td>
                                                                        <td><?php echo e($item->created_at); ?></td>
                                                                        <td><?php echo $item->d_status==1 ? '<i class="far fa-check-circle"></i>':'<i class="far fa-times-circle"></i>'; ?></td>
                                                                        <td><button class="btn btn-sm btn-danger"><i class="fas fa-trash-alt"></i></button></td>
                                                                    </tr>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            <?php endif; ?>
                                                           
                                                        </tbody>
                                                    </table>   
                                        
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
        </div>
</main>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customModal'); ?>

      
      <!-- The Modal -->
      <div class="modal" id="myModal">
        <div class="modal-dialog">
          <div class="modal-content">
      
            <!-- Modal Header -->
          <form method="POST" action="<?php echo e(url('cp/products/new_price/'.$product['item_id'])); ?>">
                <?php echo csrf_field(); ?>
            <div class="modal-header">
              <h4 class="modal-title">Change Current of Product Price</h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
      
            <!-- Modal body -->
            <div class="modal-body">

                    <div class="form-group">
                            <label for="nf-email">Item Price</label>
                            <input class="form-control" value="<?php echo e(old('price')); ?>" placeholder="new price" required name="price" type="text">
                    </div>
            </div>
      
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="submit" class="btn btn-primary" onclick="return confirm('Are you sure to add new current price of this product?')" >Save</button>
            </div>
          </form>
      
          </div>
        </div>
      </div>
    
<?php $__env->stopSection(); ?>

<?php $__env->startSection('customJs'); ?>
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote-lite.js"></script>
    <script>
        $('.summernote').summernote({
        placeholder: 'Description in english',
        tabsize: 2,
        height: 100
      });
    </script>
<?php $__env->stopSection(); ?>





     
     
<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /home/honesttechnic/public_html/website/resources/views/backend/products/edit.blade.php */ ?>
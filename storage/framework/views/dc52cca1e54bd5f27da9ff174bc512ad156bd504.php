<?php /* D:\laragon\www\honesttechnic\resources\views/frontend/pages/contact.blade.php */ ?>
<?php $__env->startSection('title', 'Home | Welcome to Honest Technic'); ?>
<?php $__env->startSection('active-contact', 'nav-active'); ?>


<?php $__env->startSection('content'); ?>

<div class="map" style="background:#fbfbfb;">
        <div class="container">
           
            

            <div class="contact_us_form">
              <div class="container inner_form">
                  <div class=""><br><br>
                    <h3 class="color_title"><?php echo app('translator')->getFromJson('contact.information'); ?></h3>
                    <div class="contact_us">
                      <p><?php echo app('translator')->getFromJson('contact.require'); ?></p>
                    </div>
                  </div>

                    <div class="form-row"  style="margin-top:15px;">
                        <div class="form-group col-md-4">
                            <input type="hidden" name="_token" value="X1o1Hssp9THP246j2U5opUX4n3TzlDINS7Ayd3u1">
                            <label for="name" class="d-none d-xl-block"><?php echo app('translator')->getFromJson('contact.name'); ?>*</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Your Full Name">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputEmail4" class="d-none d-xl-block"><?php echo app('translator')->getFromJson('contact.email'); ?>*</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputEmail4" class="d-none d-xl-block"><?php echo app('translator')->getFromJson('contact.subject'); ?>*</label>
                            <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject">
                        </div>

                        <div class="form-group col-md-12">
                            <label for="exampleTextarea" class="d-none d-xl-block"><?php echo app('translator')->getFromJson('contact.message'); ?></label>
                            <textarea class="form-control" id="message" name="message" rows="3"></textarea>
                        </div>
                    </div>

                <div class="row justify-content-end">
                  
                    
                    <div class="col text-right" style="padding-right:30px;">
                      <span><?php echo app('translator')->getFromJson('contact.send_copy'); ?></span> &nbsp;
                      <label>
                        <i onclick="checkBox()" id="check-box" class="fa fa-square-o fa-lg"></i>
                        <input name="send-yourself" type="hidden" id="send-your-self" value="0">
                      </label>&nbsp;
                      
                      
                      <button id="btn-submit" type="button" onclick="sendContact()" class="btn btn-primary btn-send" data-toggle="button" aria-pressed="false" autocomplete="off">
                        <i id="send-icon" class="fa fa-send"></i> <?php echo app('translator')->getFromJson('contact.submit'); ?>
                      </button>
                    </div>
                  </div>
               </div>
            </div>
            
            <div class="row inner_map" >
                <br><br>
                <div class="col-lg-4">
                   <h3 class="color_title"><?php echo app('translator')->getFromJson('contact.phone'); ?></h3>
                   <div class="contact_us">
                      <p><span class="fa fa-phone" aria-hidden="true" style="color:#d39e00"></span>&nbsp;(+855) 23 222 399<br><span class="fa fa-print" aria-hidden="true" style="color:#d39e00"></span>&nbsp;(+855) 23 222 199</p>

                    </div>
                </div>
                <div class="col-lg-4">
                    <h3 class="color_title"><?php echo app('translator')->getFromJson('contact.email'); ?></h3>
                    <div class="contact_us">
                      <p><span class="fa fa-envelope" aria-hidden="true" style="color:#d39e00"></span>&nbsp;info@honesttechnic.com</p>
                    </div>
                </div>
                <div class="col-lg-4">
                    <h3 class="color_title"><?php echo app('translator')->getFromJson('contact.website'); ?>:</h3>
                    <div class="contact_us">
                      <p><span class="fa fa-globe" aria-hidden="true" style="color:#d39e00"></span>&nbsp;<a style="color:#212529;" target="_blank" href="http://www.honesttechnic.com/">honesttechnic.com</a></p>
                    </div>
                </div>
            </div>
            <br><br>
            <div class="row inner_map" style="padding-left:15px;">
                <div class="col-lg-6">
                  <h3 class="color_title"><?php echo app('translator')->getFromJson('home.cambodia'); ?>:</h3>
                  <div class="contact_us">
                      <p><span class="fa fa-home" aria-hidden="true" style="color:#d39e00"></span>&nbsp;#41-43, Norodom Blvd,Sangkat Phsar Thmey III,Khan Doun Penh, Phnom Penh, Cambodia.</p>
                    </div>
                  <div>
                    <!--<img src="https://geolinklogistics.com.kh/public/frontend/images/map.png" class="w-100">-->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1199.1519288204172!2d104.85012420015607!3d11.527630190911855!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3109502184ba3b05%3A0x4a9d81cf424c8f8b!2zMTHCsDMxJzM4LjIiTiAxMDTCsDUxJzAyLjIiRQ!5e0!3m2!1skm!2skh!4v1553331102038" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                      <br><br>
                  </div>
                </div>
                <div class="col-lg-6">
                  <h3 class="color_title"><?php echo app('translator')->getFromJson('home.vietnam'); ?>:</h3>
                  <div class="contact_us">
                      <p><span class="fa fa-home" aria-hidden="true" style="color:#d39e00"></span>&nbsp;Street217, Prekampeus village, Dunkor District, Khan Dunkor, Phnom Penh, Cambodia.</p>
                  </div>
                  <div>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1199.1519288204172!2d104.85012420015607!3d11.527630190911855!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3109502184ba3b05%3A0x4a9d81cf424c8f8b!2zMTHCsDMxJzM4LjIiTiAxMDTCsDUxJzAyLjIiRQ!5e0!3m2!1skm!2skh!4v1553331102038" width="100%" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
                      <br><br>
                  </div>
                </div>
            </div>

           </div>
        </div>

<?php $__env->stopSection(); ?>        



<?php $__env->startSection('footertop'); ?>

    <?php echo $__env->make('frontend.layouts.footertop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend/layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
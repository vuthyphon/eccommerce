<?php /* D:\laragon\www\honesttechnic\resources\views/backend/home/index.blade.php */ ?>
<?php $__env->startSection('title','Honest Technic | Users'); ?>
<?php $__env->startSection('home','li_active'); ?>

<?php $__env->startSection('main-content'); ?>
    <main class="main">
        <ol class="breadcrumb">
           <li class="breadcrumb-item">
           <a href="#"><?php echo e(Auth::user()->username); ?></a>
           </li>
           <li class="breadcrumb-item active">Dashboard</li>
        </ol>
        <div class="container-fluid">
                <div class="card row">
                        <div class="card-header">
                           <i class="fa fa-edit"></i> Setting Information
                           <div class="card-header-actions">
                          
                           </div>
                        </div>
                        <div class="card-body">

                            <?php if($errors->any()): ?>
                                <div class="alert alert-danger">
                                    <ul>
                                        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <li><?php echo e($error); ?></li>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                            
                            <?php echo session('message'); ?>

                              
                        <form action="<?php echo e(url('home/update_setting')); ?>" method="post">
                                <?php echo csrf_field(); ?>
                                
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="nf-email">Facebook</label>
                                    <input class="form-control" value="<?php echo e($setting->fb_link); ?>" required name="facebook" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">Instagram</label>
                                    <input class="form-control" value="<?php echo e($setting->instagram_link); ?>" required name="instagram" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">Twiiter</label>
                                    <input class="form-control" value="<?php echo e($setting->twitter_link); ?>" required name="twitter" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-password">Email</label>
                                    <input class="form-control" name="email" value="<?php echo e($setting->email_link); ?>" type="text" data-validation="[EMAIL]">
                                    </div>
                                    <div class="form-group">
                                            <label for="nf-password">Telephone</label>
                                    <input class="form-control" value="<?php echo e($setting->telephone); ?>" name="telephone" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">Phone</label>
                                    <input type="text" class="form-control" value="<?php echo e($setting->mobile); ?>" name="phone">
                                    </div>
                                    <div class="form-group">
                                        <label for="nf-email">Address</label>
                                    <input type="text" class="form-control" value="<?php echo e($setting->address); ?>" name="address" >
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary" style="margin-top:30px">Save</button>
                                    </div>
                                </div>
                            </div>
                            
                           
                        </form>
                           
                        
                    </div>
                </div>
        </div>
</main>
<?php $__env->stopSection(); ?>





     
     
<?php echo $__env->make('backend.layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* D:\laragon\www\honesttechnic\resources\views/frontend/pages/productdetail.blade.php */ ?>
<?php $__env->startSection('title', 'Product | Welcome to Honest Technic'); ?>

<?php $__env->startSection('functionalscript'); ?>

    <script>

        function addtocart(id)
        {
            
            $.ajax({
                type: "get",
                url: "<?php echo e(url('addtocart')); ?>/"+id,
                //data:'_token = <?php echo csrf_token() ?>',
                success: function(msg) {
                    if(msg=='login')
                    {
                        $('#loginModal').modal(); 
                    }
                    else
                    {
                        $('#toast').html(msg);
                        $('#toast').show(1000);
                    }
                }
            });
        }
        
        function loginAjax()
        {
            $.ajax({
                type: "get",
                url: "<?php echo e(url('loginajax')); ?>",
                data:{
                    e_mail: $('#email').val(),
                    password: $('#paswd').val()
                },
                success: function(msg) {
                    if(msg=='LoginOk'){
                        $('#loginModal').modal('hide');
                    }
                    $('#loginmsg').html(msg);
                }
            });
        }
        
    </script>  

<?php $__env->stopSection(); ?>

<?php $__env->startSection('active-product', 'nav-active'); ?>

<?php
    if(session('locale'))
    {
        $lang=session('locale');
        if($lang=='cn'){
            $descriptions=($product->descriptions_ch!='' ? $product->descriptions_ch:$product->descriptions_en);
        }
        elseif($lang=='kh')
        {
            $descriptions=($product->descriptions_kh!='' ? $product->descriptions_kh:$product->descriptions_en);
        }
        elseif ($lang=='vn') {
            $descriptions=($product->descriptions_vn!='' ? $product->descriptions_vn:$product->descriptions_en);
        }
        else{
            $descriptions=$product->descriptions_en;
        }
    }
    else{
        $descriptions=$product->descriptions_en;
    }
?>


<?php $__env->startSection('content'); ?>

<div class="header-bradcrubm pb-50">
    <div class="container">
        <div class="row">
            <!-- Product Categorie List Start -->
            <div class="col-md-12">
                <div class="main-categorie">
                    <!-- Breadcrumb Start -->
                    <div class="main-breadcrumb">
                        <ul class="ptb-15 breadcrumb-list">
                            <li><a href="<?php echo e(route('home')); ?>">home</a></li>
                            <li><a href="<?php echo e(route('product')); ?>">product</a></li>
                            <li><a href="<?php echo e(url('product/cate-'.$category->cate_id)); ?>"><?php echo e($category->cate_name); ?></a></li>
                            <li><a href="<?php echo e(url('product/category/'.$subcate->sub_cateid)); ?>"><?php echo e($subcate->sub_cate_name); ?></a></li>
                            
                            <li class="active"><a href="javascript:void(0)"><?php echo e($product->item_name_en); ?></a></li>
                        </ul>
                    </div>
                    <!-- Breadcrumb End -->
                </div>
            </div>
            <!-- product Categorie List End -->
        </div>
        <!-- Row End -->
    </div>
</div>

<!-- Product Thumbnail Start -->
        <div class="main-product-thumbnail pb-50">
            <div class="container">
                <div class="row">
                    <!-- Main Thumbnail Image Start -->
                    <div class="col-sm-5">
                        <!-- Thumbnail Large Image start -->
                        <div class="tab-content">
                                
                            <?php if($product->item_detail): ?>
                                <?php
                                    $i=0;
                                ?>
                                <?php $__currentLoopData = $product->item_detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php
                                        $i++;
                                    ?>
                                     <div id="thumb<?php echo e($item->id); ?>" class="tab-pane fade <?php echo e($i==1 ? 'in active':''); ?>">
                                            <a data-fancybox="images" href="<?php echo e(asset('storage/photos/'.$item->filename)); ?>"><img src="<?php echo e(asset('storage/photos/'.$item->filename)); ?>" alt="product-view"></a>
                                    </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                            <?php if(count($product->item_detail)==0): ?>
                                <div id="thumb" class="tab-pane fade in active">
                                    <a data-fancybox="images" href="<?php echo e(asset('frontend/img/no-image.png')); ?>">
                                        <img src="<?php echo e(asset('frontend/img/no-image.png')); ?>" alt="product-view">
                                    </a>
                                </div>
                            <?php endif;?>
                        </div>
                        <!-- Thumbnail Large Image End -->

                        <!-- Thumbnail Image End -->
                        <div class="product-thumbnail">
                            <div class="thumb-menu owl-carousel">
                                <?php if($product->item_detail): ?>
                                    <?php
                                        $i=0;
                                    ?>
                                    <?php $__currentLoopData = $product->item_detail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                            $i++;
                                        ?>
                                        <div class="<?php echo $i==1 ? 'active' : ''; ?>}">
                                            <a data-toggle="tab" href="#thumb<?php echo e($item->id); ?>"> <img src="<?php echo e(asset('storage/photos/thumbnail/'.$item->filename)); ?>" alt="product-thumbnail"></a>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php else: ?>
                                    <div class="active">
                                        <a data-toggle="tab" href="#thumb"> <img src="<?php echo e(asset('frontend/img/no-image.png')); ?>" alt="product-thumbnail"></a>
                                    </div>   
                                <?php endif; ?>
                            </div>
                        </div>
                        <!-- Thumbnail image end -->
                    </div>
                    <!-- Main Thumbnail Image End -->
                    <!-- Thumbnail Description Start -->
                    <div class="col-sm-7">
                        <div class="thubnail-desc fix">
                            <h2 class="product-header"><?php echo e($product->item_name_en); ?></h2>
                            <div class="pro-ref">
                                
                            </div>
                            <div class="rating-summary fix mtb-10">
                                <div class="rating f-left">
                                    <i class="">&nbsp;</i>
                                    
                                </div>
                                <div class="rating-feedback f-left">
                                    <a href="javascript:void(0)">(<?php echo e($product->item_view); ?> review)</a>
                                <a href="javascript:void(0)"> <b color='blue'><?php echo e($product->item_code); ?></b></a>
                                </div>
                            </div>
                            <div class="pro-price mb-15">
                                <span>$<?php echo $product->item_price!=null ? number_format($product->item_price->unit_price,2):'0.00'; ?></span>
                            </div>
                            <div class="box-quantity mb-30">
                                <form action="#">
                                    <input class="number" id="numeric" type="number" value="1">
                                    <button type="button" class="action-prime" onclick="addtocart(<?php echo e($product->item_id); ?>);">add to cart</button>
                                </form>
                            </div>
                            <div class="product-social-link">
                                <ul class="list-inline">
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- Thumbnail Description End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>
        <!-- Product Thumbnail End -->


    <div class="thumnail-desc pb-50">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="main-thumb-desc">
                            <li class="active"><a data-toggle="tab" href="#dtail">Details</a></li>
                            <!--<li><a data-toggle="tab" href="#review">Reviews 1</a></li>-->
                        </ul>
                        <!-- Product Thumbnail Tab Content Start -->
                        <div class="tab-content thumb-content">
                            <div id="dtail" class="tab-pane fade in active">
                                <?php echo $descriptions; ?>

                            </div>
                            <div id="review" class="tab-pane fade">
                                <!-- Reviews Start -->
                                <div class="review pb-40">
                                    <h3 class="review-title mb-35">Customer Reviews</h3>
                                    <h4 class="review-mini-title">Plazathemes</h4>
                                    <ul class="review-list">
                                        <!-- Single Review List Start -->
                                        <li>
                                            <span>Quality</span>
                                            <i class="zmdi zmdi-star"></i>
                                            <i class="zmdi zmdi-star"></i>
                                            <i class="zmdi zmdi-star"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <label>Plazathemes</label>
                                        </li>
                                        <!-- Single Review List End -->
                                        <!-- Single Review List Start -->
                                        <li>
                                            <span>price</span>
                                            <i class="zmdi zmdi-star"></i>
                                            <i class="zmdi zmdi-star"></i>
                                            <i class="zmdi zmdi-star"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <label>Review by Plazathemes</label>
                                        </li>
                                        <!-- Single Review List End -->
                                        <!-- Single Review List Start -->
                                        <li>
                                            <span>value</span>
                                            <i class="zmdi zmdi-star"></i>
                                            <i class="zmdi zmdi-star"></i>
                                            <i class="zmdi zmdi-star"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <label>Posted on 7/20/16</label>
                                        </li>
                                        <!-- Single Review List End -->
                                    </ul>
                                </div>
                                <!-- Reviews End -->
                                <!-- Reviews Start -->
                                <div class="review pt-50">
                                    <h3 class="review-title mb-30">You're reviewing: <br><span>Go-Get'r Pushup Grips</span></h3>
                                    <p class="mb-10 req">your rating</p>
                                    <ul class="review-list">
                                        <!-- Single Review List Start -->
                                        <li>
                                            <span>Quality</span>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                        </li>
                                        <!-- Single Review List End -->
                                        <!-- Single Review List Start -->
                                        <li>
                                            <span>price</span>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                        </li>
                                        <!-- Single Review List End -->
                                        <!-- Single Review List Start -->
                                        <li>
                                            <span>value</span>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                        </li>
                                        <!-- Single Review List End -->
                                    </ul>
                                </div>
                                <!-- Reviews End -->
                                <!-- Reviews Field Start -->
                                <div class="riview-field mt-30">
                                    <form autocomplete="off" action="#">
                                        <div class="form-group">
                                            <label class="req" for="sure-name">Nickname</label>
                                            <input type="text" class="form-control" id="sure-name" required="required">
                                        </div>
                                        <div class="form-group">
                                            <label class="req" for="subject">Summary</label>
                                            <input type="text" class="form-control" id="subject" required="required">
                                        </div>
                                        <div class="form-group">
                                            <label class="req" for="comments">Review</label>
                                            <textarea class="form-control" rows="5" id="comments" required="required"></textarea>
                                        </div>
                                        <button type="submit" class="btn-default">Submit Review</button>
                                    </form>
                                </div>
                                <!-- Reviews Field Start -->
                            </div>
                        </div>
                        <!-- Product Thumbnail Tab Content End -->
                    </div>
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>

  

<!-- Related Product Start -->
        <div class="related-product mb-50">
            <div class="container">
                <!-- Group Title Start -->
                <div class="group-title">
                    <h2>related product</h2>
                </div>
                <!-- Group Title End -->
                <div class="row">
                    <div class="col-sm-12">
                        <!-- Main Product Activation Start -->
                        <div class="related-main-pro owl-carousel">
                            <?php $__currentLoopData = $related_product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="single-product" style="height:329.118px;">
                                    <!-- Product Image Start -->
                                    <div class="pro-img">
                                        <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>">
                                            <?php if($item->feature_image): ?>
                                                <img class="primary-img" src="<?php echo e(asset('storage/photos/'.$item->feature_image->filename)); ?>" alt="single-product">
                                            <?php else: ?>
                                                <img class="primary-img" src="<?php echo e(asset('frontend/img/no-image.png')); ?>" alt="single-product">
                                            <?php endif; ?>
                                        </a>
                                        <div class="quick-view text-center">
                                            <a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>">Quick View</a>
                                        </div>
                                    </div>
                                    <!-- Product Image End -->
                                    <!-- Product Content Start -->
                                    <div class="pro-content">
                                        <h4><a href="<?php echo e(url('product/detail/'.$item->item_id)); ?>"><?php echo e($item->item_name_en); ?></a></h4>
                                        <p><span>$<?php echo $item->item_price ? number_format($item->item_price->unit_price,2): '0.00'; ?></span></p>
                                        <div class="rating">
                                            <i class="zmdi zmdi-star"></i>
                                            <i class="zmdi zmdi-star"></i>
                                            <i class="zmdi zmdi-star"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                            <i class="zmdi zmdi-star-outline"></i>
                                        </div>
                                        <div class="pro-actions">
                                            <div class="actions-primary">
                                                <a href="javascript:void(0)" onclick="addtocart(<?php echo e($item->item_id); ?>);" data-toggle="tooltip" title="Add to Cart"><i class="zmdi zmdi-shopping-cart-plus"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Product Content End -->
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <!-- Single Products Start -->
                        </div>
                        <!-- Main Product Activation Start -->
                    </div>
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>
        <!-- Related Product End -->


        
        
<?php $__env->stopSection(); ?>


<?php $__env->startSection('footertop'); ?>

    <?php echo $__env->make('frontend.layouts.footertop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('loginmodal'); ?>

    <?php echo $__env->make('frontend.layouts.loginmodal', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend/layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!doctype html>
<html class="no-js" lang="en">

<head>
<meta charset="utf-8">
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo $__env->yieldContent('title'); ?></title>
    <meta name="description" content="Default Description">
    <meta name="keywords" content="E-commerce" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- place favicon.ico in the root directory -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('frontend/img/icon/favicon.png')); ?>">
    
    <!-- Google Font Open Sans -->
    <!--<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,900" rel="stylesheet">-->
    <!-- mobile menu css -->
    
    <link rel="stylesheet" href="<?php echo e(asset ('frontend/css/meanmenu.min.css')); ?>">
    <!-- animate css -->
    <link rel="stylesheet" href="<?php echo e(asset ('frontend/css/animate.css')); ?>">
    <!-- nivo slider css -->
    <link rel="stylesheet" href="<?php echo e(asset ('frontend/css/nivo-slider.css')); ?>">
    <!-- owl carousel css -->
    <link rel="stylesheet" href="<?php echo e(asset ('frontend/css/owl.carousel.min.css')); ?>">
    <!-- price slider css -->
    <link rel="stylesheet" href="<?php echo e(asset ('frontend/css/jquery-ui.min.css')); ?>">
    <!-- fancybox css -->
    <link rel="stylesheet" href="<?php echo e(asset ('frontend/css/jquery.fancybox.css')); ?>">
    <!-- material design css -->
    <link rel="stylesheet" href="<?php echo e(asset ('frontend/css/material-design-iconic-font.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset ('frontend/css/font-awesome.min.css')); ?>">
    <!-- bootstrap css -->
    <link rel="stylesheet" href="<?php echo e(asset ('frontend/css/bootstrap.min.css')); ?>">
    <!-- default css  -->
    <link rel="stylesheet" href="<?php echo e(asset ('frontend/css/default.css')); ?>">
    <!-- style css -->
    <link rel="stylesheet" href="<?php echo e(asset ('frontend/css/style.css')); ?>">
    <!-- responsive css -->
    <link rel="stylesheet" href="<?php echo e(asset ('frontend/css/responsive.css')); ?>">
    <!-- modernizr js -->
    <script src="<?php echo e(asset ('frontend/js/vendor/modernizr-2.8.3.min.js')); ?>"></script>

    <?php echo $__env->yieldContent('functionalscript'); ?> 

</head>

<body>
    <?php 
        $langauge=array('en'=>'English','kh'=>'Khmer','cn'=>'Chinese','vn'=>'Vietnam');
        $lang=session('locale');
    ?>
    

    

    
    <div class="wrapper">
               
        <!-- Header Area Start -->
        <header>
            <!-- Header Top Start -->
            <div class="header-top white-bg">
                <div class="container">
                    <div class="row">
                        <!-- Header Top left Start -->
                        <div class="col-md-3 col-sm-4 col-xs-12">
                            <div class="header-top-left f-left">
                                <ul class="header-list-menu">
                                    <!-- Language Start -->
                                    <li><a href="javascript:void(0);"><?php echo $lang ? $lang :'EN' ?></a>
                                        <ul class="ht-dropdown">
                                            <li><a href="<?php echo e(url('locale/kh')); ?>">Khmer</a></li>
                                            <li><a href="<?php echo e(url('locale/en')); ?>">English</a></li>
                                            <li><a href="<?php echo e(url('locale/cn')); ?>">Chinese</a></li>
                                            <li><a href="<?php echo e(url('locale/vn')); ?>">Vietnam</a></li>
                                        </ul>
                                    </li>
                                    
                                </ul>
                                <!-- Header-list-menu End -->
                            </div>
                        </div>
                        <!-- Header Top left End -->
                        
                        <!-- Header Top Right Start -->
                        <div class="col-md-9 col-sm-8 col-xs-12">
                            <div class="header-top-right f-right header-top-none">
                                <ul class="header-list-menu right-menu">
                                    <li><a href="#">my account</a>
                                        <ul class="ht-dropdown ht-account" style="text-transform: lowercase;">
                                            <?php if(Session::has('usr')): ?>
                                                <li><a href="#"> <?php echo e('Hi! '.session('usr.first_name')); ?></a></li>
                                                <li><a href="<?php echo e(url('mycart')); ?>"> My Cart</a></li>
                                                <li><a href="<?php echo e(route('signout')); ?>"> Sign Out</a></li>
                                            <?php else: ?>
                                                <li><a href="<?php echo e(route('signin')); ?>"> Sign In</a></li>
                                            <?php endif; ?>
                                            
                                        </ul>
                                    </li>
                                    
                                    <li><a href="<?php echo e(route('checkout')); ?>" class="<?php echo $__env->yieldContent('active-checkout'); ?>">checkout</a></li>
                                </ul>
                                <!-- Header-list-menu End -->
                            </div>
                            
                            <div class="small-version">
                                <div class="header-top-right f-right">
                                    <ul class="header-list-menu right-menu">
                                        
                                        <li><a href="<?php echo e(route('signin')); ?>"><i class="fa fa-user"></i></a></li>
                                        <li><a href="<?php echo e(url('register')); ?>"><i class="fa fa-registered"></i></a></li>
                                        <li><a href="<?php echo e(route('checkout')); ?>"><i class="fa fa-usd"></i></a></li>
                                        <li><a href="javascript:void(0);"><?php echo $lang ? $lang :'EN' ?></a>
                                        <ul class="ht-dropdown">
                                            <li><a href="<?php echo e(url('locale/kh')); ?>">Khmer</a></li>
                                            <li><a href="<?php echo e(url('locale/en')); ?>">English</a></li>
                                            <li><a href="<?php echo e(url('locale/cn')); ?>">Chinese</a></li>
                                            <li><a href="<?php echo e(url('locale/vn')); ?>">Vietnam</a></li>
                                        </ul>
                                    </li>
                                    </ul>
                                    <!-- Header-list-menu End -->
                                </div>
                            </div>
                            
                        </div>
                        <!-- Header Top Right End -->
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Container End -->
            </div>
            <!-- Header Top End -->


            <!-- Header Middle Start -->
            <div class="header-middle ptb-20 white-bg">
                <div class="container">
                    <div class="row">
                        <!-- Logo Start -->
                        <div class="col-lg-3 col-md-4">
                            <div class="logo">
                                <a href="<?php echo e(route('home')); ?>"><img src="<?php echo e(asset ('frontend/img/logo/logo.png')); ?>" alt="Honest Technic" title="Honest Technic"></a>
                            </div>
                        </div>
                        <!-- Logo End -->
                        <!-- Header Middle Menu Start -->
                        <div class="col-lg-9 col-md-8">
                            <div class="middle-menu hidden-sm hidden-xs">
                                <nav>
                                    <ul class="middle-menu-list">
                                        <li><a href="<?php echo e(route('home')); ?>" class="<?php echo $__env->yieldContent('active-home'); ?>" alt="Home" title="Home"><?php echo app('translator')->getFromJson('home.home_menu'); ?></a></li>
                                        <li><a href="<?php echo e(route('product')); ?>" class="<?php echo $__env->yieldContent('active-product'); ?>" alt="Product" title="Product"><?php echo app('translator')->getFromJson('home.product_menu'); ?></a></li>
                                        <li><a href="<?php echo e(route('about')); ?>" class="<?php echo $__env->yieldContent('active-about'); ?>" alt="About Honest Technic" title="About Honest Technic"><?php echo app('translator')->getFromJson('home.about_menu'); ?></a></li>
                                        <li><a href="<?php echo e(url('/contact')); ?>" class="<?php echo $__env->yieldContent('active-contact'); ?>" alt="Contact" title="Contact"><?php echo app('translator')->getFromJson('home.contact_menu'); ?></a></li>
                                        <li><a href="<?php echo e(url('register')); ?>" class="<?php echo $__env->yieldContent('active-register'); ?>" alt="Register" title="Register"><?php echo app('translator')->getFromJson('home.register_menu'); ?></a></li>
                                        <li><a href="<?php echo e(route('signin')); ?>" class="<?php echo $__env->yieldContent('active-login'); ?>" alt="Signin" title="Signin"><?php echo app('translator')->getFromJson('home.sign_in_menu'); ?></a></li>
                                        
                                    </ul>
                                </nav>
                            </div>

                           <!-- Main Cart Box Start 
                            Mobile view for check out
                            -->
                            <div class="cart-box visible-xs">
                                <ul>
                                    <li>
                                       
                                        
                                        <a href="<?php echo e(route('mycart')); ?>">
                                        
                                                <?php if(count($mycart)>0): ?>
                                                    <span style="position: absolute;
                                                    right: -3px;
                                                    top: -7px;
                                                    background: red;
                                                    color: #fff;
                                                    width: 16px;
                                                    height: 16px;
                                                    border-radius: 50%;
                                                    text-align: center;
                                                    font-size: 10px;
                                                    line-height: 16px;"> <?php echo e(count($mycart)); ?></span>
                                                <?php endif; ?>    
                                        </a> 
                                        
                                    </li>
                                </ul>
                            </div>
                            <!-- Main Cart Box End -->

                            
                            <!-- Mobile Menu  Start -->
                            <div class="mobile-menu visible-sm visible-xs">
                                    <nav>
                                        <ul>
                                            <li><a href="<?php echo e(route('home')); ?>">home</a></li>
                                            <li><a href="<?php echo e(url('product/cate-1')); ?>">Computer</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="<?php echo e(url('product/category/1')); ?>">All-In-One</a></li>
                                                    <li><a href="<?php echo e(url('product/category/2')); ?>">Laptop</a></li>
                                                    <li><a href="<?php echo e(url('product/category/3')); ?>">Desktop</a></li>
                                                    <li><a href="<?php echo e(url('product/category/4')); ?>">Server</a></li>
                                                    <li><a href="<?php echo e(url('product/category/7')); ?>">Workstation</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-2')); ?>">Monitors & Display</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="<?php echo e(url('product/category/5')); ?>">TN Panel</a></li>
                                                    <li><a href="<?php echo e(url('product/category/6')); ?>">IPS Panel</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-3')); ?>">Components & Peripherals</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="<?php echo e(url('product/category/8')); ?>">Computer Case</a></li>
                                                    <li><a href="<?php echo e(url('product/category/9')); ?>">CPU</a></li>
                                                    <li><a href="<?php echo e(url('product/category/10')); ?>">Graphic Card</a></li>
                                                    <li><a href="<?php echo e(url('product/category/11')); ?>">HDD/SSD</a></li>
                                                    <li><a href="<?php echo e(url('product/category/12')); ?>">Motherboard</a></li>
                                                    <li><a href="<?php echo e(url('product/category/13')); ?>">Power Supply</a></li>
                                                    <li><a href="<?php echo e(url('product/category/14')); ?>">RAM</a></li>
                                                    <li><a href="<?php echo e(url('product/category/15')); ?>">UPS</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-4')); ?>">Networking</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="<?php echo e(url('product/category/16')); ?>">Media Converter</a></li>
                                                    <li><a href="<?php echo e(url('product/category/17')); ?>">MikorTik</a></li>
                                                    <li><a href="<?php echo e(url('product/category/18')); ?>">Network Cable</a></li>
                                                    <li><a href="<?php echo e(url('product/category/19')); ?>">Network Switch</a></li>
                                                    <li><a href="<?php echo e(url('product/category/20')); ?>">Patch Panel</a></li>
                                                    <li><a href="<?php echo e(url('product/category/21')); ?>">PCI Wireless Card</a></li>
                                                    <li><a href="<?php echo e(url('product/category/22')); ?>">PDU</a></li>
                                                    <li><a href="<?php echo e(url('product/category/23')); ?>">RJ-45</a></li>
                                                    <li><a href="<?php echo e(url('product/category/24')); ?>">Router & Wireless Access point</a></li>
                                                    <li><a href="<?php echo e(url('product/category/25')); ?>">Server Rack</a></li>
                                                    <li><a href="<?php echo e(url('product/category/26')); ?>">USB Wireless Adapter</a></li>
                                                    <li><a href="<?php echo e(url('product/category/27')); ?>">Wifi Range Extender</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-5')); ?>">Security Camera</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="<?php echo e(url('product/category/28')); ?>">Camera</a></li>
                                                    <li><a href="<?php echo e(url('product/category/29')); ?>">Camera Cable</a></li>
                                                    <li><a href="<?php echo e(url('product/category/30')); ?>">CCTV Video Balun</a></li>
                                                    <li><a href="<?php echo e(url('product/category/31')); ?>">DVR & NVR</a></li>
                                                    <li><a href="<?php echo e(url('product/category/32')); ?>">IP Camera</a></li>
                                                    <li><a href="<?php echo e(url('product/category/33')); ?>">Power Supply</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-6')); ?>">Printers</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="<?php echo e(url('product/category/34')); ?>">B&W Printer</a></li>
                                                    <li><a href="<?php echo e(url('product/category/35')); ?>">Color Printer</a></li>
                                                    <li><a href="<?php echo e(url('product/category/36')); ?>">Dot Matrix Printer</a></li>
                                                    <li><a href="<?php echo e(url('product/category/37')); ?>">Printer Ink</a></li>
                                                    <li><a href="<?php echo e(url('product/category/38')); ?>">Printer Ribbon</a></li>
                                                    <li><a href="<?php echo e(url('product/category/39')); ?>">Printer Toner</a></li>
                                                    <li><a href="<?php echo e(url('product/category/40')); ?>">Scanner</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-7')); ?>">Accessories</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="<?php echo e(url('product/category/41')); ?>">Connector</a></li>
                                                    <li><a href="<?php echo e(url('product/category/42')); ?>">Keyboard</a></li>
                                                    <li><a href="<?php echo e(url('product/category/43')); ?>">Mouse</a></li>
                                                    <li><a href="<?php echo e(url('product/category/44')); ?>">Mouse Pad</a></li>
                                                    <li><a href="<?php echo e(url('product/category/45')); ?>">Webcam</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-8')); ?>">Stationery</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="<?php echo e(url('product/category/46')); ?>">Pen</a></li>
                                                    <li><a href="<?php echo e(url('product/category/47')); ?>">Paper</a></li>
                                                    <li><a href="<?php echo e(url('product/category/48')); ?>">Office Accessory</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-9')); ?>">Factory Accessory</a>
                                                <!-- Mobile Menu Dropdown Start -->
                                                <ul>
                                                    <li><a href="<?php echo e(url('product/category/49')); ?>">Tool</a></li>
                                                    <li><a href="<?php echo e(url('product/category/50')); ?>">Cleaning</a></li>
                                                    <li><a href="<?php echo e(url('product/category/51')); ?>">Light</a></li>
                                                    <li><a href="<?php echo e(url('product/category/52')); ?>">Accessory</a></li>
                                                </ul>
                                                <!-- Mobile Menu Dropdown End -->
                                            </li>
                                            <li><a href="<?php echo e(route('about')); ?>">About Us</a></li>
                                            <li><a href="<?php echo e(url('/contact')); ?>">Contact</a></li>
                                            <li><a href="<?php echo e(route('signout')); ?>"> Sign Out</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            <!-- Mobile Menu  End -->
							
							
							
                        </div>
                        <!-- Header Middle Menu End -->
						
						
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Container End -->
            </div>
            <!-- Header Middle End -->


            <!-- Header Bottom Start -->
            <div class="header-bottom ptb-10 blue-bg">
                <div class="container">
                    <div class="row">

                    
                        <!-- Primary Vertical-Menu Start -->
                        <div class="col-lg-4 col-md-4 col-sm-4 hidden-sm hidden-xs">
                                <div class="vertical-menu">
                                    <span class="categorie-title">categories menu</span>
                                    <nav>
                                        <?php if(View::hasSection('home')): ?>
                                            <ul class="vertical-menu-list">
                                        
                                        <?php else: ?>
                                            <ul class="vertical-menu-list menu-hideen">
                                        <?php endif; ?>
                                        
                                        
                                            <li><a href="<?php echo e(url('product/cate-1')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/computer.png')); ?>" alt="menu-icon"></span>Computer</a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column pb-20 fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="<?php echo e(url('product/category/1')); ?>">All-In-One</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/2')); ?>">Laptop</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/3')); ?>">Desktop</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/4')); ?>">Server</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/7')); ?>">Workstation</a></li>
                                                                    
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                        </ul>
                                                    </li>
                                                
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-2')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/monitors.png')); ?>" alt="menu-icon"></span>Monitors & Display</a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                            
                                                                <ul>
                                                                    <li><a href="<?php echo e(url('product/category/5')); ?>">TN Panel</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/6')); ?>">IPS Panel</a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                                                                    
                                                        </ul>
                                                    </li>
                                                    <!-- Mega-Menu Three Column End -->
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-3')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/component.png')); ?>" alt="menu-icon"></span>Components & Peripherals</a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Two Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="<?php echo e(url('product/category/8')); ?>">Computer Case</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/9')); ?>">CPU</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/10')); ?>">Graphic Card</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/11')); ?>">HDD/SSD</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/12')); ?>">Motherboard</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/13')); ?>">Power Supply</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/14')); ?>">RAM</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/15')); ?>">UPS</a></li>
                                                                    
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                            
                                                        </ul>
                                                    </li>
                                                    
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-4')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/networking.png')); ?>" alt="menu-icon"></span>Networking</a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    
                                                                    <li><a href="<?php echo e(url('product/category/16')); ?>">Media Converter</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/17')); ?>">MikorTik</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/18')); ?>">Network Cable</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/19')); ?>">Network Switch</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/20')); ?>">Patch Panel</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/21')); ?>">PCI Wireless Card</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/22')); ?>">PDU</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/23')); ?>">RJ-45</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/24')); ?>">Router & Wireless Access point</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/25')); ?>">Server Rack</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/26')); ?>">USB Wireless Adapter</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/27')); ?>">Wifi Range Extender</a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                            
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-5')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/camera.png')); ?>" alt="menu-icon"></span>Security Camera</a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                            
                                                                <ul>
                                                                    <li><a href="<?php echo e(url('product/category/28')); ?>">Camera</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/29')); ?>">Camera Cable</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/30')); ?>">CCTV Video Balun</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/31')); ?>">DVR & NVR</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/32')); ?>">IP Camera</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/33')); ?>">Power Supply</a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                            
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-6')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/printer.png')); ?>" alt="menu-icon"></span>Printers</a>
                                                <!-- Vertical Mega-Menu Start -->
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="<?php echo e(url('product/category/34')); ?>">B&W Printer</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/35')); ?>">Color Printer</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/36')); ?>">Dot Matrix Printer</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/37')); ?>">Printer Ink</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/38')); ?>">Printer Ribbon</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/39')); ?>">Printer Toner</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/40')); ?>">Scanner</a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <!-- Vertical Mega-Menu End -->
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-7')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/storage.png')); ?>" alt="menu-icon"></span>Accessories</a>
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="<?php echo e(url('product/category/41')); ?>">Connector</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/42')); ?>">Keyboard</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/43')); ?>">Mouse</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/44')); ?>">Mouse Pad</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/45')); ?>">Webcam</a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li><a href="<?php echo e(url('product/cate-8')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/storage.png')); ?>" alt="menu-icon"></span>Stationery</a>
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="<?php echo e(url('product/category/46')); ?>">Pen</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/47')); ?>">Paper</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/48')); ?>">Office Accessory</a></li>
                                                                 </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                             <li><a href="<?php echo e(url('product/cate-9')); ?>"><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/storage.png')); ?>" alt="menu-icon"></span>Factory Accessory</a>
                                                <ul class="ht-dropdown megamenu">
                                                    <!-- Mega-Menu Three Column Start -->
                                                    <li class="megamenu-three-column fix">
                                                        <ul>
                                                            <!-- Single Column Start -->
                                                            <li>
                                                                <ul>
                                                                    <li><a href="<?php echo e(url('product/category/49')); ?>">Tool</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/50')); ?>">Cleaning</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/51')); ?>">Light</a></li>
                                                                    <li><a href="<?php echo e(url('product/category/52')); ?>">Accessory</a></li>
                                                                </ul>
                                                            </li>
                                                            <!-- Single Column End -->
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                             
                                            
                                            <!-- More Categoies Start -->
                                            <li id="cate-toggle" class="category-menu">
                                                <ul>
                                                    <li class="has-sub"> <a href="#">More Categories</a>
                                                        <ul class="category-sub">
                                                            <li><span><img src="<?php echo e(asset ('frontend/img/vertical-menu/5.png')); ?>" alt="menu-icon"></span> <a href="javascript:void(0)">No more categories</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </li>
                                            <!-- More Categoies End -->
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        <!-- Primary Vertical-Menu End -->
                        <!-- Search Box Start -->
                        <div class="col-lg-6 col-md-5 col-sm-8">
                            <div class="search-box-view fix">
                            <form action="<?php echo e(url('product/search')); ?>" method="GET">
                                    <input class="email" type="text" placeholder="Search entire store here..." name="filter" id="search">
                                    <button type="submit" class="submit"></button>
                                </form>
                            </div>
                        </div>
                        <!-- Search Box End -->
                        <!-- Cartt Box Start -->
                        <div class="col-lg-2 col-md-3 col-sm-4">
                            <div class="cart-box hidden-xs">
                                <ul>
                                    <li>
                                    <a href="<?php echo e(route('mycart')); ?>"><span class="cart-text">my cart</span>
                                            <span class="cart-counter">
                                                    <?php if(isset($mycart)): ?> <?php echo e(count($mycart)); ?> <?php endif; ?> item(s)
                                            </span>
                                        </a>
                                        
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Cartt Box End -->
                    </div>
                    <!-- Row End -->
                </div>
                <!-- Container End -->
            </div>
            <!-- Header Bottom End -->
            
        </header>
        <!-- Header Area End -->

        
        

        <?php echo $__env->yieldContent('content'); ?>
        
		
		
		
		
        <!-- Newsletter& Subscribe Start -->
        <div class="subscribe black-bg ptb-15">
            <div class="container">
                <div class="row">
                    <!-- Subscribe Box Start -->
                    <div class="col-sm-6">
                        <div class="search-box-view fix">
                            <form action="#">
                                <label for="email-two">Subscribe</label>
                                <input autocomplete="off" type="text" class="email" placeholder="Enter your email address" name="email" id="email-two">
                                <button type="submit" class="submit"></button>
                            </form>
                        </div>
                    </div>
                    <!-- Subscribe Box End -->
                    <!-- Social Follow Start -->
                    <div class="col-sm-6">
                        <div class="social-follow f-right">
                            <h3>stay connect</h3>
                            <!-- Follow Box End -->
                            <ul class="follow-box">
                                <li><a href="#"><i class="zmdi zmdi-twitter"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-google"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-facebook"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-youtube"></i></a></li>
                                <li><a href="#"><i class="zmdi zmdi-instagram"></i></a></li>
                            </ul>
                            <!-- Follow Box End -->
                        </div>
                    </div>
                    <!-- Social Follow Start -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>
        <!-- Newsletter& Subscribe End -->


        <?php echo $__env->yieldContent('footertop'); ?>
        
        <?php echo $__env->yieldContent('loginmodal'); ?>

    
    </div>
    <!-- Wrapper End -->

    <!-- jquery 3.12.4 -->
    <script src="<?php echo e(asset ('frontend/js/vendor/jquery-1.12.4.min.js')); ?>"></script>
    <!-- mobile menu js  -->
    <script src="<?php echo e(asset ('frontend/js/jquery.meanmenu.min.js')); ?>"></script>
    <!-- scroll-up js -->
    <script src="<?php echo e(asset ('frontend/js/jquery.scrollUp.js')); ?>"></script>
    <!-- owl-carousel js -->
    <script src="<?php echo e(asset ('frontend/js/owl.carousel.min.js')); ?>"></script>
    <!-- countdown js -->
    <script src="<?php echo e(asset ('frontend/js/jquery.countdown.min.js')); ?>"></script>
    <!-- wow js -->
    <script src="<?php echo e(asset ('frontend/js/wow.min.js')); ?>"></script>
    <!-- price slider js -->
    <script src="<?php echo e(asset ('frontend/js/jquery-ui.min.js')); ?>"></script>
    <!-- fancybox js -->
    <script src="<?php echo e(asset ('frontend/js/jquery.fancybox.min.js')); ?>"></script>
    <!-- nivo slider js -->
    <script src="<?php echo e(asset ('frontend/js/jquery.nivo.slider.js')); ?>"></script>
    <!-- bootstrap -->
    <script src="<?php echo e(asset ('frontend/js/bootstrap.min.js')); ?>"></script>
    <!-- plugins -->
    <script src="<?php echo e(asset ('frontend/js/plugins.js')); ?>"></script>
    <!-- main js -->
    <script src="<?php echo e(asset ('frontend/js/main.js')); ?>"></script>
    
    <?php echo $__env->yieldContent('customJs'); ?>
</body>

</html>
<?php /* /home/honesttechnic/public_html/website/resources/views/frontend/layouts/master.blade.php */ ?>
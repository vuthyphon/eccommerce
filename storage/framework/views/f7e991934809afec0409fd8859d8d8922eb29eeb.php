<!-- Slider Area Start -->
<div class="slider-area" style="background:#fbfbfb;">
            <div class="container">
                <div class="row">
                    <!-- Main Slider Area Start -->
                    <div class="col-md-9 col-md-offset-3 col-sm-12">
                        <div class="slider-wrapper theme-default">
                            <!-- Slider Background  Image Start-->
                            <div id="slider" class="nivoSlider">
                                <img src="<?php echo e(asset ('frontend/img/slider/1.jpg')); ?>" data-thumb="<?php echo e(asset ('frontend/img/slider/1.jpg')); ?>" alt="" title="#htmlcaption" />
                                <img src="<?php echo e(asset ('frontend/img/slider/2.jpg')); ?>" data-thumb="<?php echo e(asset ('frontend/img/slider/2.jpg')); ?>" alt="" title="#htmlcaption2" />
                            </div>
                            <!-- Slider Background  Image Start-->
                            <!-- Slider htmlcaption Start-->
                            <div id="htmlcaption" class="nivo-html-caption slider-caption">
                                <!-- Slider Text Start -->
                                <div class="slider-text">
                                    <!--<h1 class="wow fadeInLeft" data-wow-delay="1s">performance<br><span>sportswear</span></h1>-->
                                    <h5 class="wow fadeInRight" data-wow-delay="1s">change the way you play</h5>
                                    <p class="wow bounceInDown" data-wow-delay="1s" style="color:#0076ce;">$564 <span>.99</span></p>
                                    <a class="wow bounceInDown" data-wow-delay="0.8s" href="#">shopping now</a>
                                </div>
                                <!-- Slider Text End -->
                            </div>
                            <!-- Slider htmlcaption End -->
                            <!-- Slider htmlcaption Start -->
                            <div id="htmlcaption2" class="nivo-html-caption slider-caption">
                                <!-- Slider Text Start -->
                                <div class="slider-text">
                                    <!--<h1 class="wow zoomInUp" data-wow-delay="0.5s"><span>aw</span>-8480sh <br> washer</h1>-->
                                    <h5 class="wow zoomInUp" data-wow-delay="0.6s">shop to get what you love</h5>
                                    <p class="wow zoomInUp" data-wow-delay="0.8s" style="color:#fff;">$564 <span>.99</span></p>
                                    <a class="wow zoomInUp" data-wow-delay="1s" href="#">shopping now</a>
                                </div>
                                <!-- Slider Text End -->
                            </div>
                            <!-- Slider htmlcaption End -->
                        </div>
                    </div>
                    <!-- Main Slider Area End -->
                </div>
                <!-- Row End -->
            </div>
            <!-- Container End -->
        </div>
<?php /* /home/honesttechnic/public_html/website/resources/views/frontend/layouts/slider.blade.php */ ?>
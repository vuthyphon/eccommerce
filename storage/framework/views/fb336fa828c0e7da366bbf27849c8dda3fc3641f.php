<?php $__env->startSection('title', 'Register | Welcome to Honest Technic'); ?>
<?php $__env->startSection('active-login', 'nav-active'); ?>

<?php $__env->startSection('content'); ?>

<div class="sign-up main-signin ptb-50" style="background:#fbfbfb;">
    <div class="container">
        <!-- Section Title Start -->
        <div class="section-title mb-50">
            <h2>customer login</h2>
        </div>
        <!-- Section Title Start End -->
        <div class="row">
            <!-- Sign-in Start -->
            <div class="col-sm-6">
                <div class="create-account riview-field">
                    <!-- Sign-in Information Start -->
                    <div class="sign-in">
                        
                        <?php echo session('message'); ?>

                        
                        <h4 class="mb-15 pb-15">Registered Customers</h4>
                        <p class="mb-30">If you have an account, sign in with your email address.</p>
                        <form autocomplete="off" method="post" action="<?php echo e(url('clientlogin')); ?>">
                            <?php echo csrf_field(); ?>
                             <div class="form-group">
                                <label class="req" for="email2">Email</label>
                                <input type="email" class="form-control" name="e_mail" id="email2" required="required">
                            </div>
                            <div class="form-group">
                                <label class="req" for="password">Password</label>
                                <input type="password" class="form-control" name="password" id="password" minlength="1" required="required">
                            </div>
                            <button type="submit" id="sign-in">Sign in</button>
                            <a class="text-capitalize f-right mt-5" href="#">forgot your password?</a>
                        </form>
                    </div>
                    <!-- Sign-in Information End -->
                </div>
            </div>
            <!-- Sign-in End -->
            <!-- New Customer Start -->
            <div class="col-sm-6">
                <div class="new-customer create-account">
                    <h4 class="mb-15 pb-15">New Customers</h4>
                    <p class="mb-30">Creating an account has many benefits: check out faster, keep more than one address, track orders and more.</p>
                    <div class="mt-10">
                        <a class="btn-account" href="<?php echo e(url('register')); ?>">create an account</a>
                    </div>
                </div>
            </div>
            <!-- New Customer End -->
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>

<?php $__env->stopSection(); ?>


<?php $__env->startSection('footertop'); ?>

    <?php echo $__env->make('frontend.layouts.footertop', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend/layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php /* /home/honesttechnic/public_html/website/resources/views/frontend/pages/signin.blade.php */ ?>